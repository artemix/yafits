var express = require('express');
var router = express.Router();

var Spectroscopy = require("../models/spectroscopy");
var databases = Spectroscopy.databases;
/*
var cdms = Spectroscopy.CdmsSpectroscopy;
var jpl = Spectroscopy.JplSpectroscopy;
var local = Spectroscopy.LocalSpectroscopy;
var rrl = Spectroscopy.RRLSpectroscopy;
var ISM = Spectroscopy.ISMSpectroscopy;
var ISMCSM = Spectroscopy.ISMCSMSpectroscopy;
var metadataSpectro = Spectroscopy.SpectroscopyMetadata;*/

//let linesCount = {};
/*let sourceDatabases = { cdms : "cdms", 
                        jpl : "jpl", 
                        local : 'local',
                        ism : 'ism', 
                        ismcsm : 'ismcsm',
                        rrl: 'rrl'
                        };*/

function getSourceDatabase(db_name){
  if(databases.data[db_name] !== undefined){
    return databases.data[db_name];
  }else{
    throw "Unknown database";
  }
  /*if(db_name === sourceDatabases.cdms){
    return cdms;   
  } else if(db_name === sourceDatabases.jpl){
    return jpl;
  }else if(db_name === sourceDatabases.local){
    return local;
  }else if(db_name === sourceDatabases.rrl){
    return rrl;
  }else if(db_name === sourceDatabases.ism){
    return ISM;
  }else if(db_name === sourceDatabases.ismcsm){
    return ISMCSM;
  }*/
  
}

/**
 * Returns an object describing the current status of the databases
 * (is it empty or not ?, more infos could be added later if necessary)
 * @param {array} allResults table containing results
 * @param {array} table array of spectral db names
 * @param {number} position index in table (int)
 * @param {object} res nodejs response object
 */
 function getDbStatus(allResults, table, position, res){
  // get status of db at position
  let db =  getSourceDatabase(table[position]);
  let request = db.getFirstLine();  
  request.then((result)=>{
    if(result !== null){
      allResults.push({db:table[position], isempty:false});
    }else{
      allResults.push({db:table[position], isempty:true});
    }
    
    // remove db from list
    table.pop();

    // recall function if there are remaining dbs or return response
    if(table.length > 0){
      return getDbStatus(allResults, table, table.length-1, res);
    }else{
      res.send(allResults);
    }
  }, (error)=>{console.log(error);})
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({"Homepage":"test"});
});

/* GET species from a given database */
router.get('/species/:source', function(req, res, next) {
  let db = req.params.source;
  let query = databases.metadata.getSpecies(db);
  query.then((result)=>{
    res.send(result);    
  }).catch((err)=>{
    console.log(err);
  });  
});


router.get('/metadata', function(req, res, next) {
  let query = databases.metadata.getMetadata();
  query.then((result)=>{
    res.send(result);
  }).catch((err)=>{
    console.log(err);
  });
});

//returns some spectroscopic lines from a source by parameters
router.post('/spectroscopy/:source/lines', function(req, res, next){
    console.log ("router.get('/spectroscopy', function(req, res, next){ : entering");
    let params = req.body;
    let db =  getSourceDatabase(req.params.source);
    let request;
    if(!databases.withoutdatasets.includes(req.params.source)){
      request = db.findInFrequencyIntervals(
        {"frequencies" : params.frequencies,
         "idealisedintensity" : [{"min" : params.idealisedintensity , "max" : 0}],
         "energyup" :  [{"min" : 0 , "max" : params.energyup}],
         "sourcefile" : params.species,
         "atomcount" : params.atomcount}, (err)=>{console.log(err);});
    } else{
      request = db.findInFrequencyIntervals({
                          "frequencies" : params.frequencies,
                          "formula" : params.species,
                          "atomcount" : params.atomcount}, (err)=>{console.log(err);});
    }
    request.then((result)=>{
      res.send(result);
    }, (error)=>{console.log(error);});
});

router.get('/spectroscopy/databases/status', function(req, res, next){
  dbs = Object.keys(databases.data);
  return getDbStatus([], dbs, dbs.length-1, res);
 });


// returns a full dataset for a database
router.post('/spectroscopy/:source/dataset', function(req, res, next){
  console.log ("router.get('/dataset', function(req, res, next){ : entering");

  let params = req.body;
  let db =  getSourceDatabase(req.params.source);
  let request;

  // local database does not provide datasets, only individual lines
  if(!databases.withoutdatasets.includes(req.params.source)){
    request = db.findByTitle(params.sourcefile);
  } else{
    res.send([]);
  }
  request.then((result)=>{
    res.send(result);
  }, (error)=>{console.log(error);});
});

module.exports = router;
