
This is a NodeJS application that queries the Artemix spectroscopic database
and returns lines

API : 

/species/:source (GET method):
  source = cdms | jpl | local
  
  Returns all the species for the given data source
  
/metadata (GET method):

  Returns all metadata related to spectroscopic data 
    min an max numbers of atoms for a molecule in the database
    list of species for each data source
    
    
/spectroscopy/:source (POST method):
  source = cdms | jpl | local
  
  POST parameter : 
    JSON object :
      {
        "frequencies" : [{"min" : float, "max" : float}, ...],
        "atomcount" : [{"min" : float, "max" : float}],
        "energyup" : float,
        "species" : [ "species1", "species2", ...],
        "idealisedintensity" : float    
      }
  

  Returns all the lines matching the given criteria
