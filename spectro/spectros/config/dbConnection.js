/**
* Database connection
*
* Awa - 2018-01
*/
//A config stored in config/default.json
var config = require('config');
var mongoose = require('mongoose');


function createConnection (dbURL, options) {
  // restart: on-failure in docker-compose restarts container if mongodb container is not yet available
  var db = mongoose.createConnection(dbURL, options);

  db.once('open', function () {
      console.log("Connection to db established.");
  });

  return db;
}

function getConnection(){
  let userAccess = config.get("dbSpectroscopy.user")+":"+config.get( "dbSpectroscopy.password")+"@";
  if(userAccess == ":@")
    userAccess = '';
  let url = config.get("dbSpectroscopy.host")+":"+config.get("dbSpectroscopy.port")+"/";
  let urlConnection = 'mongodb://'+userAccess+url+config.get("dbSpectroscopy.dbName");
  return createConnection(urlConnection, {useUnifiedTopology:true, useNewUrlParser: true});
}

var spectroscopyConnection = getConnection();


module.exports = {
	//miningConnection : miningConnection,
	spectroscopyConnection : spectroscopyConnection
};
