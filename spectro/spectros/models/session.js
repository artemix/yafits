//var mongoose = require('mongoose');
var dbConnections =require('../config/dbConnection');

module.exports = dbConnections.miningConnection.model('Session', {
    id : String,
    session : String,
    expires : Date
});