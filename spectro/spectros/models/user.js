/**
* User model and associated queries.
*
* Artemix Group - 2018-01
*/
var dbConnections =require('../config/dbConnection');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    id: String,
    email: String,
    password: String,
    institution: String,
    status: String,
    active: Boolean,
    lastLogin : Date,
    creationDate: Date,
    recentFiles: [String],
    numberOfLogin: Number,
    resetPasswordToken: String,
    resetPasswordExpires: Date
  }
);

//Find a token
UserSchema.statics.findToken = function findToken(token, cb){
    return this.findOne({ resetPasswordToken: token, resetPasswordExpires: { $gt: new Date(Date.now()) } },cb);
};

//Find a user with exec Methods
UserSchema.statics.findOneUser = function findOneUser(email, cb){
    return this.findOne({email: email}).exec(cb);
};

//Update the user files
UserSchema.statics.updateUserFiles = function updateUserFiles(id, files,cb){
    return this.update({_id: id}, {recentFiles : files} ,cb);
};

//Update a user
UserSchema.statics.updateUser = function updateUser(email, active, status, cb){
    return this.update({email: email}, {active : active, status: status},  cb);
};

//Find all UserSchema
UserSchema.statics.findAllUsers = function findAllUsers(cb){
    return this.find({},cb);
};

//Find user by id
UserSchema.statics.findUserById = function findUserById(id, cb){
    return this.findById(id, cb);
};

//Find user by email
UserSchema.statics.findUserByEmail = function findUserByEmail(email, cb){
    return this.findOne({ email : { $regex : new RegExp('^' + email+'$', 'i')}  }, cb);
};

//Find by email without RegExp
UserSchema.statics.findByEmail = function findByEmail(email, cb){
    return this.findOne({ 'email' :  email });
};

//static function : update the user password where email correspond to the parameter
UserSchema.statics.updatePassword =  function updatePassword(email,newPassword, cb) {
      return this.update({ email: email },{$set: {password:newPassword}}, cb);
};

//get crew or all users
UserSchema.statics.findCrewOrAllUsers = function findCrewOrAllUsers(type,cb){
  var condition = {};
  if(type == "crew")
    condition = {status:"crew"};
  return this.find(condition,{email:1,_id:0},cb);
};

var User = dbConnections.miningConnection.model('User', UserSchema);
module.exports = User ;
