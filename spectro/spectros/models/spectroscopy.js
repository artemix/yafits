/**
* Metafits model and associated queries.
*
* Artemix Group - 2018-05
*/
var mongoose = require('mongoose');
mongoose.set('debug', true);
var dbConnections = require("../config/dbConnection")
var Schema = mongoose.Schema;

function getMetadataSchema(){
  let schema = new Schema(    
    {
      id : String,
      name : String,
      atomcountmin : Number,
      atomcountmax : Number,
      species: Array
    },
  );

  /*
    Returns all metadata
  */  
  schema.statics.getMetadata = function(){ 
    return this.find({}).exec();
  }

  schema.statics.getSpecies = function(source){
    return this.aggregate([{$project: { _id:0, 'species': "$"+source+".species"}}]).exec();
  }

  return schema;
}

function getSpectroscopySchema(){
  let schema =  new Schema({
    id : String,
    frequency : Number,
    idealisedIntensity : Number,
    sourcefile : String,
    species : {
      inchikey : String,
      formula : String,
      inchi : String,
      atomcount : Number
    },
    probability : Number,
    lower_state : {
      energy : Number,
      label : Number/*,
      qns : {
        type : Map,
        of : String
      }*/
    },
    upper_state : {
      energy : Number,
      label : Number/*,
      qns : {
        type : Map,
        of : String
      }*/
    }
  });

  /**
    returns a list of mongodb condtions accroding to the given interval llist
    @param  intervals  a list of interval ['min' : val, 'max' : val]
    @param intervalType   string defining the considered interval
  */
  function getIntervals(intervals, intervalType){
    let ands = [];    
    for(let i = 0; i< intervals.length; i++){
      // verify that value has been set
      if(!isNaN(parseFloat(intervals[i]['min']))  && !isNaN(parseFloat(intervals[i]['max']))){
        ands.push({$and : [ {[intervalType] : {$gte :  parseFloat(intervals[i]['min'])}},  {[intervalType] : {$lte :  parseFloat(intervals[i]['max'])}}]});
      }
    }
    return ands;
  }

  function getValues(param, callback){
    let result = [];
    if(param !== undefined && param.length > 0){
      result = callback();
    }
    return result;
  }

  schema.statics.findInFrequencyIntervals = function(values, cb){
    // frequency interval is mandatory    
    if(values['frequencies'].length !== 0){
      let frequencies  = getIntervals(values['frequencies'], "frequency");
      let atomcount = getValues(values['atomcount'], ()=>{ return getIntervals(values['atomcount'], "species.atomcount");});//values['atomcount'] !== undefined ? getIntervals(values['atomcount'], "species.atomcount") : [];
      let intensities = getValues(values['idealisedintensity'], ()=>{return getIntervals(values['idealisedintensity'], "idealisedintensity");}); //values['idealisedintensity'] !== undefined ? getIntervals(values['idealisedintensity'], "idealisedintensity") : [];
      let energyup =  getValues(values['energyup'], ()=>{return getIntervals(values['energyup'], "upper_state.energy");});//values['energyup'] !== undefined ? getIntervals(values['energyup'], "upper_state.energy") : [];
      let sourcefiles = getValues(values['sourcefile'], ()=>{return [{$and : [ {["sourcefile"] : {$in  : values['sourcefile']}}]}]});//values['sourcefile'] !== undefined ? [{$and : [ {["sourcefile"] : {$in  : values['sourcefile']}}]}] : [];
      let formula = getValues(values['formula'], ()=>{return [{$and : [ {["species.formula"] : {$in  : values['formula']}}]}];});//values['formula'] !== undefined ? [{$and : [ {["species.formula"] : {$in  : values['formula']}}]}] : [];
      let params = [{$or :  frequencies  }];

      if(atomcount.length == 1)
        params.push(atomcount[0]);
      if(energyup.length == 1)
        params.push(energyup[0]);
      if(intensities.length == 1)
        params.push(intensities[0]);  
      if(sourcefiles.length == 1)
        params.push(sourcefiles[0]);  
      if(formula.length == 1)
        params.push(formula[0]);
      
      return this.find({$and : params}).exec();
    }else{ 
      return [];
    }
  }

  /**
   * Returns all lines from a source file ordered by frequency
   * @param {} title 
   * @param {*} cb 
   * @returns 
   */
  schema.statics.findByTitle = function(title, cb){      
    //sorted in ascendding order
    return this.find({"sourcefile" : title}).sort({'frequency':"asc"}).exec(cb);
  }


  /**
   * Returns first line from a given db
   * @returns 
   */
   schema.statics.getFirstLine = function(){    
    return this.findOne().exec();
  }


  schema.statics.findByFrequencyValue = function(frequency, cb){
    return this.find({'frequency' : frequency}).exec(cb);
  }
  return schema;
}

var CdmsSpectroscopy = dbConnections.spectroscopyConnection.model('CDMS', getSpectroscopySchema(), 'cdms');
var JplSpectroscopy = dbConnections.spectroscopyConnection.model('JPL', getSpectroscopySchema(), 'jpl');
var LocalSpectroscopy = dbConnections.spectroscopyConnection.model('Local', getSpectroscopySchema(), 'local');
var RRLSpectroscopy = dbConnections.spectroscopyConnection.model('rrl', getSpectroscopySchema(), 'rrl');
var ISMSpectroscopy = dbConnections.spectroscopyConnection.model('ISM', getSpectroscopySchema(), 'ism');
var ISMCSMSpectroscopy = dbConnections.spectroscopyConnection.model('ISMCSM', getSpectroscopySchema(), 'ismcsm');
var SpectroscopyMetadata = dbConnections.spectroscopyConnection.model('Metadata', getMetadataSchema(), 'metadata');

var databases = {
  "data":{
    "cdms" : CdmsSpectroscopy,
    "jpl" : JplSpectroscopy,
    "local" : LocalSpectroscopy,
    "rrl" : RRLSpectroscopy, 
    "ism": ISMSpectroscopy,
    "ismcsm" : ISMCSMSpectroscopy,    
  },
  "metadata" : SpectroscopyMetadata,
  "withoutdatasets": ["local", "rrl"]
}

module.exports = {
  databases
};
