#! /bin/bash
echo "start import"

mongo spectroscopy --eval "db.dropDatabase()"
#mongoimport --host mongodb --db spectroscopy --collection cdms   --file /cdms.json
#mongoimport --host mongodb --db spectroscopy --collection ism  --file /ISM_cdms.json
#mongoimport --host mongodb --db spectroscopy --collection ismcsm  --file /ISM_CSM_cdms.json
#mongoimport --host mongodb --db spectroscopy --collection jpl --file /jpl.json
mongoimport --host mongodb --db spectroscopy --collection local  --file /local.json
mongoimport --host mongodb --db spectroscopy --collection rrl  --file /rrl.json
mongoimport --host mongodb --db spectroscopy --collection metadata --file /metadata.json

#mongo --host mongodb spectroscopy --eval "db.ism.createIndex({sourcefile:1})"
#mongo --host mongodb spectroscopy --eval "db.ism.createIndex({frequency:1})"

#mongo --host mongodb spectroscopy --eval "db.ismcsm.createIndex({sourcefile:1})"
#mongo --host mongodb spectroscopy --eval "db.ismcsm.createIndex({frequency:1})"

#mongo --host mongodb spectroscopy --eval "db.jpl.createIndex({sourcefile:1})"
#mongo --host mongodb spectroscopy --eval "db.jpl.createIndex({frequency:1})"

#mongo --host mongodb spectroscopy --eval "db.cdms.createIndex({sourcefile:1})" 
#mongo --host mongodb spectroscopy --eval "db.cdms.createIndex({frequency:1})"

echo "import done"
