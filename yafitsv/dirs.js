// Directories definitions.
const PNG_ROOT_DIR = '/home/partemix/dataroot/PNG';
const OBJ_ROOT_DIR = '/home/partemix/dataroot/OBJ';
const SPECTRUM_DIR = '/home/partemix/dataroot/SPECTRUM';
const SMOOTH_DIR = '/home/partemix/dataroot/SMOOTH';
const IMG_DIR = '/home/partemix/dataroot/IMG';

exports.PNG_ROOT_DIR = PNG_ROOT_DIR;
exports.OBJ_ROOT_DIR = OBJ_ROOT_DIR;
exports.SPECTRUM_DIR = SPECTRUM_DIR;
exports.SMOOTH_DIR = SMOOTH_DIR;
exports.IMG_DIR = IMG_DIR;
