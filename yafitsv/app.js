var fs = require('fs');

/* Now what do we need ? */
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
//var bodyParser = require('body-parser');
var fs = require('fs');

var index = require('./routes/index');
var users = require('./routes/users');
var olqv = require('./routes/olqv');
var connectArtemix = require('./routes/connectArtemix')
var fitsBrowser = require('./routes/fitsBrowser');
var monitor = require('./routes/monitor');
var purge = require('./routes/purge');

var query = require('./routes/sqlite');
const dirs = require('./dirs');
var test = require('./routes/test');


/*
** Set up the logging machinery
*/
const {logger, connectAccess} = require('./logger');

var express = require('express');
var app = express();

/*
** How the application will use the loggers
*/
connectAccess(app) ;  // HTTP requests will sent to a dedicated file ( see logger.js )

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// defines static paths in which files can be found (png, fits)
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'png')));
app.use(express.static(path.join(__dirname, 'spectrum')));
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use(express.static(path.join(__dirname, 'smooth')));
app.use(express.static(path.join(__dirname, 'img')));

app.use('"'+dirs.SPECTRUM_DIR+'"', express.static(path.join(__dirname, '"'+dirs.SPECTRUM_DIR+'"')));
app.use('"'+dirs.SMOOTH_DIR+'"', express.static(path.join(__dirname, '"'+dirs.SMOOTH_DIR+'"')));
app.use('"'+dirs.IMG_DIR+'"', express.static(path.join(__dirname, '"'+dirs.IMG_DIR+'"')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//app.use("/", index);

app.use('/browse', fitsBrowser);
app.use('/browse/getEntries', fitsBrowser);
app.use('/query', query);
app.use('/query/q', query);
app.use('/query/u', query);
app.use('/connectArtemix', connectArtemix);
app.use('/visit', olqv);
app.use('/users', users);
app.use('/monitor', monitor);
app.use('/purge', purge);
app.use('/test', test);
app.use('/', fitsBrowser);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.locals.error =  err;

  // render the error page
  res.status(err.status || 500);
  // Header already set when we arrive here, haven't find out why yet
  //res.render('error');
});

// SPECTRUM_DIR cleaning of temporary FITS files scheduled
let regex1 = /spectrum[a-f0-9\-]+\.fits$/;
let regex2 = /averageSpectrum[a-f0-9\-]+\.fits$/;
//let regex3 = /imageTop_[0-9]+_[0-9]+_[A-Za-z0-9_\-\.]*\.fits$/;
let regex3 = /imageTop_[A-Za-z0-9_\-\.]*\.fits$/;
let regex4 = /imageBottom_[A-Za-z0-9_\-\.]*\.fits$/;
let regex5 = /smoothCube_[A-Za-z0-9_\-\.]*\.fits$/;

// Remove all files and subdirectories in a directory
function removeDir(dir,ttlInMilliseconds, regex,now) {
  var files = fs.readdirSync(dir);
  var endTime = ttlInMilliseconds;

  if(files.length>0){
    files.forEach(function(file) {
      var fstat = fs.statSync(path.join(dir, file));
      filePath = path.join(dir, file);
      if (fstat.isDirectory()) {
        removeDir(filePath, ttlInMilliseconds, regex,now);
      }
      else {
        endTime = new Date(fstat.mtime).getTime() + ttlInMilliseconds;
        if(regex.test(file)){
          if (now > endTime) {
            try{
		fs.unlinkSync(filePath);
            	logger.info(`Deleted ${file}`);
	    }catch(error){
 	    	logger.error(`impossible to delete files from this server`);
	    }
          }
        }
      }
    });
  }else{
    if(dir !== dirs.IMG_DIR && dir !== dirs.SMOOTH_DIR){
      if (now > endTime) {
       try{
       	   fs.rmdirSync(dir);
       }catch (error) {
           logger.error(`impossible to delete files from this server`);
    	}
      }
    }
  }
};

function unlinkOlderThan(f, ttlInMilliseconds) {
  var fstat = fs.statSync(f);
  var now = new Date().getTime();
  var endTime = new Date(fstat.mtime).getTime() + ttlInMilliseconds;

  if (now > endTime) {
    fs.unlinkSync(f);
    //logger.info(`Deleted ${f}`);
  }
}

function clearSAMPFITSFiles() {
  var ttlInMilliseconds = 300000;
  var ttlSmoothImg = process.env.YAFITSV_TTL_SMOOTH_IMG * 1000;
  var now = new Date().getTime();
 
  //logger.info("About to clean spectrum and averageSpectrum FITS files found in " + dirs.SPECTRUM_DIR);
  fs.readdirSync(dirs.SPECTRUM_DIR).filter(f => regex1.test(f)).map(f => unlinkOlderThan(dirs.SPECTRUM_DIR + "/" + f, ttlInMilliseconds));
  fs.readdirSync(dirs.SPECTRUM_DIR).filter(f => regex2.test(f)).map(f => unlinkOlderThan(dirs.SPECTRUM_DIR + "/" + f, ttlInMilliseconds));
  
  clearSMOOTHIMGFITSFiles(ttlSmoothImg,now);
}

function clearSMOOTHIMGFITSFiles(ttlInMin,now) {
  //logger.info("About to clean smooth and img FITS files found in " + dirs.SMOOTH_DIR+" and "+dirs.IMG_DIR);
  removeDir(dirs.IMG_DIR,ttlInMin,regex3,now);
  removeDir(dirs.IMG_DIR,ttlInMin,regex4,now);
  removeDir(dirs.SMOOTH_DIR,ttlInMin,regex5,now);
}

setInterval(clearSAMPFITSFiles, 60000);

logger.info("Ready to serve!");
module.exports = app;
