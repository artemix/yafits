import { dataPaths, withSAMP, URL_ROOT } from './init.js';
import { displayVotable } from "./votable.js";

/*
 ** A set of classes and function definitions utilized by the
 ** differents flavours of SAMP.
 **
 ** Author : Y. A. BA
 ** Date : 18th September 2019
 */

/**
 * Manages connection to samp hub
 * @param {*} connector  samp hub connector
 * @param {*} dataPaths  paths to data file
 */
var SAMPPublisher = function(connector, dataPaths) {
    console.log("SAMPPublisher ctor: entering");
    let _connector = connector;
    let _dataPaths = dataPaths;

    /**
     * Sends a PNG file to Aladin
     * @param {string} pngPath 
     */
    this._sendPNG = (pngUrl) => {
        console.log("var _sendPNGSlice = function(connection) {: entering");
        console.log("About to send this message 'load " + pngUrl + "' to the HUB");
        var msg = new samp.Message("script.aladin.send", {
            "script": "load " + pngUrl
        });
        _connector.connection.notifyAll([msg]);
        console.log("var _sendPNGSlice = function(connection) {: exiting");
    }

    /**
     * Sends currently displayed single slice image to Aladin
     */
    this.sendPNGSlice = ()=>{
        this._sendPNG(URL_ROOT + "/" + _dataPaths.relSlicePNG);
    }

    /**
     * Sends currently displayed summed slice image to Aladin
     */
    this.sendPNGSummedSlices = ()=> {
        this._sendPNG(URL_ROOT + "/" + _dataPaths.relSummedSlicesPNG);
    }

    /**
     * Sends a script to Aladin to display a list of markers
     * @param {list} markers 
     */
    this.sendMarkers = (markers) => {
        console.log("var _sendPNGSlice = function(connection) {: entering");
        let command = "";
        for (let marker of markers){
            console.log(marker);
            command = command + `draw tag(${marker.RA.value}, ${marker.DEC.value}, "${marker.label.value}",30,30,circle,15);`
        }
        console.log("About to send this message 'load " + command + "' to the HUB");
        const msg = new samp.Message("script.aladin.send", {
            "script": command
        });
        _connector.connection.notifyAll([msg]);
        console.log("var _sendPNGSlice = function(connection) {: exiting");
    }


    /**
     * Sends a spectrum to all applications accepting table.load.fits message
     * @param {string} spectrumUrl 
     * @param {number} tableId (int)
     */
    this.sendSpectrumToAll = (spectrumUrl, tableId)=> {
        console.log("var _sendSpectrum = function(connection) {: entering");
        //var tabUrl = URL_ROOT + _spectrumPath;
        console.log(`Sending url ${spectrumUrl}`);
        var msg = new samp.Message("table.load.fits", {
            "table-id": tableId,
            "url": spectrumUrl,
            "name": "Artemix"
        });
        _connector.connection.notifyAll([msg]);
        console.log("var _sendSpectrum = function(connection) {: exiting");
    }

    /**
     * Sends an image to all applications accepting image.load.fits message
     * @param {string} imageUrl 
     * @param {number} imageId (int)
     */
     this.sendFitsImageToAll = (imageUrl, imageId, imageName) =>{
        console.log("var _sendSpectrum = function(connection) {: entering");
        //var tabUrl = URL_ROOT + _spectrumPath;
        console.log(`Sending url ${imageUrl}`);
        var msg = new samp.Message("image.load.fits", {
            "image-id": imageId,
            "url": imageUrl,
            "name": imageName
        });
        _connector.connection.notifyAll([msg]);
        console.log("var _sendSpectrum = function(connection) {: exiting");
    }

    this.setSliceRelPNGPath = function(relSlicePNGPath) {
        _dataPaths.relSlice = relSlicePNGPath;
    };

    console.log("SAMPPublisher ctor: exiting");
}

var sAMPPublisher = undefined;
var setOnHubAvailability = undefined;
var setOnHubAvailabilityButtons1D = undefined;
var setOnHubAvailability2D = undefined;
var setOnHubAvailabilityButtons = undefined;
var setOnHubAvailabilityButtonsFirstExec = true;


if (withSAMP) {
    /*
     ** Set up everything required for the SAMP publisher.
     */
    var cc = new samp.ClientTracker();
    var callHandler = cc.callHandler;
    var previousState = false;
    //let publishSpectrumDiv = $("#publish-spectrum");


    callHandler["samp.app.ping"] = function(senderId, message, isCall) {
        if (isCall) {
            return {
                text: "ping to you, " + cc.getName(senderId)
            };
        }
    };

    //receive a votable
    callHandler["table.load.votable"] = function(senderId, message, isCall) {
        var params = message["samp.params"];
        var origUrl = params["url"];
        var proxyUrl = cc.connection.translateUrl(origUrl);
        var xhr = samp.XmlRpcClient.createXHR();
        xhr.open("GET", proxyUrl);
        xhr.onload = function() {
            var xml = xhr.responseXML;
            if (xml) {
                try {
                    var data = {};
                    data.content = displayVotable(xml);
                    //to complete when the markers will be available in 3D
                } catch (e) {
                    console.log("error " + e);
                }
            } else {
                console.log("No XML response");
            }
        };
        xhr.onerror = function(err) {
            console.log("Error getting table " + origUrl + "\n" +
                "(" + err + ")");
        };
        xhr.send(null);
    };

    var subs = cc.calculateSubscriptions();

    //description on Artemix in the hub
    let descr = "Artemix is an ALMA data mining experiment prototype. " +
        "its aim is to provide astronomers with a webservice " +
        "that enables to quickly explore the ALMA scientific Archive.";
    var meta = {
        "samp.name": "Artemix:" + dataPaths.relFITSFilePath,
        "samp.description": descr
    };
    //connection to the hub
    let connector = new samp.Connector("Artemix", meta, cc, subs);

    /**
     * Enable or disable SAMP in 2D slice
     */
    /*setOnHubAvailability2D = function(customControls,publishSAMP) {
        samp_registration_area2D.append(connector.createRegButtons());
        let configureSampEnabled = function(isHubRunning) {
            //if the hub is not running or the registration into the hub is not done yet, 
            // the samp button will be hidden
            if (!isHubRunning) {
                
            } else {
                if (!connector.connection) {
                    customControls.removeButton(publishSAMP.getButton());
                } else { 
                    customControls.addButton(publishSAMP.getButton());
                }
            }
        };
        connector.onHubAvailability(configureSampEnabled, 3000);
    };*/

    /**
     * Enable or disable SAMP in 3D slice
     */
    setOnHubAvailabilityButtons = function(samp_registration_area, customControls,publishSAMP) {
        if(setOnHubAvailabilityButtonsFirstExec){
            samp_registration_area.append(connector.createRegButtons());
            setOnHubAvailabilityButtonsFirstExec = false;
        }
        
        let configureSampEnabled = function(isHubRunning) {
            //if the hub is not running or the registration into the hub is not done yet, 
            // the samp button will be hidden
            if (!isHubRunning) {
                console.warn("NO HUB");
            } else {
                if (!connector.connection) {
                    customControls.removeButton(publishSAMP.getButton());
                    console.warn("NO CONNECTION");
                } else { 
                    customControls.addButton(publishSAMP.getButton());
                    console.warn("CONNECTED");
                }
            }
        };
        connector.onHubAvailability(configureSampEnabled, 3000);
    };

    setOnHubAvailabilityButtons1D = function(samp_registration_area) {
        if(setOnHubAvailabilityButtonsFirstExec){
            samp_registration_area.append(connector.createRegButtons());
            setOnHubAvailabilityButtonsFirstExec = false;
        }        
        //connector.onHubAvailability(configureSampEnabled, 3000);
    };





    /**
     * Enable or disable SAMP from a list of viewers
     * @param {*} viewers 
     */

    setOnHubAvailability = function(viewers) {
        //samp_registration_area.append(connector.createRegButtons());
        let configureSampEnabled = function(isHubRunning) {
            let hasChanged = false;
            if(previousState != connector.connection)
                hasChanged = true;
            //if the hub is not running or the registration into the hub is not done yet, 
            // the samp button will be hidden
            if (!isHubRunning) {

            } else {
                if (!connector.connection) {
                    for (let viewer of viewers) {
                        if (viewer !== undefined && hasChanged) {
                            viewer.setSampButtonVisible(false);
                        }
                    }
                } else {
                    for (let viewer of viewers) {
                        if (viewer !== undefined  && hasChanged) {
                            viewer.setSampButtonVisible(true);

                        }
                    }
                }
            }
            previousState = connector.connection;
        };
        connector.onHubAvailability(configureSampEnabled, 3000);
        
    };

    window.onbeforeunload = function() {
        connector.unregister();
    };

    sAMPPublisher = new SAMPPublisher(connector, dataPaths);
}

export {
    sAMPPublisher,
    setOnHubAvailability,
    //setOnHubAvailability2D,
    setOnHubAvailabilityButtons1D,
    setOnHubAvailabilityButtons
}