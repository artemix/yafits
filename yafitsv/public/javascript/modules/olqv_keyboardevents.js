class KeyCodeProcessor {
    static enter(what) {
        //console.group(this.name + "." + what);
        //console.trace();
    }

    static exit() {
        //console.groupEnd();
    }

    constructor(viewer) {
        KeyCodeProcessor.enter(this.constructor.name);
        this.viewer = viewer;
        this.key2action = {};
        KeyCodeProcessor.exit()
    }

    teach(keycode, action){
        KeyCodeProcessor.enter(this.teach.name);
        this.key2action[keycode] = action;
        KeyCodeProcessor.exit();
    }

    open() {
        KeyCodeProcessor.enter(this.open.name);
        document.addEventListener('keydown', this.process.bind(this), false);
        KeyCodeProcessor.exit();
    }

    close() {
        KeyCodeProcessor.enter(this.close.name);
        document.addEventListener('keydown', null);
        KeyCodeProcessor.exit();
    }

    process(event) {
        KeyCodeProcessor.enter(this.process.name);
        // Filter out Ctrl-<somechar> and Meta-<somechar>
        if (!event.ctrlKey && !event.metaKey) {
            let kc = event.key;
            if (kc in this.key2action) {
                this.key2action[kc]();
            }
        }
        KeyCodeProcessor.exit();
    }
}

export{
    KeyCodeProcessor
}
