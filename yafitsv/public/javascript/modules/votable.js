/*
 ** Convert VOTABLE to HTML Format
 */
var displayVotable = function(xml) {
    let vDocEl = xml.documentElement;
    let vTableEl = vDocEl.getElementsByTagName("TABLE")[0];
    let vFieldEls = vTableEl.getElementsByTagName("FIELD");
    let vTrEls = vTableEl.getElementsByTagName("TR");
    let ncol = vFieldEls.length;
    let nrow = vTrEls.length;
    let ic;
    let ir;

    let tableHtml = '';
    tableHtml += '<table border="1"><thead><tr>';
    for (ic = 0; ic < ncol; ic++) {
        tableHtml += "<th> <strong> " + vFieldEls[ic].getAttribute("name") + "</strong></th>";
    }
    tableHtml += "</tr></thead><tbody>";
    let vTdEls;
    for (ir = 0; ir < nrow; ir++) {
        vTdEls = vTrEls[ir].getElementsByTagName("TD");
        tableHtml += "<tr>";
        for (ic = 0; ic < ncol; ic++) {
            tableHtml += "<td>" + getTextContent(vTdEls[ic]) + "</td>";
        }
        tableHtml += "</tr>";
    }
    tableHtml += "</tbody></table>";
    return tableHtml;
};

var getTextContent = function(node) {
    let txt = "";
    let n;
    for (n = node.firstChild; n; n = n.nextSibling) {
        if (n.nodeType === 3 || // text
            n.nodeType === 4) { // CDATA section
            txt = txt + n.data;
        }
    }
    return txt;
};

/*Retrieve list of Ra/Dec from Votable*/
var getRaDecCoord = function(xml) {
    let vDocEl = xml.documentElement;
    let vTableEl = vDocEl.getElementsByTagName("TABLE")[0];
    let vFieldEls = vTableEl.getElementsByTagName("FIELD");
    let vTrEls = vTableEl.getElementsByTagName("TR");
    let ncol = vFieldEls.length;
    let nrow = vTrEls.length;
    let ic;
    let ir;


    let result = [];
    let numRa;
    let numDec;
    for (ic = 0; ic < ncol; ic++) {
        if (vFieldEls[ic].getAttribute("ucd") == "pos.eq.ra")
            numRa = ic;
        else if (vFieldEls[ic].getAttribute("ucd") == "pos.eq.dec")
            numDec = ic;
    }
    let vTdEls;
    for (ir = 0; ir < nrow; ir++) {
        vTdEls = vTrEls[ir].getElementsByTagName("TD");
        let radec = {};
        radec.RAInDD = getTextContent(vTdEls[numRa]);
        radec.DECInDD = getTextContent(vTdEls[numDec]);
        result.push(radec);
    }

    return result;
}

export {
    displayVotable,
    getRaDecCoord
}