// HighChart graph configuration
// width of the plotted spectra, in pixels
const Constants ={
    PLOT_WIDTH_1D : 1200,
    PLOT_WIDTH_1D_LARGE : 1400,
    PLOT_WIDTH_3D : 700,
    PLOT_WIDTH_3D_LARGE : 1000,

    // height of the plotted spectra, this is a width ratio
    PLOT_HEIGHT_RATIO_1D : "40%",
    PLOT_HEIGHT_RATIO_1D_LARGE : "34%",
    PLOT_HEIGHT_RATIO_3D : "80%",
    PLOT_HEIGHT_RATIO_3D_LARGE : "55%",
    // default line color on graph
    PLOT_DEFAULT_COLOR : "#2242c7",
    // default line width on graph
    PLOT_DEFAULT_LINE_WIDTH : 2,
    
    
    // In m/s
    SPEED_OF_LIGHT : 299792458.0,
    
    // default intensity when searching spectroscopy data
    DEFAULT_INTENSITY : -5.1,
    
    //default upper level energy when searching spectroscopy data
    DEFAULT_EUP : 80,
    
    UNIT_FACTOR : {
        "M/S": 1.0,
        "KM/S": 1000.0,
        "HZ": 1.0,
        "MHZ": 1000000.0,
        "GHZ": 1.0e+9,
        "NM": 1.0e-09,
        "CM-1": 1.0,
        "ANGSTROM": 1.0
    },
    
    DEFAULT_OUTPUT_UNIT : {
        "FREQ": "GHZ",
        "VRAD": "KM/S",
        "VOPT": "KM/S",
        "STOKES": "_"
    },
    
    // name of data collections in spectroscopy database
    // ism and ismcsm are predefined lists from CDMS database
    SPECTRO_COLLECTIONS : {
        cdms : "cdms",
        jpl : "jpl",
        local : "local",
        ism : "ism",
        ismcsm : "ismcsm",
        rrl : "rrl"
    },
    
    // Kelvin units format accepted in opened FITS file
    KELVIN_UNITS : new Set(["K (Ta*)", "K (Tmb)"]),
    
    // redshift can also be considered as velocity
    VELOCITY_SHIFT_TYPE : "velocity",
    REDSHIFT_SHIFT_TYPE : "redshift",
    
    // NASA Extragalagtic Database service url
    NED_SERVICE_URL : "https://ned.ipac.caltech.edu/byname?objname=",
    // minimum redshift value to calculate distance and other values
    MIN_REDSHIFT_FOR_CALC : 0.05,
    
    // Spectroscopy database selected by default in user interface
    //DEFAULT_SPECTROSCOPY_DATABASE : "nodatasource",
    LINES_PER_GROUP : 50,
};

export {
    Constants
};