
/**
 * An object to mark informations related to a position
 * in a popup bow located close to the position passed
 * as a parameter.
 * @typedef {Object} LastClickMarker
 * @param {Map} map open layer map where the popup will be displayed
 * @param {string} elementId id of DOM element containing the text
 */
 class LastClickMarker{

    constructor(map, elementId) {
        this._map = map;
        this._overlay = null;
        this._container = document.getElementById(elementId);
        this._content = document.getElementById(elementId + '-content');
        this._closer = document.getElementById(elementId + '-closer');

        this._lastChanIndex = null;
        this._lastCoordinate = null;
        this._lastRADEC = null;
        this._lastRA = null;
        this._lastDEC = null;
        this._lastX = null;
        this._lastY = null;
        this._lastFluxDensity = null;
        this._lastFluxDensityUnit = null;
        this._isVisible = false;
        // create our popup.
        this._popupLastClickInfos();
    }

    get isVisible(){
        return this._isVisible;
    }

    /**
     * Popup creation and addition to an ol overlay to the map passed
     * as a parameter.
     */
    _popupLastClickInfos(){
        let self = this;
        if (this._overlay == null) {
            /**
             * Create an overlay to anchor the popup to the map.
             */
            this._overlay = new ol.Overlay({
                element: this._container,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250
                }
            });

            /**
             * Adds a click handler to hide the popup.
             * @return {boolean} Don't follow the href.
             */
            this._closer.onclick = function() {
                self._overlay.setPosition(undefined);
                self._closer.blur();
                self._isVisible = false;
                return false;
            };

            this._map.addOverlay(this._overlay);
        }
    }

    /**
     * Updates the content of the popup by using the informations
     * stored in _lastCoordinate, _lastRADEC and _lastFluxDensity
     */
    updateLastClickInfos() {
        if (this._lastCoordinate == null) return;
        this._content.innerHTML = `
            Chan#${this._lastChanIndex}<br>
            x = ${this._lastX.toFixed(0)},
            y = ${this._lastY.toFixed(0)}<br>
            RA = ${this._lastRA}<br>
            DEC = ${this._lastDEC}<br>
            Value = ${Number(this._lastFluxDensity).toExponential(4)} ${this._lastFluxDensityUnit}`;

        this._overlay.setPosition(this._lastCoordinate);
        this._isVisible = true;
    }

    setChanIndex(chanIndex){
       this._lastChanIndex = chanIndex;
    }

    setPositions(x, y, ra, dec){
        this._lastRA = ra;
        this._lastDEC = dec;
        this._lastX = x;
        this._lastY = y;
        this._lastCoordinate = [x, y];
    }

    /**
     * Public method to register the fluxDensity value passed as a parameter
     * and update the popup content accordingly.
     * @param {number} fluxDensity flux density (float)
     */
    setFluxDensity(fluxDensity, unit) {
        console.log("this.setFluxDensity = function(fluxDensity) { : entering");
        this._lastFluxDensity = fluxDensity;
        this._lastFluxDensityUnit = unit;
        console.log("this.setFluxDensity = function(fluxDensity) { : exiting");
    }

}

class LastClickMarkerSummed extends LastClickMarker{

    /**
     * Updates the content of the popup by using the informations
     * stored in _lastCoordinate, _lastRADEC and _lastFluxDensity
     */
     updateLastClickInfos(isRefresh) {
        console.log("updateLastClickInfos LastClickMarkerSummed");
        if (this._lastCoordinate == null) return;
        this._content.innerHTML = `
            x = ${this._lastX.toFixed(0)},
            y = ${this._lastY.toFixed(0)} <br>
            RA = ${this._lastRA} <br>
            DEC = ${this._lastDEC} <br>
            Value = ${Number(this._lastFluxDensity).toExponential(4)} ${this._lastFluxDensityUnit}`;

            // poupup is already visible, it is displayed again
            if(this._isVisible){
                this._overlay.setPosition(this._lastCoordinate);
            }
            // poupup was not visible, it is not displayed if request is only a refresh
            else if(!this._isVisible && !isRefresh){
                this._overlay.setPosition(this._lastCoordinate);
                this._isVisible = true;
            }
    }
}

class LastClickMarkerNoChannel extends LastClickMarker{

    /**
     * Updates the content of the popup by using the informations
     * stored in _lastCoordinate, _lastRADEC and _lastFluxDensity
     */
     updateLastClickInfos() {
        console.log("updateLastClickInfos LastClickMarkerSummed");
        if (this._lastCoordinate == null) return;
        this._content.innerHTML = `
            x = ${this._lastX.toFixed(0)},
            y = ${this._lastY.toFixed(0)} <br>
            RA = ${this._lastRA} <br>
            DEC = ${this._lastDEC} <br>
            Value = ${Number(this._lastFluxDensity).toExponential(4)} ${this._lastFluxDensityUnit}`;
            this._overlay.setPosition(this._lastCoordinate);
    }
}

export{
    LastClickMarker, LastClickMarkerSummed, LastClickMarkerNoChannel
};