/**
 * This module centralizes somes statics to manipulate elements of the page
 *  
 */

class DOMAccessor{

    static get3DSampConnection(){
        return document.getElementById("samp-registration");
    }

    static get2DSampConnection(){
        return document.getElementById("sampRegistration");
    }

    /**
     * Returns the velocity input field
     * @returns {Element} 
     */
    static getYafitsVersion(){
        return document.getElementById("version").innerText.trim();
    }


    /**
     * Returns the velocity input field
     * @returns {Element} 
     */
    static getVelocityField(){
        return document.getElementById("lines-velocity");
    }


    /**
     * Returns the redshift input field
     * @returns {Element} 
     */
    static getRedshiftField(){
        return document.getElementById("lines-redshift");
    }

    static getResetZButton(){
        return document.getElementById("z-reset");
    }

    /**
     * Returns the title of the average spectrum
     * @returns {Element} 
     */
    static getAverageSpectrumTitle(){
        return document.getElementById("summedchart-title");
    }

    /**
     * Returns element displaying the position of the mouse on the single slice
     * @returns {Element} 
     */
    static getSingleSliceMousePosition(){
        return document.getElementById('external-mouse-position-1');
    }

    static getSingleChartCoordinates(){
        return document.getElementById('chart-coordinates');
    }

    static getSummedChartCoordinates(){
        return document.getElementById('summedchart-coordinates');
    }

    static getSummedChartCoordinatesArray(){
        let string = this.getSummedChartCoordinates().innerText;
        let strings = string.split(" ");
        let result =[];
        for(let i=1; i < strings.length; i++){
            result.push(strings[i].split("=")[1]);
        }
        return result;
    }

    static getHnotField(){
        return document.getElementById("lines-hnot");
    }

    static getOmegaMField(){
        return document.getElementById("lines-omegam");
    }

    static getSearchRadiusField(){
        return document.getElementById("ned-search-radius");
    }

    static getDlField(){
        return document.getElementById("lines-dl");
    }


    /**
     * Returns element displaying the position of the mouse on the summed slice
     * @returns {Element} 
     */
    static getSummedSliceMousePosition(){
        return document.getElementById('external-mouse-position-2');
    }

    static setSliceChannel(text){
        document.getElementById("slice-channel").textContent = text;
    }

    static setSliceRMS(text){
        document.getElementById("slice-rms").textContent = text;
    }

    static setSummedSliceRMS(text){
        document.getElementById("summedslice-rms").textContent = text;
    }

    static setSliceMean(text){
        document.getElementById("slice-mean").textContent = text;
    }

    static setSummedSliceMean(text){
        document.getElementById("summedslice-mean").textContent = text;
    }

    static updateSingleSliceColorBar(path){
        document.getElementById("slice-colormap").innerHTML=`<img src="${path}" alt="colorbar"></img>`;
    }

    static updateSingleSummedColorBar(path){
        document.getElementById("summed-slice-colormap").innerHTML=`<img src="${path}" alt="colorbar"></img>`;
    }

    /**
     * Returns the loading image
     * @returns 
     */
    static getLoading(){
        return document.getElementById('loading');
    }

    /**
     * Toggles the loading image
     * @param {boolean} isVisible 
     */
    static showLoaderAction(isVisible) {
        if (isVisible) {
            this.getLoading().style.display = 'block';
        } else {
            this.getLoading().style.display = 'none';
        }
    }

    /**
     * Returns the field containing the status of the SAMP connection
     * The status is written in text and must be parsed before use.
     * @returns {Element}
     */
    static getSampConnectionStatus(){
        return document.getElementById("withSamp");
    }

    /**
     * Returns the field containing the status of the test mode
     * The status is written in text and must be parsed before use.
     * @returns {Element}
     */
     static getTestModeStatus(){
        return document.getElementById("testMode");
     }

     /**
     * Returns the field containing the organisation that installed this instance
     * of Yafits
     * The value is written in text and must be parsed before use.
     * @returns {Element}
     */
     static getYafitsTarget(){
        return document.getElementById("yafitsTarget");
    }

    /**
     * Returns the field containing the root url of the service
     * @returns {Element}
     */
    static getUrlRoot(){
        return document.getElementById("urlRoot");
    }

    /**
     * Returns the field containing the path of the currently opened FITS file
     * @returns {Element}
     */
    static getFitsFilePath(){
        return document.getElementById("relFITSFilePath");
    }

    /**
     * Returns the field containing the FITS file header
     * @returns {Element}
     */
    static getHeader(){
        return document.getElementById("header");
    }

    /**
     * Returns the field containing the name of the product whose data are displayed
     * @returns {Element}
     */
    static getProduct(){
        return document.getElementById("product");
    }

    /**
     * Returns the field containing the default value for LUT select
     * @returns {Element}
     */
    static getDefaultLUTIndex(){
        return document.getElementById("defaultLutIndex");
    }

    /**
     * Returns the field containing the url of the spectroscopy server
     * @returns {Element}
     */
    static getSpectroServerUrl(){
        return document.getElementById("spectroServer");
    }

    /**
     * Returns the color selected in the color palette
     * @returns {object} values for ittName, lutName, vmName
     */
    static getConfiguration() {
        return {
            ittName: $('#ITT-selector').find(':selected').text().trim(),
            lutName: $('#LUT-selector').find(':selected').text().trim(),
            vmName: $('#video-mode-selector').find(':selected').text().trim()
        };
    }

    /**
     * Select a LUT in the list
     * @param {object} value name of selected LUT
     */
    static selectLut(value){
        $('#LUT-selector').prop('selectedIndex', value);
    }

    static markLoadingDone(){
        let span = document.createElement("span");
        span.id="loading_done";
        document.getElementById("initialLoad").appendChild(span);
    }
}

export{
    DOMAccessor
};