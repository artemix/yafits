/**
 * An object creating custom events
 */
class EventFactory{

    // available event types
    static EVENT_TYPES = { custom : "custom-event", 
                           refreshFrequency : "refresh-freq",
                           getDataset : 'getdataset'
                    };


    /**
     * Returns a CustomEvent
     * @param {string} eventType  type of event, must exist in EVENT_TYPES
     * @param {object} jsonContent json object stored in the detail attribute of the event
     * @returns 
     */
    static getEvent(eventType, jsonContent){
        switch(eventType){
            case this.EVENT_TYPES.custom:
                let event = new CustomEvent(this.EVENT_TYPES.custom, jsonContent);
                return event;
            
            case this.EVENT_TYPES.refreshFrequency:
                return new CustomEvent(this.EVENT_TYPES.refreshFrequency, jsonContent);

            case this.EVENT_TYPES.getDataset:
                return new CustomEvent(this.EVENT_TYPES.getDataset, jsonContent);
            
            default:
                throw new Error("Unknown event type");    
        }
    }
}


export{
    EventFactory
};