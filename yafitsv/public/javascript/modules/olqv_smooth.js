import { ServerApi } from "./serverApi.js";

class CubeSmoother {
    constructor(relFITSFilePath) {
        this._smoothCubePath = "";
        this._relFITSFilePath = relFITSFilePath;
    }

    getSmoothedCubePath() {
        return this._smoothCubePath;
    }

    createSmoothedCube(nbox) {
        let self = this;
        //Create a FITS file containing the smooth cube to download.
        let serverApi = new ServerApi();
        return serverApi.createSmoothCube(nbox, this._relFITSFilePath, (resp)=>{
            var x = JSON.parse(resp);
            if (x["status"]) {
                self._smoothCubePath = x["result"];
            } else {
                console.log(`Something went wrong during the generation of the smooth cube FITS file, the message was
                ${x["message"]}`);
                alert(x["message"]);
            }
        })
    };

}

export { CubeSmoother }