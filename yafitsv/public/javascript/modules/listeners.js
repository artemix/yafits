import { DOMAccessor } from "./domelements.js";
import { velocity2Redshift, redshift2Velocity, getRadiusInDegrees} from "./utils.js";
import { Constants } from "./constants.js"; 
import { computeDl } from "./olqv_ned.js";
import { FITS_HEADER } from "./fitsheader.js";
import { SpectroApi } from "./serverApi.js";
import { changeDatabase, changeLinesGroup } from "./olqv_spectro.js";
import { CubeSmoother } from "./olqv_smooth.js";
import { dataPaths, URL_ROOT } from "./init.js";


function setNedUiListeners(sourceTable, spectroUI){
    // when switching modal display mode
    $('#ned-modal').on('show.bs.modal', function(event) {
        let selectRadius = DOMAccessor.getSearchRadiusField();
        let radius = getRadiusInDegrees(selectRadius.options[selectRadius.selectedIndex].value, FITS_HEADER);
        try{
            let Hnot = spectroUI.getHubbleCst();
            let OmegaM = spectroUI.getMassDensity();
            sourceTable.getSources(FITS_HEADER.crval1, FITS_HEADER.crval2, radius, Hnot, OmegaM);
        }catch(e){
            console.log(e);
            alert(e);
        }
    });

    try{
        DOMAccessor.getResetZButton().addEventListener("click",function(){
            if(sourceTable !== null && sourceTable.lastZ !== null){
                spectroUI.setRedshift(sourceTable.lastZ);
                DOMAccessor.getRedshiftField().dispatchEvent(new Event('input'));
            }
        })
    }catch(e){
        console.log(e);
    }

}

function setSpectroUiListeners(spectroUI){
    // perform spectroscopy requests
    let spectroq = new SpectroApi();
    // synchronizes velocity and redshift values when user changes it manually
    DOMAccessor.getVelocityField().addEventListener("input", function(event) {
        // get velocity value
        let v;
        try{
            v = spectroUI.getVelocity();
        }catch(e){
            console.log(e);
            alert(e);
            return false;
        }

        if (!isNaN(v)) {
            //spectroUI.setVelocity(v);
            //spectroUI.setRedshift(velocity2Redshift(v));
            spectroUI.setRedshift(0);
            // updates distance value
            try{
                const z = spectroUI.getRedshift();
                if(z >= Constants.MIN_REDSHIFT_FOR_CALC){
                    spectroUI.setDl(computeDl(spectroUI.getHubbleCst(),
                    spectroUI.getMassDensity(),
                    z));
                }else{
                    spectroUI.setDl("");
                }

            }catch(e){
                console.log(e);
                alert(e);
            }
        }


    });

    /*DOMAccessor.getVelocityField().addEventListener("keyup", function(event) {
        viewLinker.isRefreshable = false;
        if(event.key === 'Enter'){
            viewLinker.isRefreshable = true;
            if(viewLinker.spectrumViewer !== null){
                viewLinker.spectrumViewer.refresh();
            }
        }  
    });*/

    DOMAccessor.getRedshiftField().addEventListener("input", function() {
        // get redshift value
        let z;
        console.log("### INPUT REDSHIFT");

        try{
            z = spectroUI.getRedshift();
        }catch(e){
            console.log(e);
            alert(e);
            return;
        }

        if (!isNaN(z)) {
            //spectroUI.setRedshift(z);
            //spectroUI.setVelocity(redshift2Velocity(z));
            spectroUI.setVelocity(0);
            // updates distance value
            try{
                if(z >= Constants.MIN_REDSHIFT_FOR_CALC){
                    spectroUI.setDl(computeDl(spectroUI.getHubbleCst(),
                    spectroUI.getMassDensity(),
                    z));
                }else{
                    spectroUI.setDl("");
                }

            }catch(e){
                alert(e);
                return;
            }
        }
    });

    /*DOMAccessor.getRedshiftField().addEventListener("keyup", function(event) {
        viewLinker.isRefreshable = false;
        if(event.key === 'Enter'){
            viewLinker.isRefreshable = true;
            if(viewLinker.spectrumViewer !== null){
                viewLinker.spectrumViewer.refresh();
            }
        }
    });*/

    // refresh buttons in lines table when DL is modified
    DOMAccessor.getDlField().addEventListener('input', function(){
        spectroUI.refreshEnergyGroupLines();
    })

    // user select a datasource
    $("#localdatasource").on("click", function() {
        spectroUI.disableExtraFields();
        spectroq.getMetadata(function(metadata) { spectroUI.initSpeciesSelection("local", metadata) });
        changeDatabase(spectroq, spectroUI, Constants.SPECTRO_COLLECTIONS.local);
    });

    $("#rrldatasource").on("click", function() {
        spectroUI.enableExtraFields();
        changeDatabase(spectroq, spectroUI, Constants.SPECTRO_COLLECTIONS.rrl);
    });

    $("#ismdatasource").on("click", function() {
        spectroUI.enableExtraFields();
        changeDatabase(spectroq, spectroUI, Constants.SPECTRO_COLLECTIONS.ism);
    });

    $("#ismcsmdatasource").on("click", function() {
        spectroUI.enableExtraFields();
        changeDatabase(spectroq, spectroUI, Constants.SPECTRO_COLLECTIONS.ismcsm);
    });

    $("#jpldatasource").on("click", function() {
        spectroUI.enableExtraFields();
        changeDatabase(spectroq, spectroUI, Constants.SPECTRO_COLLECTIONS.jpl);
    });

    $("#cdmsdatasource").on("click", function() {
        spectroUI.enableExtraFields();
        changeDatabase(spectroq, spectroUI, Constants.SPECTRO_COLLECTIONS.cdms);
    }); 

}

function initSpectroUI(spectroUI){
    // set default values for z and spectroscopy
    spectroUI.clearRedshift();
    spectroUI.clearVelocity();
    spectroUI.setIntensity(Constants.DEFAULT_INTENSITY);
    spectroUI.setEnergyUp(Constants.DEFAULT_EUP);
    $("#spectroscopy-menu").className = "hidden";
    $("#toggle-lines-search").checked = false;
}

function initCubeSmoothing(){
    // checks conformity of number of channels to smooth
    $("#get-nbox").keyup(function() {
        var nbox = $(this).val();
        if (nbox !== "" && isNaN(parseInt(nbox)))
            alert("nbox must be a number");
    });

    $("#get-smooth-cube").on('click', function() {
        DOMAccessor.showLoaderAction(true);
        let cubeSmoother = new CubeSmoother(dataPaths.relFITSFilePath);
        $.when(cubeSmoother.createSmoothedCube(parseInt($("#get-nbox").val()))).done(function() {
            var url = URL_ROOT + cubeSmoother.getSmoothedCubePath();
            DOMAccessor.showLoaderAction(false);
            window.open(url, '_blank');
        });
    });
    $("#view-smooth-cube").on('click', function() {
        DOMAccessor.showLoaderAction(true);
        let cubeSmoother = new CubeSmoother(dataPaths.relFITSFilePath);
        $.when(cubeSmoother.createSmoothedCube(parseInt($("#get-nbox").val()))).done(function() {
            var url = URL_ROOT + "/visit/?relFITSFilePath=//SMOOTH" + cubeSmoother.getSmoothedCubePath();
            DOMAccessor.showLoaderAction(false);
            window.open(url);
        });
    });
}


function initSamp(withSamp){
    if (!withSamp) {
        $("#publish-spectrum").css("display", "none");
        $('#samp-img').css("display", "none");
    }    
}

export{
    setNedUiListeners, setSpectroUiListeners, initSpectroUI, initCubeSmoothing, initSamp
}