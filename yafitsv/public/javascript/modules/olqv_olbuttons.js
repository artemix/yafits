

class Button{
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(viewer){
        this._keyboardMapping = [];
        this.viewer = viewer;
        this.button = document.createElement("button");
        this.button.setAttribute("type", "button");
        this.button.setAttribute("class", "btn btn-primary btn-sm");
        this.button.setAttribute("data-tooltip", "tooltip");
        this.initButton();
    }

    getButton() {
        return this.button;
    }

    test(){
        console.log("test");
    }

    getKeyboardMapping(){
        return this._keyboardMapping;
    }

    initButton(){

    }

    setActive(){
        this.button.classList.add("active");
        //this.button.classList.add("focus");
    }

    unsetActive(){
        this.button.classList.remove("active");
        //this.button.classList.remove("focus");
    }

    setClickAction(action){
        $(this.button).on("click", function (event) { action() });
    }
}

class AxisButton extends Button{
    constructor(viewer) {
        super(viewer);        
    }

    initButton(){
        this._keyboardMapping = ["G", "g"];
        this.button.setAttribute("title", "Toggle grid display");
        let x = document.createElement("span");
        x.innerText = "G";
        this.button.appendChild(x);
    }  
}


class ResetButton extends Button {

    constructor(viewer) {
        super(viewer);
    }

    initButton(){
        this._keyboardMapping = ["R", "r"];
        this.button.setAttribute("title", "Reset zoom and position");
        let x = document.createElement("span");
        x.innerText = "R"
        this.button.appendChild(x);
    }
}

class BoxButton extends Button{
    constructor(viewer) {
        super(viewer);
        this.viewer.addInteractionListener(this);
    }

    initButton(){
        this._keyboardMapping = ["B", "b"];
        this.button.setAttribute("title", "Work with boxes");
        let x = document.createElement("span");
        x.innerText = "B"
        this.button.appendChild(x);
    }

    interactionChanged(){
        this.unsetActive();
    }
}

class MarkerButton extends Button{
    constructor(viewer) {
        super(viewer);
        this.viewer.addInteractionListener(this);
    }

    initButton(){
        this._keyboardMapping = ["M", "m"];
        this.button.setAttribute("title", "Work with markers");
        let x = document.createElement("span");
        x.innerText = "M"
        this.button.appendChild(x);
    }

    interactionChanged(){
        this.unsetActive();
    } 
}

class ClickMarkerButton extends Button{
    constructor(viewer) {
        super(viewer);
        this.isOpened = false;
        this.viewer.addInteractionListener(this);
    }

    initButton(){
        this._keyboardMapping = ["V", "v"];
        this.button.setAttribute("title", "Show pixel information on click");
        let x = document.createElement("span");
        x.innerText = "V"
        this.button.appendChild(x);

        let self = this;
        let f = function (event) {
            if(self.isOpened){
               self.unsetActive();
            } else {
                self.setActive();
            }                
        };
        this.setClickAction(f);
    }

    interactionChanged(){
        this.unsetActive();
    } 
}

class ContoursButton extends Button {

    constructor(viewer) {
        super(viewer);
    }

    initButton(){
        this._keyboardMapping = ["c", "C"];
        this.button.setAttribute("data-toggle", "modal");
        this.button.setAttribute("title", "Work with contours");
        let x = document.createElement("span");
        x.innerHTML = "C"
        this.button.appendChild(x);
    }
}

class DeleteButton extends Button{
    constructor(viewer) {
        super(viewer);
    }

    initButton(){
        this._keyboardMapping = ["Delete", "Backspace"];
        this.button.setAttribute("title", "Publish image via a SAMP hub");
        let x = document.createElement("span");
        x.innerHTML = "D"
        this.button.appendChild(x);
    }
}

class View2DButton extends Button {

    constructor(viewer) {
        super(viewer);
    }

    initButton(){
        this.button.setAttribute("title", "Open in 2D viewer");
        let x = document.createElement("span");
        x.innerHTML = "&#9673;"
        //x.setAttribute("class", "fas fa-home");
        this.button.appendChild(x);
    }
}



class SaveImageButton extends Button {

    constructor(viewer) {
        super(viewer);
    }

    initButton(){
        this.button.setAttribute("title", "Download the 2D image in FITS on disk");
        let x = document.createElement("span");
        x.innerHTML = "&#8681;"
        //x.setAttribute("class", "fas fa-home");
        this.button.appendChild(x);
    }
}

class PublishSAMPButton extends Button{
    constructor(viewer) {
        super(viewer);
    }

    initButton(){
        this.button.setAttribute("title", "Publish image via a SAMP hub");
        let x = document.createElement("span");
        x.setAttribute("class", "fas fa-location-arrow");
        this.button.appendChild(x);
    }
}

class SelectorButton extends Button{
    constructor(viewer) {
        super(viewer);
        this.viewer.addInteractionListener(this);
    }

    initButton(){
        this._keyboardMapping = ["Escape"];
        this.button.setAttribute("title", "Enable object selector (boxes, markers, ...)");
        let x = document.createElement("span");
        x.setAttribute("class", "fas fa-hand-point-up");
        this.button.appendChild(x);
    }

    interactionChanged(){
        this.unsetActive();
    }
}

export{
    ResetButton, View2DButton, SaveImageButton, PublishSAMPButton, BoxButton, AxisButton, 
    SelectorButton, MarkerButton, ClickMarkerButton,
    ContoursButton, DeleteButton
}