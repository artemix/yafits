class CustomControls {
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(viewer) {
        CustomControls.enter(this.constructor.name);
        this.viewer = viewer;
        this.div = document.createElement("div");
        this.div.setAttribute("class", "btn-group-vertical customcontrols");
        
        $(`#${this.viewer.getDivId()} > .ol-viewport`).append(this.div);
        this.div.addEventListener("mouseenter", () => {
            this.viewer.stopCurrentInteraction(); 
            this.viewer.hasMouse = false;
        });

        this.div.addEventListener("mouseleave", () => {
            this.viewer.startCurrentInteraction(); 
            this.viewer.hasMouse = true;
        });
        CustomControls.exit();
    }

    addButton(button) {
        this.div.append(button);
    }

    removeButton(button) {
        if (this.div.contains(button)) {
            this.div.removeChild(button);
        }
    }
}

export{
    CustomControls
}