import {createAndAppendFromHTML} from "./utils.js";

class Help {
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }
    
    constructor(viewer, infosBlock) {
        Help.enter(this.constructor.name);
        this.infosBlock = infosBlock;
        this.viewerId = viewer.getDivId();
        this.ModalHelpId = `${this.viewerId}_ModalHelp`;
        this.ModalHelpBodyId = `${this.ModalHelpId}_body`;
        this.html = `
<div id="${this.ModalHelpId}" class="modal fade">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title">Help</h1>
      </div>
      <div id="${this.ModalHelpBodyId}" class="modal-body">
      </div> <!-- modal-body -->
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- #ModalHelpForm -->
`
        this.map = viewer.getMap();
        this.modal = createAndAppendFromHTML(this.html, $(this.map.getTargetElement()));
        this.button = document.createElement("button");
        this.button.setAttribute("type", "button");
        this.button.setAttribute("class", "btn btn-primary btn-sm");
        this.button.setAttribute("data-toggle", "modal");
        this.button.setAttribute("data-target", `#${this.ModalHelpId}`);
        this.button.setAttribute("data-tooltip", "tooltip");
        this.button.setAttribute("title", "Help");
        this.button.append("?");

        $(`#${this.ModalHelpId}`).on('shown', () => { this.infosBlock.setMode("Help") });
        $(`#${this.ModalHelpId}`).on('hide', () => { this.infosBlock.setMode("") });

        $(`#${this.ModalHelpBodyId}`).load("../html/olqv_help_2d.html", 
            (response, status, xhr) => {
                console.log("Back from load");
                if (status == "error") {
                    var msg = "Sorry but there was an error: ";
                    alert(msg + xhr.status + " " + xhr.statusText);
                }
                else
                    console.log("Help loaded"); 
            });
        Help.exit();
    }

    getButton() {
        return this.button;
    }
}

export {
  Help
}