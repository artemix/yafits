let getBasicStyle = (feature)=>{
    return {
        'LineString': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#cc3333',
                width: 2
            }),
            text: new ol.style.Text({
                font: '12px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({
                    color: '#fff', width: 1
                }),
                offsetX: 0,
                offsetY: 0,
                text: feature.get('label')
            })
        }),
        'Polygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#9e9d9d',
                width: 2
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255,0,0,0.1)'
            }),
            text: new ol.style.Text({
                font: '16px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({
                    color: '#fff', width: 2
                }),
                offsetX: 10,
                offsetY: -10,
                text: feature.get('label')
            })
        }),
        'Point': [
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 4,
                    stroke: new ol.style.Stroke({
                        color: '#3399CC',
                        width: 2
                    }),
                    fill: new ol.style.Fill({
                        color: "magenta"
                    })
                }),
                text: new ol.style.Text({
                    font: '16px Calibri,sans-serif',
                    fill: new ol.style.Fill({ color: '#000' }),
                    stroke: new ol.style.Stroke({
                        color: '#fff', width: 2
                    }),
                    offsetX: 10,
                    offsetY: -10,
                    text: feature.get('label')
                })
            })
        ]
    };
}

let getHighlightedStyle = (feature)=>{
    return {
        'LineString': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#cc3333',
                width: 3
            })/*,
            text: new ol.style.Text({
                font: '10px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({
                    color: '#fff', width: 2
                }),
                offsetX: 0,
                offsetY: 0,
                text: feature.get('label')
            })*/
        }),
        'Polygon': new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#9e9d9d',
                width: 3,
                lineDash: [1, 5]
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255,0,0,0.1)'
            }),
            text: new ol.style.Text({
                font: '16px Calibri,sans-serif',
                fill: new ol.style.Fill({ color: '#000' }),
                stroke: new ol.style.Stroke({
                    color: '#fff', width: 2
                }),
                offsetX: 10,
                offsetY: -10,
                text: feature.get('label')
            })
        }),
        'Point': [
            new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 4,
                    stroke: new ol.style.Stroke({
                        color: '#cc3333'
                    }),
                    fill: new ol.style.Fill({
                        color: "magenta"
                    })
                }),
                text: new ol.style.Text({
                    font: '16px Calibri,sans-serif',
                    fill: new ol.style.Fill({ color: '#000' }),
                    stroke: new ol.style.Stroke({
                        color: 'blue', width: 1
                    }),
                    offsetX: 10,
                    offsetY: -10,
                    text: feature.get('label')
                })
            })
        ]
    };
}

let highlightedFeature = (feature) => {
    let styles = getHighlightedStyle(feature);
    return styles[feature.getGeometry().getType()];
};

let standardFeature = (feature) => {
    let styles = getBasicStyle(feature);
    return styles[feature.getGeometry().getType()];
};



export {
    highlightedFeature, standardFeature
}