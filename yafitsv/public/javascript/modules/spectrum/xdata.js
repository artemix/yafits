
import {v2f,f2v} from "../utils.js";
import { FITS_HEADER } from '../fitsheader.js';
import { Constants } from "../constants.js";

/*
 ** Coordinate tabulators
 **
 **  - crval : value at reference point
 **  - cdelt : increment of abscissa
 **  - crpix : coordinate of reference point
 **  - n : tabulate at point of coordinate n
 */
 var linearTabulator = function(crval, cdelt, crpix, n) {
    return crval + (n - crpix) * cdelt;
}


class XDataCompute{
    constructor(){
        this.dataType = null
    }

    setStrategy(source){
        this.dataType = source;
    }

    computeSpectrum(numberOfPoints, velocity, redshift){
        return this.dataType.computeSpectrum(numberOfPoints, velocity, redshift);
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        return this.dataType.computeSummedSpectrum(numberOfPoints, velocity);
    }
}

class FrequencyXData{
    computeSpectrum(numberOfPoints, velocity, redshift){
        let xData = new Array(numberOfPoints, );
        let velo = FITS_HEADER.getVeloLsr();
        if(velo === undefined){
            velo = 0;                            
        }

        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            tmp = tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3] / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT[FITS_HEADER.ctype3]];             
            let value = tmp;
            if (velo === 0){
                if(redshift !== undefined && redshift !== '' && redshift !== 0) {
                    value = value  * (1+ redshift);
                }else{
                    if(velocity !== undefined){                                                                        
                        value =  tmp / (1 - ((velocity) / Constants.SPEED_OF_LIGHT)) ;                        
                    }else {
                        value =  tmp;
                    }                                
                }
            }else{
                value =  tmp / (1 - ((velocity - velo) / Constants.SPEED_OF_LIGHT)) ;
            }
            if (FITS_HEADER.cdelt3 > 0) {
                xData[numberOfPoints - i - 1] = value
            } else {
                xData[i] = value;
            }
        }
        return xData;
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        let xData = new Array(numberOfPoints);
        const checkedVelocity = velocity !== undefined ? velocity : 0 ;
        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            let tmpCenter = tmp; //+ (FITS_HEADER.restfreq - (FITS_HEADER.crval3+FITS_HEADER.naxis3/2*FITS_HEADER.cdelt3)) /*/ FITS_HEADER.cdelt3*/; /*- FITS_HEADER.naxis3 / 2 * FITS_HEADER.cdelt3*/ ;
            const frequency = tmpCenter * Constants.UNIT_FACTOR[FITS_HEADER.cunit3];
            //ALTRVAL
            let factor = 1;
            let tmp1;
            if(FITS_HEADER.isCASA()){
                tmp1 = f2v(frequency*factor, FITS_HEADER.restfreq, 0) / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT['VRAD']];
            }else{
                tmp1 = f2v(frequency*factor, FITS_HEADER.restfreq, checkedVelocity) / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT['VRAD']];
            }

            if (FITS_HEADER.cdelt3 > 0) {
                xData[numberOfPoints - i - 1] = tmp1;
            } else {
                xData[i] = tmp1;
            }
        }
        return xData;
    }
}

class VelocityXData{
    computeSpectrum(numberOfPoints, velocity, redshift){
        let xData = new Array(numberOfPoints);

        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            let tmp1 = v2f(tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3], FITS_HEADER.restfreq, velocity) / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT['FREQ']];
            if(velocity === 0){
                tmp1 = tmp1 * (1 + redshift);
            }else{

            }

            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmp1;
            } else {
                xData[numberOfPoints - i - 1] = tmp1;
            }
        }
        return xData;        
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3] / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT[FITS_HEADER.ctype3]];
            } else {
                xData[numberOfPoints - i - 1] = tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3] / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT[FITS_HEADER.ctype3]];
            }
        }
        return xData;
    }
}


class VeloLSRXData{
    computeSpectrum(numberOfPoints, velocity, redshift){
        let xData = new Array(numberOfPoints);

        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            let tmpvelo = v2f(tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3], FITS_HEADER.restfreq, 0 /*FITS_HEADER.crval3*/) / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT['FREQ']];
            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmpvelo;
            } else {
                xData[numberOfPoints - i - 1] = tmpvelo;
            }
        }
        return xData;        
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3] / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT[FITS_HEADER.ctype3]];
            } else {
                xData[numberOfPoints - i - 1] = tmp * Constants.UNIT_FACTOR[FITS_HEADER.cunit3] / Constants.UNIT_FACTOR[Constants.DEFAULT_OUTPUT_UNIT[FITS_HEADER.ctype3]];
            }
        }
        return xData;
    }
}

class WaveXData{
    computeSpectrum(numberOfPoints, velocity, redshift){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            xData[i] = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);       
        }
        return xData;        
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmp;
            } else {
                xData[numberOfPoints - i - 1] = tmp;
            }
        }
        return xData;
    }
}

class WavnXData{
    computeSpectrum(numberOfPoints, velocity, redshift){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            xData[i] = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);      
        }
        return xData;        
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmp;
            } else {
                xData[numberOfPoints - i - 1] = tmp;
            }
        }
        return xData;
    }
}

class AWavXData{
    computeSpectrum(numberOfPoints, velocity, redshift){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            xData[i] = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);          
        }
        return xData;        
    }

    computeSummedSpectrum(numberOfPoints, velocity){
        let xData = new Array(numberOfPoints);
        for (var i = 0; i < numberOfPoints; i++) {
            let tmp = linearTabulator(FITS_HEADER.crval3, FITS_HEADER.cdelt3, FITS_HEADER.crpix3, i + 1);
            if (FITS_HEADER.cdelt3 > 0) {
                xData[i] = tmp;
            } else {
                xData[numberOfPoints - i - 1] = tmp;
            }
        }
        return xData;
    }
}

export{
    XDataCompute, FrequencyXData, VelocityXData, VeloLSRXData, WaveXData, WavnXData, AWavXData
}