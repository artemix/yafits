
import { round}  from "../utils.js";
import { FITS_HEADER } from '../fitsheader.js';

class ChartLegend{
    constructor(){
        this.dataType = null
    }

    setStrategy(source){
        this.dataType = source;
    }

    defineSpectrumLegend(spectrum){        
        return this.dataType.defineSpectrumLegend(spectrum);
    }

    defineSummedSpectrumLegend(spectrum){
        return this.dataType.defineSummedSpectrumLegend(spectrum);
    }
}

class SitelleLegend{
    defineSpectrumLegend(spectrum){
        spectrum._xtitle = FITS_HEADER.ctype3 + " (" + FITS_HEADER.cunit3 + ")";
        spectrum._ytitle = "FLUX (" + FITS_HEADER.bunit + ")";
        spectrum.toptitle = "";        
    }

    defineSummedSpectrumLegend(spectrum){
        spectrum._xtitle = FITS_HEADER.ctype3 + " (" + FITS_HEADER.cunit3 + ")";
        spectrum._ytitle = "FLUX (" + spectrum._spectrumUnit + ")";
        spectrum._toptitle_unit = "ergs/s/cm^2";     
    }
}

class CasaLegend{
    defineSpectrumLegend(spectrum){
        let velocity = spectrum._spectroUI.getVelocity("km/s");

        if(velocity === 0 || velocity === undefined){
            if(spectrum._spectroUI.getRedshift() !== 0 && spectrum._spectroUI.getRedshift() !== undefined ){
                spectrum._xtitle = "Frequency (GHz) in " + FITS_HEADER.specsys + " frame at z= " + spectrum._spectroUI.getRedshift();
            }else{
                spectrum._xtitle = "Sky Frequency (GHz)" ;
            }                
        }else{
            spectrum._xtitle = "Frequency (GHz) in " + FITS_HEADER.specsys + " frame at v= " + velocity + "km/s";
        }            
        spectrum._ytitle = "Flux density (" + FITS_HEADER.bunit + ")";
        let coeff = Math.PI / 180. / 4.86e-6;
        spectrum.toptitle = "B: " + round(FITS_HEADER.bmaj * coeff, 1) + "x" +
            round(FITS_HEADER.bmin * coeff, 1) + " PA " +
            round(FITS_HEADER.bpa, 0) + "°";
    }

    defineSummedSpectrumLegend(spectrum){
        let prefix = "";

        if(FITS_HEADER.getVeloLsr() !== undefined && FITS_HEADER.getVeloLsr() !== 0){
            prefix = "LSR ";            
        }

        const flux_unit = FITS_HEADER.bmin !== undefined ? "(" + spectrum._spectrumUnit + ")" : "";

        spectrum._xtitle = prefix + " Velocity (km/s) - " + FITS_HEADER.specsys;
        spectrum._ytitle = "Int. flux density " + flux_unit;
        spectrum._toptitle_unit = "Jy.km/s"; 
    }
}

class GildasLegend{
    defineSpectrumLegend(spectrum){
        let velocity = spectrum._spectroUI.getVelocity("km/s");
        if(velocity === 0){
            if(spectrum._spectroUI.getRedshift() !== 0){
                spectrum._xtitle = "Frequency (GHz) in " + FITS_HEADER.specsys + " frame at z= " + spectrum._spectroUI.getRedshift();
            }else{
                spectrum._xtitle = "Sky Frequency (GHz)" ;
            }                
        }else{
            spectrum._xtitle = "Frequency (GHz) in " + FITS_HEADER.specsys + " frame at v= " + velocity + "km/s";
        }   
        spectrum._ytitle = "Flux density (" + FITS_HEADER.bunit + ")";
        spectrum.toptitle = "";
    }

    defineSummedSpectrumLegend(spectrum){
        let prefix = "";
        let suffix = "";
        
        if(FITS_HEADER.getVeloLsr() !== undefined && FITS_HEADER.getVeloLsr() !== 0){

            prefix = "LSR ";            
        }else{ 
            console.log(spectrum._spectroUI.getRedshift());
            suffix = " at z="+spectrum._spectroUI.getRedshift();
        }

        spectrum._xtitle = prefix + "Velocity (km/s)" + suffix;
        spectrum._toptitle_unit = "Jy.km/s";

        const flux_unit = FITS_HEADER.bmin !== undefined ? "(" + spectrum._spectrumUnit + ")" : "";

        // special case
        if (FITS_HEADER.isSpectrumInK()) {
            spectrum._ytitle = "Int. flux density (K)";
            spectrum._toptitle_unit = "K.km/s";
        }
        // common case
        else {
            spectrum._ytitle = "Int. flux density " + flux_unit;
        }       

        console.log(spectrum._xtitle);
    }
}

class MuseLegend{
    defineSpectrumLegend(spectrum){
        spectrum._xtitle = FITS_HEADER.ctype3 + " (" + FITS_HEADER.cunit3 + ")";
        spectrum._ytitle = "FLUX (" + FITS_HEADER.bunit + ")";
        spectrum.toptitle = "";
    }

    defineSummedSpectrumLegend(spectrum){
        spectrum._xtitle = FITS_HEADER.ctype3 + " (" + FITS_HEADER.cunit3 + ")";
        spectrum._ytitle = "Int. flux)";
        spectrum._toptitle_unit = "";
    }
}


class MiriadLegend{
    defineSpectrumLegend(spectrum){
        spectrum._xtitle = FITS_HEADER.ctype3 + " (" + FITS_HEADER.cunit3 + ")";
        spectrum._ytitle = "FLUX (" + FITS_HEADER.bunit + ")";
        spectrum.toptitle = "";     
    }

    defineSummedSpectrumLegend(spectrum){}
}

class MeerkatLegend{
    defineSpectrumLegend(spectrum){
        spectrum._xtitle = "Sky Frequency (GHz) - " + FITS_HEADER.specsys;
        spectrum._ytitle = "Flux density (" + FITS_HEADER.bunit + ")";
        spectrum.toptitle = "";        
    }

    defineSummedSpectrumLegend(spectrum){
        const flux_unit = FITS_HEADER.bmin !== undefined ? "(" + spectrum._spectrumUnit + ")" : "";
        spectrum._xtitle = "Velocity (km/s) - " + FITS_HEADER.specsys;
        spectrum._ytitle = "Int. flux density " + flux_unit;
        spectrum._toptitle_unit = "Jy.km/s";
    }
}


class NenufarLegend{
    defineSpectrumLegend(spectrum){
        let velocity = spectrum._spectroUI.getVelocity("km/s");
        spectrum._xtitle = "Frequency (GHz) in " + FITS_HEADER.specsys + " frame at v= " + velocity + "km/s";
        spectrum._ytitle = "Flux density (" + FITS_HEADER.bunit + ")";
        spectrum.toptitle = "";        
    }

    defineSummedSpectrumLegend(spectrum){}
}

export{
    ChartLegend, SitelleLegend, CasaLegend, GildasLegend, MuseLegend, MiriadLegend, MeerkatLegend, NenufarLegend
}