
import { FITS_HEADER } from '../fitsheader.js';
import { revertArray,  unitRescale} from "../utils.js";

class YDataCompute{
    constructor(){
        this.dataType = null
    }

    setStrategy(source){
        this.dataType = source;
    }

    computeSpectrum(values){
        return this.dataType.computeSpectrum(values);
    }

    computeSummedSpectrum(values, unit){
        return this.dataType.computeSummedSpectrum(values, unit);
    }
}

class FrequencyYData{
    computeSpectrum(values){
        let result = null;
        if (FITS_HEADER.cdelt3 > 0) {
            result = revertArray(values);
        } else {
            result = values;
        }
        return result;
    }

    computeSummedSpectrum(values, unit){
        if (FITS_HEADER.cdelt3 > 0) {
            return revertArray(values);
        } else {
            return values;
        }
    }
}

class VelocityYData{
    computeSpectrum(values){
        let result = null;
        if (FITS_HEADER.cdelt3 > 0) {
            result = values;
        } else {
            result = revertArray(values);
        }

        return result; 
    }

    computeSummedSpectrum(values, unit){
        let averageSpectrum = values.map((x) => {
            return x * unitRescale(unit);
        });
        if (FITS_HEADER.cdelt3 > 0) {
            return averageSpectrum;
        } else {
            return revertArray(averageSpectrum);
        }
    }
}


class VeloLSRYData{
    computeSpectrum(values){
        let result = null;
        if (FITS_HEADER.cdelt3 > 0) {
            result = values;
        } else {
            result = revertArray(values);
        }
        
        return result;
    }

    computeSummedSpectrum(values, unit){
        let averageSpectrum = values.map((x) => {
            return x * unitRescale(unit);
        });
        if (FITS_HEADER.cdelt3 > 0) {
            return averageSpectrum;
        } else {
            return revertArray(averageSpectrum);
        }
    }
}

class WaveYData{
    computeSpectrum(values){
        return values;
    }

    computeSummedSpectrum(values, unit){
        // alert("ctype3 case not implemented : " + FITS_HEADER.ctype3);
        let averageSpectrum = values.map((x) => {
            return x * unitRescale(unit);
        });
        if (FITS_HEADER.cdelt3 > 0) {
            return averageSpectrum;
        } else {
            return revertArray(averageSpectrum);
        }
    }
}

class WavnYData{
    computeSpectrum(values){
        return values;
    }

    computeSummedSpectrum(values, unit){
        // alert("ctype3 case not implemented : " + FITS_HEADER.ctype3);
        let averageSpectrum = values.map((x) => {
            return x * unitRescale(unit);
        });
        if (FITS_HEADER.cdelt3 > 0) {
            return averageSpectrum;
        } else {
            return revertArray(averageSpectrum);
        }
    }
}

class AWavYData{
    computeSpectrum(values){
        return values;
    }

    computeSummedSpectrum(values, unit){
        let averageSpectrum = values.map((x) => {
            return x * unitRescale(unit);
        });
        if (FITS_HEADER.cdelt3 > 0) {
            return averageSpectrum;
        } else {
            return revertArray(averageSpectrum);
        }
    }
}

export{
    YDataCompute, FrequencyYData, VelocityYData, VeloLSRYData, WaveYData, WavnYData, AWavYData
}