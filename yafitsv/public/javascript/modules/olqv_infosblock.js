/*
**
** an InfosBlock is made of :
** * a div, infosLine, which is expected to contain some summarized information along with a link to make visible a modal containing a complete version of the information
** * a modal which contains the complete version of the information.
**
** infosLine is passed as a parameter to the constructor, while the modal is created by the constructor.
**
** infosLine is exected to display in that order : a mode - a title - a link to make visible the complete information.
**
**
** Author : Michel Caillat
** Date : 6 may 2020
**
**/
import {createAndAppendFromHTML} from "./utils.js";
class InfosBlock {
    static enter(what) {
        console.group(this.name + "." + what);
    }

    static exit() {
        console.groupEnd();
    }

    constructor(infosLine, parent, context) {
        InfosBlock.enter(this.constructor.name);
        this.infosLine = infosLine;
        this.context = context;
        this.preamble = {};
        ['file', 'url', 'pixel-slice-range', 'slice-phys-chars'].forEach(element => {
            if (element in context) {
                this.preamble[element] = context[element];
            }

        });
        this.infosBlockId=`ModalInfosBlock_${this.infosLine.id}`;
        this.infosBlockTitleId=`ModalInfosBlockTitle_${this.infosLine.id}`;
        this.infosBlockBodyId = `ModalInfosBlockBody_${this.infosLine.id}`;
        this.html = `
<div class="modal fade" id="${this.infosBlockId}" tabindex="-1" role="dialog" aria-labelledby="${this.infosBlockTitleId}" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="${this.infosBlockTitleId}">Infos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="${this.infosBlockBodyId}">
      </div>
      <textarea style="height:0px;width:0px;opacity:0">
      </textarea>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm fas fa-files-o" data-tooltip="tooltip" title="Copy verbatim"></button>
        <button type="button" class="btn btn-primary btn-sm">Copy as JSON</button>
        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>`;
        this.infosBlock = createAndAppendFromHTML(this.html, parent); // viewer.getMap().getTargetElement());
        this.infosBlockTitle = document.getElementById(this.infosBlockTitleId);
        this.infosBlockBody = document.getElementById(this.infosBlockBodyId);
        this.hiddenInfosBlock = this.infosBlock.querySelector("textarea");
        this.verbatimCopyBtn = this.infosBlock.querySelectorAll("button")[1]
        this.jsonCopyBtn = this.infosBlock.querySelectorAll("button")[2];

        this.jsonCopyBtn.onclick = this.jsonCopy.bind(this);
        this.verbatimCopyBtn.onclick = this.verbatimCopy.bind(this);
        this.mode = "";
        this.infosLine.innerHTML = "&nbsp;";
        this.sections = [];
        $(`#${this.infosBlockId}`).on('shown.bs.modal', function () { console.log("Modal display"); });
        this.accordionId = `accordion-${this.infosLine.id}`;

        InfosBlock.exit();
    }

    /*
    ** A convenience method to format the output of float numerical values
    */
    format(floatValue) {
        let result = floatValue;
        if (typeof result === "number" && !Number.isInteger(result)) {
            result = result.toExponential(4);
        }
        return result;
    };

    /*
    ** Return the context
    */
   getContext() { return this.context; }

    /*
    ** Return the preamble
    */
   getPreamble() { return this.preamble; }

    /*
    ** Define the mode 
    */
    setMode(mode) {
        InfosBlock.enter(this.setMode.name);
        this.mode = mode;
        this.infosLine.innerHTML = `<span> Active mode : ${this.mode} </span>`;
        InfosBlock.exit();
    }

    /*
    ** Return the mode
    */
   getMode() {
       InfosBlock.enter(this.getMode.name);
       InfosBlock.exit();
       return this.mode;
   }

    /*
    ** Return the infosLine element
    */
    getInfosLine() {
        InfosBlock.enter(this.getInfosLine.name);
        InfosBlock.exit();
        return this.infosLine;
    }

    /*
    ** Define the title displayed in the infosLine
    **
    */
    headline(title) {
        InfosBlock.enter(this.headline.name);
        this.infosLine.innerHTML = `${this.mode}`;
        this.infosLine.innerHTML += ` - ${title}`;
        this.infosLine.innerHTML += ` <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#${this.infosBlockId}"> See more</button>`;
        InfosBlock.exit();
    }

    preamble2HTML() {
        InfosBlock.enter(this.preamble2HTML.name);
        let x = "";
        /*
        if ('file' in this.preamble) {
            x += `file: ${this.preamble['file']} <br>`;
        }
        */
        if ('url' in this.preamble) {
            x += `url : <a href="${this.preamble['url']}" target=_blank> ${this.preamble['url']} </a><br>`;
        }
        if ('pixel-slice-range' in this.preamble) {
            x += `slice range: ${this.preamble['pixel-slice-range']}<br>`;
        }
        if ('slice-phys-chars' in this.preamble) {
            if (['type', 'value', 'unit'].every(element => element in this.preamble['slice-phys-chars'])) {
                x += `${this.preamble['slice-phys-chars']['type']} : ${this.preamble['slice-phys-chars']['value']} ${this.preamble['slice-phys-chars']['unit']} <br>`;
            };
        }
        InfosBlock.exit();
        return x;
    }

    preamble2TXT() {
        InfosBlock.enter(this.preamble2TXT.name);
        let x = "";
        /*
        if ('file' in this.preamble) {
            x += `file: ${this.preamble['file']} \r\n`;
        }
        */
        if ('url' in this.preamble) {
            x += `url: ${this.preamble['url']}\r\n`;
        }
        if ('pixel-slice-range' in this.preamble) {
            x += `slice range: ${this.preamble['pixel-slice-range']}\r\n`;
        }
        if ('slice-phys-chars' in this.preamble) {
            if (['type', 'value', 'unit'].every(element => element in this.preamble['slice-phys-chars'])) {
                x += `${this.preamble['slice-phys-chars']['type']} : ${this.preamble['slice-phys-chars']['value']} ${this.preamble['slice-phys-chars']['unit']} \r\n`;
            };
        }
        InfosBlock.exit();
        return x;
    }

    preamble2DICT() {
        InfosBlock.enter(this.preamble2DICT.name);
        let x = {};
        /*
        if ('file' in this.preamble) {
            $.extend(x, {'file': `${this.preamble['file']}`});
        }
        */
        if ('url' in this.preamble) {
            $.extend(x, {'url' : `${this.preamble['url']}`});
        }
        let d = {};
        if ('pixel-slice-range' in this.preamble) {
            $.extend(d, {'slice range': `${this.preamble['pixel-slice-range']}`});
        }
        if ('slice-phys-chars' in this.preamble) {
            if (['type', 'value', 'unit'].every(element => element in this.preamble['slice-phys-chars'])) {
                $.extend(d, this.preamble['slice-phys-chars']);
            };
        }
        $.extend(x, d);
        InfosBlock.exit(); 
        return x;       
    }

    /*
    ** Fills the infosBlock
    **
    ** * title : a general string
    ** * collection : a collection of {"key":{"value": .. , "unit": ..}} [optional] 
    */
    populate(title, collection=null) {
        InfosBlock.enter(this.populate.name);
        this.title = title;
        this.collection = collection;
        this.infosBlockBody.innerHTML = `<b>${title}</b><br><br>`;
        this.infosBlockBody.innerHTML += this.preamble2HTML();
        this.infosBlockBody.innerHTML += "<br>";
        if ( collection ) {
            this.collection = collection;
            for (var k in collection) {
                this.infosBlockBody.innerHTML += k + ":" + this.format(collection[k]["value"]) + " " + collection[k]["unit"] + "<br>";
            }
            this.sections = null;       
        }
        else {
            this.collection = null;
        }
        InfosBlock.exit()
    }

    openSectionList() {
        InfosBlock.enter(this.openSectionList.name);
        this.infosBlockBody.innerHTML += `
<div class="panel-group" id="${this.accordionId}">
`;
        this.sections.length = 0;
        InfosBlock.exit();
    }

    /*
    ** Append sections in the infosBlock
    **
    ** * title : the section's title
    ** * collections : an array of array of {"key":{"value": .. , "unit": ..}}
    */
    section(title, collections) {
        InfosBlock.enter(this.section.name);
        this.sections.push({ 'title': title, 'collections': collections });

        let x = "";
        collections.forEach((collection) => {
            for (var k in collection) {
                x += k + ":" + this.format(collection[k]["value"]) + " " + collection[k]["unit"] + "<br>";
            }
            x += '<br>';
        });

        let content = `
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#${this.accordionId}" href="#${title}">${title}</a>
            </h4>
        </div>
        <div id="${title}" class="panel-collapse collapse in show">
            <div class="panel-body"> 
                ${x}
            </div>
        </div>
    </div>
        `;

        document.getElementById(`${this.accordionId}`).innerHTML += `${content}`;
        InfosBlock.exit()
    }

    /**
     * Delete summarized and complete version of the information
     */
    clear() {
        InfosBlock.enter(this.clear.name);
        this.mode = "undefined";
        this.infosLine.innerHTML = "";
        this.infosBlockBody.innerHTML = "";
        InfosBlock.exit();
    }

    /**
    * Make a verbatim copy version of the information and stores it in the hidden textarea to be used by the copy/paste mechanism
    */
    verbatimCopy() {
        InfosBlock.enter(this.verbatimCopy.name);
        this.hiddenInfosBlock.value = `${this.title}\r\n\r\n`;
        this.hiddenInfosBlock.value += this.preamble2TXT();

        if (this.collection) {
            for (var k in this.collection) {
                const newLocal = `${k}:${this.format(this.collection[k]["value"])} ${this.collection[k]["unit"]}\r\n`;
                this.hiddenInfosBlock.value += newLocal;
            }
        }
        else if (this.sections) {
            for (var i in this.sections) {
                this.hiddenInfosBlock.value += `${this.sections[i]["title"]}\r\n`;;
                this.sections[i]["collections"].forEach((collection) => {
                    for (var k in collection) {
                        this.hiddenInfosBlock.value += `${k}:${this.format(collection[k]["value"])} ${collection[k]["unit"]}\r\n`;
                    }
                    this.hiddenInfosBlock.value += "\r\n";                   
                });
                this.hiddenInfosBlock.value += "\r\n";
            }
        }

        this.hiddenInfosBlock.select();
        try {
            var success = document.execCommand('copy');
        }
        catch (err) {
            alert("Copy failed. " + err);
        }
        InfosBlock.exit();
    }

    /**
     * Make a json version of the information and stores it in the hidden textarea to be used by the copy/paste mechanism
     */
    jsonCopy() {
        InfosBlock.enter(this.jsonCopy.name);
        if (this.collection) {
            this.hiddenInfosBlock.value = JSON.stringify($.extend(this.preamble2DICT(), this.collection), 0, 4);
        }
        else if (this.sections) {
            let d = {};
            this.sections.forEach((s) => {
                d[s["title"]] = s["collections"];
            })
            this.hiddenInfosBlock.value = JSON.stringify($.extend(this.preamble2DICT(), d), 0, 4);
        }
        this.hiddenInfosBlock.select();
        try {
            var success = document.execCommand('copy');
        }
        catch (err) {
            alert("Copy failed. " + err);
        }
        InfosBlock.exit();
    }

    set shownAction(action) {
        InfosBlock.enter("set shownAction");
        $(`#${this.infosBlockId}`).on('shown.bs.modal', action);
        InfosBlock.exit();
    }
}

export{
    InfosBlock
}