
import {DOMAccessor} from "./domelements.js";

const withSAMP = JSON.parse(DOMAccessor.getSampConnectionStatus().textContent);
const testMode = JSON.parse(DOMAccessor.getTestModeStatus().textContent);
const yafitsTarget = JSON.parse(DOMAccessor.getYafitsTarget().textContent);
const URL_ROOT = JSON.parse(DOMAccessor.getUrlRoot().textContent);
const relFITSFilePath = JSON.parse(DOMAccessor.getFitsFilePath().textContent);
const header = JSON.parse(DOMAccessor.getHeader().textContent);
const product = JSON.parse(DOMAccessor.getProduct().textContent);
const default_lut_index = JSON.parse(DOMAccessor.getDefaultLUTIndex().textContent);
const URL_SPECTRO_SERVER = DOMAccessor.getSpectroServerUrl().textContent.trim();
const URL_3D_TO_1D = URL_ROOT + "/visit/?relFITSFilePath=//SPECTRUM";
const URL_3D_TO_2D = URL_ROOT + "/visit/?relFITSFilePath=//IMG";


// split cube path to get file name
let fileParts = relFITSFilePath.split("/");

/**
 * Stores path to datafile that will be used later
 */
let dataPaths = {
    // averaged spectrum (bottom right image)
    averageSpectrum: "",
    relFITSFilePath: relFITSFilePath,
    // slice PNG file (top left image)
    relSlicePNG: "",
    // summed slice PNG file (bottom left image)
    relSummedSlicesPNG: "",
    // opened file name
    fileName : fileParts[fileParts.length-1],
    // spectrum (top right image)
    spectrum: ""
};

export {
    withSAMP,
    testMode,
    yafitsTarget,
    dataPaths,
    header,
    product,
    default_lut_index,
    URL_ROOT,
    URL_SPECTRO_SERVER, 
    URL_3D_TO_1D,
    URL_3D_TO_2D
};