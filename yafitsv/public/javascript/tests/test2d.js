import { FITS_HEADER } from '../modules/fitsheader.js';
import { dataPaths } from '../modules/init.js';
import { degToArcsec, printVal } from '../modules/utils.js';
import {Header, Image, Spectrum, IntegratedValue } from './testModel.js'
import { ServerApi } from "../modules/serverApi.js";


class Run{
    constructor(){
        this.image = new Image();
    }
}

class Test2D{
    constructor(){
        this.source = "";
        this.yafitsVersion = "";
        this.header = new Header();
        this.data = new Run();
    }
}

class Tester{
    constructor(){
        this._result = new Test2D();
        this._result.source = dataPaths.fileName;
        this._getHeaderValues();
    }

    setYafitsVersion(value){
        this._result.yafitsVersion = value;
    }

    _getHeaderValues(){
        this._result.header.bmin.value = degToArcsec(FITS_HEADER.bmin);
        this._result.header.bmin.unit = "arcsec";
        this._result.header.bmaj.value = degToArcsec(FITS_HEADER.bmaj);
        this._result.header.bmaj.unit = "arcsec";
    }

    imageLoaded(event){
        this._result.data.image.rms.value = event.rmsValue;
        this._result.data.image.rms.unit = event.rmsUnit;
        this._result.data.image.min = event.minValue;
        this._result.data.image.max = event.maxValue;
        /*image.refPixel.RA = event.sliceRefRaDec.ra;
        image.refPixel.DEC = event.sliceRefRaDec.dec;*/
        this._result.data.image.refPixel.x = event.xRef;
        this._result.data.image.refPixel.y = event.yRef;

        let extent = event.boxExtent;
        this._result.data.image.box.width = Math.abs(extent[0]-extent[2]);
        this._result.data.image.box.height = Math.abs(extent[1]-extent[3]);
        this._result.data.image.box.xmin = extent[0];
        this._result.data.image.box.ymin = extent[1];
        this._getPixelValue(event.xRef, event.yRef);
        
    }

    _saveTest(){
        $.post("", {
            "method": "saveTest",
            "fileName": dataPaths.fileName,
            "testData": JSON.stringify(this._result),
            "cubeType": "2d"
        }).done((resp) => {
        }).fail(function(xhr, status, error) {
            console.log(status);
            console.log(error);
        });
    }

    _getPixelValue = (iRA, iDEC) =>{
        let self = this;

        function processResult(data){
            self._result.data.image.refPixel.flux.value = data.result.value;
            self._result.data.image.refPixel.flux.unit = data.result.unit;
            self._saveTest();
        }
        let serverApi = new ServerApi();
        if(FITS_HEADER.naxis == 2){
            serverApi.getPixelValue(dataPaths.relFITSFilePath, iRA, iDEC, processResult);
        }else if(FITS_HEADER.naxis >= 3){
            serverApi.getPixelFreqValue(dataPaths.relFITSFilePath, iRA, iDEC, 0, processResult);
        }

    }
}

export{
    Tester
}