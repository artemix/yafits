import { FITS_HEADER } from '../modules/fitsheader.js';
import { dataPaths } from '../modules/init.js';
import { degToArcsec, printVal } from '../modules/utils.js';
import {Header, Image, Spectrum, IntegratedValue } from './testModel.js'


class Run{
    constructor(){
        this.upperImage = new Image();
        this.lowerImage = new Image();
        this.upperSpectrum = new Spectrum();
        this.lowerSpectrum = new Spectrum();
        this.lowerIntegratedValues = new IntegratedValue();
    }
}

class Test3D{
    constructor(){
        this.source = "";
        this.yafitsVersion = "";
        this.header = new Header();
        this.data = new Run();
    }
}

class Tester{
    constructor(viewLinker){
        this._viewLinker = viewLinker;
        this._result = new Test3D();
        this._result.source = dataPaths.fileName;
        this._getHeaderValues();

        this._steps = {
            slices : 0,
            spectra : 0,
        }
    }

    setYafitsVersion(value){
        this._result.yafitsVersion = value;
    }

    _getHeaderValues(){
        this._result.header.bmin.value = degToArcsec(FITS_HEADER.bmin);
        this._result.header.bmin.unit = "arcsec";
        this._result.header.bmaj.value = degToArcsec(FITS_HEADER.bmaj);
        this._result.header.bmaj.unit = "arcsec";
    }

    sliceLoaded(event){
        let image = null;
        if(event.type === "single"){
            image = this._result.data.upperImage;
        } else if(event.type === "summed"){
            image = this._result.data.lowerImage;
            let extent = event.polygon.getExtent();
            image.box.width = Math.abs(extent[0]-extent[2]) + 1;
            image.box.height = Math.abs(extent[1]-extent[3]) + 1;
            image.box.xmin = extent[0];
            image.box.ymin = extent[1];
        } else{
            throw new Error("Unknown event type in sliceLoaded : " + event.type );
        }

        image.rms.value = event.rmsValue;
        image.rms.unit = event.rmsUnit;
        image.channel = event.channel;
        image.min = event.sliceMinValue;
        image.max = event.sliceMaxValue;
        image.refPixel.RA = event.sliceRefRaDec.ra;
        image.refPixel.DEC = event.sliceRefRaDec.dec;
        image.refPixel.x = event.sliceRefXi;
        image.refPixel.y = event.sliceRefYi;
        this._getPixelValue(image);
    }

    spectrumLoaded(event){
        let spectrum = null;
        if(event.type === "single"){
            spectrum = this._result.data.upperSpectrum;
            /*this._result.data.lowerIntegratedValues.freqmin.value = event.freqmin;
            this._result.data.lowerIntegratedValues.freqmin.unit = event.unit;
            this._result.data.lowerIntegratedValues.freqmax.value = event.freqmax;
            this._result.data.lowerIntegratedValues.freqmax.unit = event.unit;*/
            this._result.header.JyperK = event.jyperk;
            spectrum.refPixel.x = event.iRA;
            spectrum.refPixel.y = event.iDEC;
            spectrum.rms.value = event.rmsValue;
            spectrum.rms.unit = event.unit;
            spectrum.min = event.minValue;
            spectrum.max = event.maxValue;
            spectrum.freqMin = event.freqMin;
            spectrum.freqMax = event.freqMax;
        } else if(event.type === "summed"){
            spectrum = this._result.data.lowerSpectrum;
            this._result.data.lowerIntegratedValues.flux.value = event.fluxValue;
            this._result.data.lowerIntegratedValues.flux.unit = event.fluxUnit;
            this._result.data.lowerIntegratedValues.vmin.value = event.vminValue;
            this._result.data.lowerIntegratedValues.vmin.unit = event.vminUnit;
            this._result.data.lowerIntegratedValues.vmax.value = event.vmaxValue;
            this._result.data.lowerIntegratedValues.vmax.unit = event.vmaxUnit;
            this._result.data.lowerIntegratedValues.imin = event.imin;
            this._result.data.lowerIntegratedValues.imax = event.imax;
            spectrum.rms.value = event.rmsValue;
            spectrum.rms.unit = event.unit;
            spectrum.min = event.minValue;
            spectrum.max = event.maxValue;
        }else if(event.type === "bounds"){
            spectrum = this._result.data.lowerSpectrum;
            spectrum.imin = event.iFreqMin;
            spectrum.imax = event.iFreqMax;
            /*spectrum.freqmin = event.freqMin;
            spectrum.freqmax = event.freqMax;*/
            spectrum.vmin = event.velMin;
            spectrum.vmax = event.velMax;
        }
          else{
            throw new Exception("Unknown event type in spectrumLoaded");
        }

        this._steps.spectra += 1;
        this._saveTest();
    }

    _getPixelValue = (image) =>{
        let self = this;
        $.get("getPixelValueAtiFreqiRAiDEC", {
            "relFITSFilePath": self._viewLinker.relFITSFilePath,
            "iRA": image.refPixel.x,
            "iDEC": image.refPixel.y,
            "iFREQ": image.channel
        }).done(
            (resp) => {
                if(resp.status === true){
                    if(resp.result !== null){
                        image.refPixel.flux.value = resp.result.value;
                        image.refPixel.flux.unit = resp.result.unit;
                        self._steps.slices += 1;
                    }else{
                        console.log("WARNING : Pixel value is null in test3d:_getPixelValue");
                        image.refPixel.flux.value = null;
                        image.refPixel.flux.unit = null;
                        self._steps.slices += 1;
                    }
                    self._saveTest();
                }else{
                    console.log(resp);
                    throw Error("Error while getting pixel value");
                }
            });
    }

    _saveTest(){
        if(this._steps.slices >= 2 && this._steps.spectra >= 3){
            $.post("", {
                "method": "saveTest",
                "fileName": dataPaths.fileName,
                "testData": JSON.stringify(this._result),
                "cubeType": "3d"
            }).done((resp) => {
            }).fail(function(xhr, status, error) {
                console.log(status);
                console.log(error);
            });
        }
    }
}

export{
    Tester
}