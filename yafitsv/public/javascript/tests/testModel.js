
class Value{
    constructor(){
        this.value = null;
        this.unit = null;
    }
}


class Header{
    constructor(){
        this.JyperK = null;
        this.bmin = new Value();
        this.bmaj = new Value();
    }
}

class Pixel{
    constructor(){
        this.x = null;
        this.y = null;
    }
}

class RefPixel extends Pixel{
    constructor(){
        super();
        this.RA = null;
        this.DEC = null;
        this.flux = new Value();
    }
}

class Image{
    constructor(){
        this.rms = new Value();
        this.channel = null;
        this.min = null;
        this.max = null;
        this.refPixel = new RefPixel();
        this.box = {
            xmin : null,
            ymin : null,
            width : null, 
            height : null
        };

    }
}

class Spectrum{
    constructor(){
        this.rms = new Value();
        this.refPixel = new Pixel();
        this.min = null;
        this.max = null;
        this.imin = null;
        this.imax = null;
        this.vmin = null;
        this.vmax = null;
        this.freqMin = null;
        this.freqMax = null;
    }
}

class IntegratedValue{
    constructor(){
        this.flux = new Value();
        this.vmin = new Value();
        this.vmax = new Value();
        this.imin = null;
        this.imax = null;
        this.freqmin = new Value();
        this.freqmax = new Value();
    }
}

export{
    Header, Image, Spectrum, IntegratedValue
}