import {DecDeg2HMS, HMS2DecDeg, DecDeg2DMS, DMS2DecDeg} from "../modules/utils.js";


QUnit.module('DecDeg2HMS', hooks => {
  QUnit.test('180 degrees', assert => {
      assert.equal(DecDeg2HMS(180), "12:0:0.000");
    });

  QUnit.test('change separator', assert => {
    assert.equal(DecDeg2HMS(180, ";"), "12;0;0.000");
  });

  QUnit.test('0 degree', assert => {
      assert.equal(DecDeg2HMS(0), "0:0:0.000");
    });

    QUnit.test('-180 degrees', assert => {
      assert.false(DecDeg2HMS(-180), "-12:0:0.000");
    });
});

QUnit.module('HMS2DecDeg', hooks => {
  QUnit.test('12:00:00.000', assert => {
    assert.equal(HMS2DecDeg('12:00:00.000'), 180);
  });

QUnit.test('Malformed hour', function (assert) {
      assert.throws(
        function () { HMS2DecDeg('12:00') },
        function (err) { return err.toString() === 'Invalid format for H:M:S value' },
        'Error thrown'
      );
  });
});


QUnit.module('DecDeg2DMS', hooks => {
  QUnit.test('180', assert => {
    assert.equal(DecDeg2DMS('180'), "+180:0:0.000");
  });

  QUnit.test('-180', assert => {
      assert.equal(DecDeg2DMS('-180'), "-180:0:0.000");
    });

    QUnit.test('43.255', assert => {
      assert.equal(DecDeg2DMS('43.255'), "+43:15:18.000");
    });
});



QUnit.module('DMS2DecDeg', hooks => {
  QUnit.test('+180:0:0.000', assert => {
    assert.equal(DMS2DecDeg('+180:0:0.000'), 180);
  });

  QUnit.test('+180;0;0.000 ; separator', assert => {
      assert.equal(DMS2DecDeg('+180;0;0.000', ";"), 180);
  }); 

  QUnit.test('-180:0:0.000', assert => {
      assert.equal(DMS2DecDeg('-180:0:0.000'), 180);
  }); 

  QUnit.test('Malformed declination', function (assert) {
      assert.throws(
        function () { DMS2DecDeg('+180') },
        function (err) { return err.toString() === 'Invalid format for D:M:S value' },
        'Error thrown'
      );
  });
});
