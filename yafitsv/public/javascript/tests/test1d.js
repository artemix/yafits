import { FITS_HEADER } from '../modules/fitsheader.js';
import { dataPaths } from '../modules/init.js';
import { degToArcsec, printVal } from '../modules/utils.js';
import {Header, Image, Spectrum, IntegratedValue } from './testModel.js';

class Run{
    constructor(){
        this.spectrum = new Spectrum();
    }
}

class Test1D{
    constructor(){
        this.source = "";
        this.yafitsVersion = "";
        this.header = new Header();
        this.data = new Run();
    }
}

class Tester{
    constructor(){
        this._result = new Test1D();
        this._result.source = dataPaths.fileName;
        this._getHeaderValues();
    }

    setYafitsVersion(value){
        this._result.yafitsVersion = value;
    }

    _getHeaderValues(){
        this._result.header.bmin.value = degToArcsec(FITS_HEADER.bmin);
        this._result.header.bmin.unit = "arcsec";
        this._result.header.bmaj.value = degToArcsec(FITS_HEADER.bmaj);
        this._result.header.bmaj.unit = "arcsec";
    }

    spectrumLoaded(event){
        let spectrum = null;
        spectrum = this._result.data.spectrum;
        spectrum.rms.value = event.rmsValue;
        spectrum.rms.unit = event.unit;
        spectrum.min = event.minValue;
        spectrum.max = event.maxValue;
        spectrum.freqMin = event.freqMin;
        spectrum.freqMax = event.freqMax;
        spectrum.refPixel.x = event.iRA;
        spectrum.refPixel.y = event.iDEC;
        this._saveTest();
    }

    _saveTest(){
        $.post("", {
            "method": "saveTest",
            "fileName": dataPaths.fileName,
            "testData": JSON.stringify(this._result),
            "cubeType": "1d"
        }).done((resp) => {
        }).fail(function(xhr, status, error) {
            console.log(status);
            console.log(error);
        });    
    }
}

export{
    Tester
}