import {ProjUtils, Projection, GnomonicProjection, OrthoGraphicProjection, 
  AzimutalProjection, RadioProjection, getProjection, ProjEnum} from '../modules/olqv_projections.js';

QUnit.module('Factory', hooks => {
  QUnit.test('GnomonicProjection', assert => {
      let proj = Projection.create(0, 0, Math.PI, ProjEnum.TAN);
      assert.true(proj instanceof GnomonicProjection);
    });

    QUnit.test('OrthoGraphicProjection', assert => {
      let proj = Projection.create(0, 0, Math.PI, ProjEnum.SIN);
      assert.true(proj instanceof OrthoGraphicProjection);
    });

    QUnit.test('AzimutalProjection', assert => {
      let proj = Projection.create(0, 0, Math.PI, ProjEnum.ARC);
      assert.true(proj instanceof AzimutalProjection);
    });

    QUnit.test('AzimutalProjection String', assert => {
      let proj = Projection.create(0, 0, "TOTO", ProjEnum.ARC);
      assert.true(proj instanceof AzimutalProjection);
    });

    QUnit.test('RadioProjection', assert => {
      let proj = Projection.create(0, 0, 0, ProjEnum.GLS);
      assert.true(proj instanceof RadioProjection);
    });

    QUnit.test('RadioProjection invalid angle', function (assert) {
      assert.throws(
        function () { Projection.create(0, 0, Math.PI, ProjEnum.GLS); },
        function (err) { return err.toString() === `RadioProjection does not support a projection angle, value must be 0` },
        'Error thrown'
      );
    });

    QUnit.test('Unimplemented projection', function (assert) {
      assert.throws(
        function () { Projection.create(0, 0, Math.PI, ProjEnum.PCO); },
        function (err) { return err.toString() === `Projection of type ${ProjEnum.PCO} is not yet implemented.` },
        'Error thrown'
      );
    });

    QUnit.test('Unknown projection', function (assert) {
      assert.throws(
        function () { Projection.create(0, 0, Math.PI, "UNDEF"); },
        function (err) { return err.toString() === 'Undefined projection type' },
        'Error thrown'
      );
    });
});


QUnit.module('Projection', hooks => {
  QUnit.test('Initialiation a0', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._a0 === 0);
    });

    QUnit.test('Initialiation d0', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._d0 === 1);
    });

    QUnit.test('Initialiation angle', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._angle === Math.PI);
    });

    QUnit.test('Initialiation sina0', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._sina0 === Math.sin(0));
    });

    QUnit.test('Initialiation cosa0', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._cosa0 === Math.cos(0));
    });

    QUnit.test('Initialiation sind0', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._sind0 === Math.sin(1));
    });

    QUnit.test('Initialiation cosd0', assert => {
      let proj = Projection.create(0, 1, Math.PI, ProjEnum.TAN);
      assert.true(proj._cosd0 === Math.cos(1));
    });
});

