import { velocity2Redshift, redshift2Velocity, getRadiusInDegrees, unitRescale } from "./modules/utils.js";
import { withSAMP, testMode, dataPaths, URL_ROOT, header, default_lut_index } from "./modules/init.js";
import { SpectroscopyUI, SpectroscopyFormatter, changeLinesGroup } from "./modules/olqv_spectro.js";
import { FITS_HEADER } from "./modules/fitsheader.js";
import { Constants } from "./modules/constants.js";
import { FITSHeaderTable, LicensePanel } from "./modules/page_overlay.js";
import { getViewLinker } from "./modules/olqv_linkedviews.js";
import { SourceTable, computeDl } from "./modules/olqv_ned.js";
import { MarkerList } from "./modules/olqv_markers.js";
import { DOMAccessor } from "./modules/domelements.js";
import {Tester} from "./tests/test3d.js";
import { ServerApi, SpectroApi } from "./modules/serverApi.js";
import { Projection } from "./modules/olqv_projections.js";
import { setNedUiListeners, setSpectroUiListeners, initSpectroUI, initCubeSmoothing, initSamp } from "./modules/listeners.js";

let viewLinker;

// list of sources downloaded from NED catalog
let sourceTable;

// get user input from spectroscopy form
let spectroUI = new SpectroscopyUI();
// perform spectroscopy requests
let spectroq = new SpectroApi();
let optionsMenuDisplay = "none";

/**
 *  Removes the selected box on the summedslice viewer when user push X key on keyboard
 *
 * @param {Event} evt
 */
$(document).ready(function() {
    console.log("$( document ).ready(function() {: entering");  

    // Set up everything required to fill and display the FITS header.
    let FITSHDR = new FITSHeaderTable(document.getElementById("fitshdr"), header);
    //Software licenses
    let Licences = new LicensePanel(document.getElementById("licences"));

    // fill species autocomplete accordingly
    //spectroUI.disable();
    // by default spectro UI checkbox is hidden
    //spectroUI.hideToggler();
    // check that there are lines in spectro db
    // UI remains hidden if nothing is found
    spectroq.getStatuses((results)=>{
        for (let result of results) {
            spectroUI.toggleDatabaseSelector(result.db, !result.isempty);
            spectroUI.addDatabase(result.db, !result.isempty);
        }
        spectroUI.selectDefaultDatabase();
        spectroq.getMetadata(function(metadata) { spectroUI.initSpeciesSelection(spectroUI.getSelectedDatabase(), metadata) });
    });


    // remove box selection in slice viewer
    //document.addEventListener('keydown', deleteFeature, false);
    $('#LUT-selector').prop('selectedIndex', default_lut_index);

    initSpectroUI(spectroUI);
    initCubeSmoothing();
    DOMAccessor.showLoaderAction(true);


    // buttons to navigate list of spectral lines
    $("#goto-previous-group").on("click", function() {
        changeLinesGroup(viewLinker.summedPixelsSpectrumViewer.linePlotter, spectroUI, spectroUI.currentGroup - 1);
    });

    $("#goto-next-group").on("click", function() {
        changeLinesGroup(viewLinker.summedPixelsSpectrumViewer.linePlotter, spectroUI, spectroUI.currentGroup + 1);
    });

    $("#goto-last-group").on("click", function() {
        changeLinesGroup(viewLinker.summedPixelsSpectrumViewer.linePlotter, spectroUI, viewLinker.summedPixelsSpectrumViewer.linePlotter.transitionGroups.length - 1);
    });

    $("#goto-first-group").on("click", function() {
        changeLinesGroup(viewLinker.summedPixelsSpectrumViewer.linePlotter, spectroUI, 0);
    });


    // user wants to display all lines for a dataset
    document.getElementById("selectedspecies").addEventListener("getdataset", function(event) {
        spectroq.getDataset(event.detail.db, event.detail.dataset, (result) => {

            const obsFreqMin = viewLinker.summedPixelsSpectrumViewer.spectrumChart.obsFreqMin;
            const obsFreqMax = viewLinker.summedPixelsSpectrumViewer.spectrumChart.obsFreqMax;

            if (event.detail.db !== Constants.SPECTRO_COLLECTIONS.local) {
                try{
                    const formatter = new SpectroscopyFormatter();
                    const content = formatter.linesTofile(result, spectroUI.getVelocity(), obsFreqMin, obsFreqMax);
                    document.getElementById("spetro-modal-lines").innerText = content;
                    $('#spectro-modal').modal('show');
                }catch(e){
                    alert(e);
                }

            } else {
                alert("Not available for local database");
            }

        })
    });

    $("#chart-info-title").html(FITS_HEADER.getSpectrumTitle());
    $("#cube-infos").html(FITS_HEADER.getFitsSummary(false));

    $('#show-fits-header').click(function() {
        FITSHDR.show()
    });

    $('#show-license').click(function() {
        Licences.show()
    });

    // zoom buttons for both spectra
    $("#zoom-in").on('click', function() {
        let factor = Math.abs((viewLinker.spectrumViewer.spectrumChart.xAxis[0].min - viewLinker.spectrumViewer.spectrumChart.xAxis[0].max) * 0.2);
        let minVal = viewLinker.spectrumViewer.spectrumChart.xAxis[0].min + factor;
        let maxVal = viewLinker.spectrumViewer.spectrumChart.xAxis[0].max - factor;
        viewLinker.spectrumViewer.spectrumChart.xAxis[0].setExtremes(minVal, maxVal);
    });

    $("#zoom-out").on('click', function() {
        let factor = Math.abs((viewLinker.spectrumViewer.spectrumChart.xAxis[0].min - viewLinker.spectrumViewer.spectrumChart.xAxis[0].max) * 0.2);
        let minVal = viewLinker.spectrumViewer.spectrumChart.xAxis[0].min - factor;
        let maxVal = viewLinker.spectrumViewer.spectrumChart.xAxis[0].max + factor;
        viewLinker.spectrumViewer.spectrumChart.xAxis[0].setExtremes(minVal, maxVal);
    });

    $("#zoom-reset").on('click', function() {
        viewLinker.spectrumViewer.spectrumChart.xAxis[0].setExtremes(undefined, undefined);
    });

    $("#slice").on('mouseenter', function() {
        viewLinker.singleSliceImage.map_controls.forEach(function(ctrl) {
            viewLinker.singleSliceImage._map.addControl(ctrl);
        });
    });

    $("#slice").on('mouseleave', function() {
        viewLinker.singleSliceImage.map_controls.forEach(function(ctrl) {
            viewLinker.singleSliceImage._map.removeControl(ctrl);
        });
    });

    $("#summed-slices").on("mouseenter", function() {
        viewLinker.summedSlicesImage.map_controls.forEach(function(ctrl) {
            viewLinker.summedSlicesImage._map.addControl(ctrl);
        });
    });

    $("#summed-slices").on("mouseleave", function() {
        viewLinker.summedSlicesImage.map_controls.forEach(function(ctrl) {
            viewLinker.summedSlicesImage._map.removeControl(ctrl);
        });
    });

    $("#toggle-options").on("click", function(event) {       
        if(optionsMenuDisplay ==="block" ){
            optionsMenuDisplay = "none";
            viewLinker.spectrumViewer.setSpectrumSize(Constants.PLOT_WIDTH_3D_LARGE, Constants.PLOT_HEIGHT_RATIO_3D_LARGE);
            viewLinker.summedPixelsSpectrumViewer.setSpectrumSize(Constants.PLOT_WIDTH_3D_LARGE, Constants.PLOT_HEIGHT_RATIO_3D_LARGE);
            event.currentTarget.textContent = "<<<";

        }else{
            optionsMenuDisplay = "block";
            viewLinker.spectrumViewer.setSpectrumSize(Constants.PLOT_WIDTH_3D, Constants.PLOT_HEIGHT_RATIO_3D);
            viewLinker.summedPixelsSpectrumViewer.setSpectrumSize(Constants.PLOT_WIDTH_3D, Constants.PLOT_HEIGHT_RATIO_3D);
            event.currentTarget.textContent = ">>>";
        }

        Array.from(document.getElementsByClassName("last-col")).forEach(function(element){
            element.style.display = optionsMenuDisplay;
        });

    });

    DOMAccessor.getVelocityField().addEventListener("keyup", function(event) {
        viewLinker.isRefreshable = false;
        if(event.key === 'Enter'){
            viewLinker.isRefreshable = true;
            if(viewLinker.spectrumViewer !== null){
                viewLinker.spectrumViewer.refresh();
                viewLinker.summedPixelsSpectrumViewer.refresh();
            }
        }  
    });

    DOMAccessor.getRedshiftField().addEventListener("keyup", function(event) {
        viewLinker.isRefreshable = false;
        if(event.key === 'Enter'){
            viewLinker.isRefreshable = true;
            if(viewLinker.spectrumViewer !== null){
                viewLinker.spectrumViewer.refresh();
                viewLinker.summedPixelsSpectrumViewer.refresh();
            }
        }
    });


    // Shows SAMP connector logo if SAMP is activated
    initSamp(withSAMP);
    
    /**
        Get RA/DEC ranges corresponding to opened fits file
        response format is a pair of pairs of float values `[[ra0, dec0], [ra1, dec1], [ra2, dec2]]`
        expressing angles in decimal degrees where :

        * `ra0` corresponds to the index 0 along the RA axis.
        * `ra1` corresponds to the index NAXIS1-1 along the RA axis.
        * `dec0` corresponds to the index 0 along the DEC axis.
        * `dec1` corresponds to the index NAXIS2-1 along the DEC axis.
        * `ra2` corresponds to the index max(NAXIS1, NAXIS2)-1 along the RA axis.
        * `dec2` corresponds to the index max(NAXIS1, NAXIS2)-1 along the DEC axis.
    */
    let serverApi = new ServerApi();
    serverApi.getRADECRangeInDegrees(dataPaths.relFITSFilePath, (resp)=>{
        let markerList = new MarkerList("input-markers", "show-markers", "clear-markers");
        //ned data selection
        sourceTable = new SourceTable('ned-data', 'ned-modal-title', spectroUI);
        sourceTable.addListener(markerList);

        viewLinker = getViewLinker(resp.data["result"], spectroUI, sourceTable, markerList);
        $('#rccap').click(function() {
            viewLinker.refresh()
        });

        setNedUiListeners(sourceTable, spectroUI);
        setSpectroUiListeners(spectroUI);
        
        if(FITS_HEADER.isGILDAS() && FITS_HEADER.velolsr !== undefined){
            // velolsr unit not specified in header, is in m/s
            const z = velocity2Redshift(FITS_HEADER.velolsr * unitRescale("m/s"));
            spectroUI.setVelocity(FITS_HEADER.velolsr * unitRescale("m/s"));
            spectroUI.setRedshift(0);
            viewLinker.isRefreshable = true;
            // velolsr set as velocity is displayed in upper spectrum title
            viewLinker.spectrumViewer.refreshChartLegend();
            viewLinker.summedPixelsSpectrumViewer.refreshChartLegend();
            if(z > Constants.MIN_REDSHIFT_FOR_CALC){
                spectroUI.setDl(computeDl(spectroUI.getHubbleCst(),
                spectroUI.getMassDensity(),
                z));
            }
        }

        if(testMode){
            let tester = new Tester(viewLinker);
            tester.setYafitsVersion(DOMAccessor.getYafitsVersion());
            viewLinker.singleSliceImage.addSliceLoadedListener(tester);
            viewLinker.summedSlicesImage.addSliceLoadedListener(tester);

            viewLinker.spectrumViewer.addSpectrumLoadedListener(tester);
            viewLinker.summedPixelsSpectrumViewer.addSpectrumLoadedListener(tester);
        }
        Projection.print = true;
        console.log('$.post("", {"method": "RADECRangeInDegrees", "relFITSFilePath": dataPaths.relFITSFilePath}).done(function(resp) {: exiting');
    });
    console.log("$( document ).ready(function() {: exiting");
});
