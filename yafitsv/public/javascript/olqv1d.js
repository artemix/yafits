import { unitRescale } from "./modules/utils.js";
import { SpectroscopyUI, SpectroscopyFormatter, changeLinesGroup } from './modules/olqv_spectro.js';
import { withSAMP, testMode, header, default_lut_index } from './modules/init.js';
import { FITS_HEADER } from './modules/fitsheader.js';
import { Constants } from "./modules/constants.js";
import { FITSHeaderTable, LicensePanel } from "./modules/page_overlay.js";
import { SourceTable, computeDl } from "./modules/olqv_ned.js";
import { getSingleSpectrum1D } from "./modules/spectrum/olqv_spectrum.js";
import { DOMAccessor } from "./modules/domelements.js";
import { SpectroApi } from "./modules/serverApi.js";
import {Tester} from "./tests/test1d.js";
import { setNedUiListeners, setSpectroUiListeners, initSpectroUI, 
         initSamp, initCubeSmoothing } from "./modules/listeners.js";
import { setOnHubAvailabilityButtons1D } from "./modules/samp_utils.js";


// get user input from spectroscopy form
let spectroUI = new SpectroscopyUI();
// list of sources downloaded from NED catalog
let sourceTable;
// perform spectroscopy requests
let spectroq = new SpectroApi();
let spectrumViewer;
let optionsMenuDisplay = "none";


$(document).ready(function() {
    console.log("$( document ).ready(function() {: entering");
    initSpectroUI(spectroUI);

    /**
    * This loads the file by asking for the RA/DEC range
    */
    //let serverApi = new ServerApi();
    //serverApi.getRADECRangeInDegrees(dataPaths.relFITSFilePath, (resp)=>{
    sourceTable = new SourceTable('ned-data', 'ned-modal-title', spectroUI);
    
    if(FITS_HEADER.isGILDAS() && FITS_HEADER.velolsr !== undefined){
        // velolsr unit not specified in header, is in m/s
        spectroUI.setVelocity(FITS_HEADER.velolsr * unitRescale("m/s"));
        spectroUI.setRedshift(0);

        const z = spectroUI.getRedshift();

        if(z > Constants.MIN_REDSHIFT_FOR_CALC){
        spectroUI.setDl(computeDl(spectroUI.getHubbleCst(),
                                    spectroUI.getMassDensity(),
                                    z));
        }
    }
    spectrumViewer = getSingleSpectrum1D(spectroUI, sourceTable, withSAMP);
    spectrumViewer.isRefreshable = true;


    if(testMode){
        let tester = new Tester();
        tester.setYafitsVersion(DOMAccessor.getYafitsVersion());
        spectrumViewer.addSpectrumLoadedListener(tester);
    }
    //});


    // Set up everything required to fill and display the FITS header.
    let FITSHDR = new FITSHeaderTable(document.getElementById("fitshdr"), header);
    //Software licenses
    let Licences = new LicensePanel(document.getElementById("licences"));

    setNedUiListeners(sourceTable, spectroUI);
    setSpectroUiListeners(spectroUI);
    initCubeSmoothing();

    // local db is selected by default for spectroscopy

    // fill species autocomplete accordingly
    //spectroUI.disable();
    // by default spectro UI checkbox is hidden
    //spectroUI.hideToggler();
    // check that there are lines in spectro db
    // UI remains hidden if nothing is found
    spectroq.getStatuses((results)=>{
        for (let result of results) {
            spectroUI.toggleDatabaseSelector(result.db, !result.isempty);
            spectroUI.addDatabase(result.db, !result.isempty);
        }
        spectroUI.selectDefaultDatabase();
        spectroq.getMetadata(function(metadata) { spectroUI.initSpeciesSelection(spectroUI.getSelectedDatabase(), metadata) });
    });


    DOMAccessor.getVelocityField().addEventListener("keyup", function(event) {
        spectrumViewer.isRefreshable = false;
        if(event.key === 'Enter'){
            spectrumViewer.isRefreshable = true;
            if(spectrumViewer !== null){
                spectrumViewer.refresh("v");
            }
        }  
    });

    DOMAccessor.getRedshiftField().addEventListener("keyup", function(event) {
        spectrumViewer.isRefreshable = false;
        if(event.key === 'Enter'){
            spectrumViewer.isRefreshable = true;
            if(spectrumViewer !== null){
                spectrumViewer.refresh("z");
            }
        }
    });

    // remove box selection in slice viewer
    $('#LUT-selector').prop('selectedIndex', default_lut_index);
    DOMAccessor.showLoaderAction(true);


    // buttons to navigate list of spectral lines
    $("#goto-previous-group").on("click", () => {
        changeLinesGroup(spectrumViewer.linePlotter, spectroUI, spectroUI.currentGroup - 1);
    });

    $("#goto-next-group").on("click", () => {
        changeLinesGroup(spectrumViewer.linePlotter, spectroUI, spectroUI.currentGroup + 1);
    });

    $("#goto-last-group").on("click", () => {
        changeLinesGroup(spectrumViewer.linePlotter, spectroUI, spectrumViewer.activePlotter.transitionGroups.length - 1);
    });

    $("#goto-first-group").on("click", () => {
        changeLinesGroup(spectrumViewer.linePlotter, spectroUI, 0);
    });


    // user wants to display all lines for a dataset
    document.getElementById("selectedspecies").addEventListener("getdataset", (event) => {
        spectroq.getDataset(event.detail.db, event.detail.dataset, (result) => {

            const obsFreqMin = spectrumViewer.obsFreqMin;
            const obsFreqMax = spectrumViewer.obsFreqMax;

            if (event.detail.db !== Constants.SPECTRO_COLLECTIONS.local) {
                try{
                    const formatter = new SpectroscopyFormatter();
                    const content = formatter.linesTofile(result, spectroUI.getVelocity(), obsFreqMin, obsFreqMax);
                    document.getElementById("spetro-modal-lines").innerText = content;
                    $('#spectro-modal').modal('show');
                }catch(e){
                    alert(e);
                }
            } else {
                alert("Not available for local database");
            }

        })
    });


    $("#spectrum-info-title").html(FITS_HEADER.getSpectrumTitle());
    $("#cube-infos").html(FITS_HEADER.getFitsSummary(false));

    $('#show-fits-header').click(function() {
        FITSHDR.show()
    });

    $('#show-license').click(function() {
        Licences.show()
    });

    $("#zoom-reset").on('click', () => {
        spectrumViewer.resetZoom();
    });


    // Shows SAMP connector logo if SAMP is activated
    initSamp(withSAMP);
    if(withSAMP){
        setOnHubAvailabilityButtons1D(DOMAccessor.get3DSampConnection());
    }

    $("#toggle-options").on("click", function(event) {
       
        if(optionsMenuDisplay ==="block" ){
            optionsMenuDisplay = "none";
            spectrumViewer.setSpectrumSize(Constants.PLOT_WIDTH_1D_LARGE, Constants.PLOT_HEIGHT_RATIO_1D_LARGE);
            spectrumViewer.setDetailedSpectrumSize(Constants.PLOT_WIDTH_1D_LARGE, Constants.PLOT_HEIGHT_RATIO_1D_LARGE);
            event.currentTarget.textContent = "<<<";

        }else{
            optionsMenuDisplay = "block";
            spectrumViewer.setSpectrumSize(Constants.PLOT_WIDTH_1D, Constants.PLOT_HEIGHT_RATIO_1D);
            spectrumViewer.setDetailedSpectrumSize(Constants.PLOT_WIDTH_1D, Constants.PLOT_HEIGHT_RATIO_1D);
            event.currentTarget.textContent = ">>>";
        }

        const elements = document.getElementsByClassName("last-col");
        for(let i=0; i < elements.length; i++){
            elements.item(i).style.display = optionsMenuDisplay;            
        }

    });
    console.log("$( document ).ready(function() {: exiting");
});
