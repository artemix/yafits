var log4js = require('log4js');
// Logging streams
const YAFITSV_ACCESS_LOGPATH = `${process.env.HOME}/log/yafitsv-access-${process.env.PORT}.log`;
const YAFITSV_LOGGER_LOGPATH = `${process.env.HOME}/log/yafitsv-${process.env.PORT}.log`;

const YAFITS_MAXLOGSIZE_DFLT = 1024 * 1024
let YAFITS_MAXLOGSIZE = 0
YAFITS_MAXLOGSIZE = parseInt(process.env.YAFITS_MAXLOGSIZE);
if (isNaN(YAFITS_MAXLOGSIZE)) {
    YAFITS_MAXLOGSIZE = YAFITS_MAXLOGSIZE_DFLT;
    console.log(`YAFITS_MAXLOGSIZE is forced to ${YAFITS_MAXLOGSIZE_DFLT} bytes`);
}

const YAFITS_LOGBACKUPCOUNT_DFLT = 2
let YAFITS_LOGBACKUPCOUNT = 0
YAFITS_LOGBACKUPCOUNT = parseInt(process.env.YAFITS_LOGBACKUPCOUNT);
if (isNaN(YAFITS_LOGBACKUPCOUNT)) {
    YAFITS_LOGBACKUPCOUNT = YAFITS_LOGBACKUPCOUNT_DFLT;
    console.log(`YAFITS_LOGBACKUPCOUNT is forced to ${YAFITS_LOGBACKUPCOUNT_DFLT} bytes`);
}

// The loggers
log4js.configure({
    appenders: {
        logger: { type: 'file', maxLogSize: YAFITS_MAXLOGSIZE, backups: YAFITS_LOGBACKUPCOUNT, filename: YAFITSV_LOGGER_LOGPATH },
        access: { type: 'file', maxLogSize: YAFITS_MAXLOGSIZE, backups: YAFITS_LOGBACKUPCOUNT, filename: YAFITSV_ACCESS_LOGPATH }
    },
    categories: {
        access: { appenders: ['access'], level: 'debug' },
        default: { appenders: ['logger'], level: 'debug' }
    }
});

// One logger for the HTTP requests
var accessLogger = log4js.getLogger('access');

// One logger for the code debug/infos messages
var logger = log4js.getLogger();

logger.debug(
    `Log file max size set to ${YAFITS_MAXLOGSIZE} and log backup count set to ${YAFITS_LOGBACKUPCOUNT}`)

module.exports = {
    logger: logger,
    connectAccess: (app) => {
        app.use(log4js.connectLogger(accessLogger, { level: 'auto' }));
    }
}
