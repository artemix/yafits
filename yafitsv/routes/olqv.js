var express = require('express');
const {logger } = require('../logger');
var fs = require('fs'), PNG = require('pngjs').PNG;
var router = express.Router();
const path = require("path");
const uuidv1 = require('uuid/v1');
const request = require('request');
const url = require('url');
const obj2gltf = require('obj2gltf');
const dirs = require('../dirs')
const http = require('http');

const PNG_ROOT_DIR = dirs.PNG_ROOT_DIR;
const OBJ_ROOT_DIR = dirs.OBJ_ROOT_DIR;
const SAMP_DIR = dirs.SAMP_DIR;

let yafitssHost = process.env.YAFITSS_HOST;
let yafitssPort = process.env.YAFITSS_PORT;
let httpProtocol = process.env.HTTP_PROTOCOL;

console.log(`Hi I'm olqv. I'll collaborate with ...${yafitssHost}:${yafitssPort} ...for the FITS files service`);

/*
** To use SAMP or not to use SAMP
*/
let useSAMP = process.env.YAFITSV_USESAMP.trim() === "true";
let testMode = process.env.YAFITSV_TESTMODE.trim() === "true";
let yafitsTarget = process.env.YAFITS_TARGET.trim();
let yafitsVersion = process.env.YAFITS_VERSION.trim();
logger.info(`Using SAMP ... ${useSAMP}`);

var renderingCapabilities = null;

function condAss(value, predicate, defaultValue) {
  return predicate(value) ? defaultValue : value;
};

function cleanFileName(fileName){
  return fileName.replaceAll(" ", "+");
}

/***************************************************/
/* Define the interface with the HTTP FITS server. */
/***************************************************/

var clienthttp = {
  server:  "http://" + yafitssHost + ":" + yafitssPort + "/artemix",

  setData: function (relFITSFilePath, sessionID, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/setData?relFITSFilePath=" + relFITSFilePath, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  degToHMSDMS: function (relFITSFilePath, iRA, iDEC, sessionID = 0, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/degToHMSDMS?relFITSFilePath=" + relFITSFilePath + "&iRA=" + iRA + "&iDEC=" + iDEC, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  rangeToHMS: function (relFITSFilePath, sessionID, iRA0, iRA1, iRAstep, callback)   {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/rangeToHMS?relFITSFilePath=" + relFITSFilePath + "&iRA0=" + iRA0 + "&iRA1=" + iRA1 + "&iRAstep=" + iRAstep, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  rangeToDMS: function (relFITSFilePath, sessionID, iDEC0, iDEC1, iDECstep, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/rangeToDMS?relFITSFilePath=" + relFITSFilePath + "&iDEC0=" + iDEC0 + "&iDEC1=" + iDEC1 + "&iDECstep=" + iDECstep, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getSlice: function (relFITSFilePath, sessionID, iFREQ, step, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getSlice?relFITSFilePath=" + relFITSFilePath + "&iFREQ=" + iFREQ, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getAverage: function (relFITSFilePath, iRA0, iRA1, iDEC0, iDEC1, iFREQ0, iFREQ1, retFITS, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getAverage?relFITSFilePath=" + relFITSFilePath + "&iFREQ0=" + iFREQ0 + "&iFREQ1=" + iFREQ1 + "&iRA0=" + iRA0 + "&iRA1=" + iRA1 + "&iDEC0=" + iDEC0 + "&iDEC1=" + iDEC1 + "&retFITS=" + retFITS,
      { json: true }, function (error, response, body) {
        callback(error, response, body);
      });
  },

  getSpectrum: function (relFITSFilePath, sessionID, iRA, iDEC, iFREQ0, iFREQ1, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getSpectrum?relFITSFilePath=" + relFITSFilePath + "&iRA=" + iRA + "&iDEC=" + iDEC + "&iFREQ0=" + iFREQ0 + "&iFREQ1=" + iFREQ1,
      { json: true }, function (error, response, body) {
        callback(error, response, body);
      });
  },

  getPixelValueAtiRAiDEC: function (relFITSFilePath, iRA, iDEC, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getPixelValueAtiRAiDEC?relFITSFilePath=" + relFITSFilePath + "&iRA=" + iRA + "&iDEC=" + iDEC,
      { json: true }, function (error, response, body) {
        callback(error, response, body )
      });
  },  

  getPixelValueAtiFreqiRAiDEC: function (relFITSFilePath, iRA, iDEC, iFREQ, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getPixelValueAtiFreqiRAiDEC?relFITSFilePath=" + relFITSFilePath + "&iRA=" + iRA + "&iDEC=" + iDEC + "&iFREQ=" + iFREQ,
      { json: true }, function (error, response, body) {
        callback(error, response, body )
      });
  },

  createFits: function (relFITSFilePath, iRA, iDEC, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iRA": iRA,
      "iDEC": iDEC
    };
    request.post(this.server + "/createFits", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getAverageSpectrum: function (relFITSFilePath, sessionID, iDEC0, iDEC1, iRA0, iRA1, retFITS, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getAverageSpectrum?relFITSFilePath=" + relFITSFilePath + "&iDEC0=" + iDEC0 + "&iDEC1=" + iDEC1 + "&iRA0=" + iRA0 + "&iRA1=" + iRA1 + "&retFITS=" + retFITS,
      { json: true }, function (error, response, body) {
        callback(error, response, body);
      });
  },

  getSumOverSliceRectArea: function (relFITSFilePath, sessionID, iFREQ, iRA0, iRA1, iDEC0, iDEC1, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getSumOverSliceRectArea?relFITSFilePath=" + relFITSFilePath +
      "&sessionID=" + sessionID +
      "&iFREQ=" + iFREQ +
      "&iRA0=" + iRA0 +
      "&iRA1=" + iRA1 +
      "&iDEC0=" + iDEC0 +
      "&iDEC1=" + iDEC1,
      { json: true },
      function (error, response, body) {
        callback(error, response, body)
      });
  },

  RADECRangeInDegrees: function (relFITSFilePath, sessionID, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/RADECRangeInDegrees?relFITSFilePath=" + relFITSFilePath, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getHeader: function (relFITSFilePath, sessionID, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    request(this.server + "/getHeader?relFITSFilePath=" + relFITSFilePath, { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getOneSliceAsPNG: function (relFITSFilePath, iFREQ, ittName, lutName, vmName, sessionID, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ": iFREQ,
      "ittName": ittName,
      "lutName": lutName,
      "vmName": vmName,
      "sessionID": sessionID
    };
    request.post(this.server + "/getOneSliceAsPNG", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getSummedSliceRangeAsPNG: function (relFITSFilePath, iFREQ0, iFREQ1, ittName, lutName, vmName, sessionID, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ0": iFREQ0,
      "iFREQ1": iFREQ1,
      "ittName": ittName,
      "lutName": lutName,
      "vmName": vmName,
      "sessionID": sessionID
    };
    request.post(this.server + "/getSummedSliceRangeAsPNG", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  createFITSSliceImage: function (relFITSFilePath, iFREQ, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ": iFREQ
    };
    request.post(this.server + "/createFITSSliceImage", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  createFITSSumSliceImage: function (relFITSFilePath, iFREQ0,iFREQ1, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ0": iFREQ0,
      "iFREQ1": iFREQ1
    };
    request.post(this.server + "/createFITSSumSliceImage", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  createSmoothCube: function (relFITSFilePath,nbox, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "nbox": nbox
    };
    request.post(this.server + "/createSmoothCube", { json: true, body: input }, function (error, response, body) {      
      callback(error, response, body);
    });
  },

  getContours: function (relFITSFilePath, optParams, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = { "relFITSFilePath": relFITSFilePath, "optParams": optParams };
    request.post(this.server + "/getContours", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  getYtObj: function (relFITSFilePath, iRA0, iRA1, iDEC0, iDEC1, iFREQ0, iFREQ1, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    // retrieve the product name, i.e. the simple filename without extension nor prefix.    
    var product = path.parse(relFITSFilePath).name;
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "product": product,
      "iRA0": iRA0,
      "iRA1": iRA1,
      "iDEC0": iDEC0,
      "iDEC1": iDEC1,
      "iFREQ0": iFREQ0,
      "iFREQ1": iFREQ1
    };
    request.post(this.server + "/getYtObj", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  measureContour: function (relFITSFilePath, iFREQ, contour, level, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = { "relFITSFilePath": relFITSFilePath, "iFREQ": iFREQ, "contour": contour, "level": level };
    request.post(this.server + "/measureContour", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });  
  },

  measureBox: function (relFITSFilePath, iFREQ, iRA0, iRA1, iDEC0, iDEC1, callback) {
    relFITSFilePath = cleanFileName(relFITSFilePath)
    let input = {
      "relFITSFilePath": relFITSFilePath,
      "iFREQ": iFREQ,
      "iRA0": iRA0, "iRA1": iRA1,
      "iDEC0": iDEC0, "iDEC1": iDEC1
    };
    request.post(this.server + "/measureBox", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  renderingCapabilities: function (callback) {
    request(this.server + "/renderingCapabilities", { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  ping: function (callback) {
    request(this.server + "/ping", { json: true }, function (error, response, body) {
      callback(error, response, body);
    });
  },
  saveTest: function (fileName, testData, cubeType, callback) {    
    let input = {
      "fileName": fileName.replaceAll(" ", "+"),
      "testData": testData,
      "cubeType": cubeType
    };
    request.post(this.server + "/saveTest", { json: true, body: input }, function (error, response, body) {
      callback(error, response, body);
    });
  },

  /*getYtObj : function(relFITSFilePath) {
    let input = {
      "relFITSFilePath": relFITSFilePath
    };
    request.post(this.server+"/getYtObj", {json: true, body:input}, function(error, response, body){
      callback(error, response, body);
    });
  }*/
}


// Utility function to determine  will be the actual root of the URLs
// The logic below WORKS ONLY for situations like :
// http://some_host/some_prefix/<rest of the URL> where "some_prefix" must be seen as a part of the host part when a redirection is done.
// http://some_host/<rest of the URL> in classical situation.

var URLRoot = function (anurl) {
  logger.info("URLRoot: entering");
  var result=process.env.YAFITSV_URLROOT?process.env.YAFITSV_URLROOT:`${process.env.YAFITSV_HOST}:${process.env.YAFITSV_PORT}`;
  if (!result.startsWith(httpProtocol + '://')) {result = `${httpProtocol}://${result}`};
  logger.info(`URLRoot = ${result}`); 
  logger.info("URLRoot: exiting");
  return result;
};


/******************/
/*   GET routes   */
/******************/


var filterHttpResponse = function (res, error, response, body) {
  if (error) {
    var message = error.toString();
    res.send(JSON.stringify({ "status": false, "message": "Internal error : " + message + ". Maybe the server is simply not running", "result": null }));
  }
  else if (response["statusCode"] == 500) {
    res.send(JSON.stringify({ "status": false, "message": response["body"] }));
  }
  else if (body["status"] == false) {
    res.send(JSON.stringify({ "status": false, "message": body["message"] }));
  }
  return;
};

/* GET home page. */
router.get('/',

  /* Check that the FITS server is up and waiting*/
  function (req, res, next) {
    logger.info("pinq http request callback: arming");
    clienthttp.ping((error, response, body) => {
      logger.info("pinq http request callback: entering")
      filterHttpResponse(res, error, response, body);
      logger.info("FITS server is up and waiting " + JSON.stringify(body, 0, 4));
      logger.info("pinq http request callback: exiting")
      next();
    });
    logger.info("pinq http request callback: armed");
  },

  /* Retrieve the rendering capabilities */
  function (req, res, next) {
    logger.info("renderingCapabilities http request callback: arming");
    clienthttp.renderingCapabilities((error, response, body) => {
      logger.info("renderingCapabilities http request callback: entering")
      filterHttpResponse(res, error, response, body);
      logger.info("renderingCapabilities http request callback: exiting")
      renderingCapabilities = body["result"];
      next();
    });
    logger.info("renderingCapabilities http request callback: armed");
  },

  function (req, res) {
    logger.info("getHeader http request callback: arming");
    var relFITSFilePath = req.query.relFITSFilePath;
    var product = path.parse(req.query.relFITSFilePath).name;

    //
    // Obtain the FITS header.
    //
    clienthttp.getHeader(relFITSFilePath, req.query.sessionID ? req.query.sessionID : 0, (error, response, body) => {
      logger.info("getHeader http request callback: entering")
      if (error) {
        var message = error.toString();
        res.send(JSON.stringify({ error: message }));
        return;
      }
      else if (body["status"] == false) {
        //
        // If the FITS is not present in the server's memory,
        // this means that the FITS file is not yet loaded 
        // then ask the FITS server to load it.
        var params = {
          title: 'Loading ' + relFITSFilePath,
          relFITSFilePath: relFITSFilePath,
          useSAMP: useSAMP,
          testMode: testMode,
          yafitsTarget : yafitsTarget,
          yafitsVersion : yafitsVersion,
          urlRoot: URLRoot(),
          originalURL: req.originalUrl
        }
        res.render('setData', params);
        return;
      }
      else {
        //
        // Yes we have the header, so the file is present in the FITS server memory,
        // then we can proceed to its visualization.
        //
        let header_s = body["result"];
        let header = JSON.parse(header_s);
        useSAMP = useSAMP && (header["INSTRUME"] != "SITELLE");
        var params = {
          title: 'View of ' + relFITSFilePath,
          relFITSFilePath: relFITSFilePath,
          product: product,
          header: header,
          useSAMP: useSAMP,
          testMode: testMode,
          yafitsTarget : yafitsTarget,
          yafitsVersion : yafitsVersion,
          renderingCapabilities: renderingCapabilities,
          yafitsvPort: process.env.YAFITSV_PORT,
          spectrosUrlRoot: process.env.SPECTROS_URLROOT,
          httpProtocol: httpProtocol,
          spectrosPath: process.env.SPECTROS_PATH,
          urlRoot: URLRoot(),
          originalURL: req.originalUrl
        };

        // Let's render the page depending on the dimensionality of the dataset.
        // Ok it's a regular 3D dataset
        if ((header["NAXIS"] == 3 &&  header["NAXIS1"] > 1 && header["NAXIS2"] > 1 && header["NAXIS3"] > 1) 
        || (header["NAXIS"] == 4 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1 && header["NAXIS3"] > 1 )) { //&& header["NAXIS4"] == 1)) {
          res.render('olqv3d', params);
        } 
        // Ok it's a 2D dataset
        else if ((header["NAXIS"] == 3 && header["NAXIS3"] == 1 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1) 
                  ||(header["NAXIS"] == 2 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1)
                  ||(header["NAXIS"] == 4 && header["NAXIS1"] > 1 && header["NAXIS2"] > 1 && header["NAXIS3"] == 1 )) { //&& header["NAXIS4"] == 1)) {
          res.render('olqv2d', params)
        // 1D dataset
        }else if((header["NAXIS"] === 3 && header["NAXIS1"] === 1 && header["NAXIS2"] === 1 && header["NAXIS3"] > 1) ){
          res.render('olqv1d', params)
        }
        // Ok it's something that we don't know how to display
        else {
          let params = {
            title: 'No display',
            relFITSFilePath: relFITSFilePath,
            header: header
          }
          res.render('olqv_error', params);
        }
        return;
      }
      logger.info("getHeader http request callback: exiting")
    });
    logger.info("getHeader http request callback: armed");
  });

router.get('/getPixelValueAtiRAiDEC', function( req, res) {
  relFITSFilePath = req.query['relFITSFilePath'];
  iRA = req.query['iRA'];
  iDEC = req.query['iDEC'];
  clienthttp.getPixelValueAtiRAiDEC(relFITSFilePath, iRA, iDEC, (error, response, body) => {
    if (error) {
      var message = error.toString();
      res.send(JSON.stringify({ error: message }));
    }
    else {
      res.send(body)
    }
  })
});

router.get('/getPixelValueAtiFreqiRAiDEC', function( req, res) {
  relFITSFilePath = req.query['relFITSFilePath'];
  iFREQ = req.query['iFREQ'];
  iRA = req.query['iRA'];
  iDEC = req.query['iDEC'];
  clienthttp.getPixelValueAtiFreqiRAiDEC(relFITSFilePath, iRA, iDEC, iFREQ, (error, response, body) => {
    if (error) {
      var message = error.toString();
      res.send(JSON.stringify({ error: message }));
    }
    else {
      res.send(body)
    }
  })
});

/* A route to retrieve a PNG file indexed by a slice index */
router.get('/:product/:iFREQ', function (req, res, next) {
  iFREQ = req.params['iFREQ'];
  product = req.params['product'];

  var files = fs.readdirSync(PNG_ROOT_DIR + product);
  let sliceNumber = files.length;

  fs.createReadStream(PNG_ROOT_DIR + product + '/' + iFREQ + '.png')
    .pipe(new PNG({
      filterType: -1
    }))
    .on('metadata', function () {
      res.render('index', { title: 'Express', op: 'slice', iFREQ: iFREQ, product: product, width: this.width, height: this.height, sliceNumber: sliceNumber });
    });

});

/******************/
/*   POST routes  */
/******************/

/* The index post route. It will trigger a behaviour based on the value of "method" parameter */
router.post('/', function (req, res, next) {
  logger.info("router.post('/', function(req, res, next) { : entering");
  var method = req.body.method;
  logger.info(`method=${method}`);
  var self = res;
  var header;

  function handleClientHttpRes(error,res,body,consoleText){
    if (error) {
      var message = error.toString();
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ "status": false, "message": message }));
    }
    else if (body["status"] == false) {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ "status": false, "message": body["message"] }));
    }
    else {
      var tempFile = body["result"];
      logger.info(consoleText);
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.send(JSON.stringify({ "status": true,
       "message": "", 
       "result": "/" + tempFile 
      }));
    }
  }

  if (false) {
    ;
  }


  /*
    if (method === "getHMSDMS") {
      var iRA = req.body.iRA;
      var iDEC = req.body.iDEC;
  
      clienthttp.degToHMSDMS(req.body.relFITSFilePath,  parseInt(iRA), parseInt(iDEC), req.sessionID ?req.sessionID : 0, (error, response, body)=>{
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: body }));
      });
    }
  
    if (method === "getRangeHMS") {
      var startR = req.body.start;
      var endR = req.body.end;
      var step = req.body.step;
  
      clienthttp.rangeToHMS( req.body.relFITSFilePath, req.sessionID ?req.sessionID : 0, startR, endR, step, (error, response, body)=>{
        logger.info("rangeToHMS");
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: body }));
      });
    }
  
    else if (method === "getRangeDMS") {
      let startR = req.body.start;
      let endR = req.body.end;
      let step = req.body.step;
  
      clienthttp.rangeToDMS(req.body.relFITSFilePath, req.sessionID ?req.sessionID : 0, startR, endR, step, (error, response, body)=>{
        logger.info("rangeToDMS callback : entering");
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: body }));
        logger.info("rangeToDMS callback : exiting");
      } );
    }
  */
  else if (method == "RADECRangeInDegrees") {
    clienthttp.RADECRangeInDegrees(req.body.relFITSFilePath, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
      logger.info("RADECRangeInDegrees callback : entering");
      self.setHeader('Content-type', 'application/json');
      let result = { data: body };
      //logger.info ("About to return '" + JSON.stringify(result, 0, 4) + "'");
      self.send(JSON.stringify(result));
      logger.info("RADECRangeInDegrees callback : exiting");
    });
  }

  else if (method === "getHeader") {
    var relFITSFilePath = req.body.name;

    clienthttp.setData(relFITSFilePath, req.sessionID ? req.sessionID : 0, (error, response, body) => {
      logger.info("getHeader callback : entering");
      if (error) {
        var message = error.toString();
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ error: message }));
      } else {
        header = JSON.parse(body);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: { displayLimits: displayLimits, header: header } }));
      }
      logger.info("getHeader callback : exiting");
    });
  }

  else if (method === "getSlice") {
    var sliceB = req.body.slice ? req.body.slice : 0;
    step = parseInt(req.body.step) ? parseInt(req.body.step) : 1;
    clienthttp.getSlice(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, sliceB, step, (error, response, body) => {
      logger.info("getSlice callback : entering ");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
      logger.info("getSlice callback : exiting ");
    });
  }

  else if (method === "getAverage") {
    var relFITSFilePath = req.body.relFITSFilePath;
    var iRA0 = condAss(parseInt(req.body.iRA0), isNaN, 0); 
    var iRA1 = condAss(parseInt(req.body.iRA1), isNaN, null);
    var iDEC0 = condAss(parseInt(req.body.iDEC0), isNaN, 0);
    var iDEC1 = condAss(parseInt(req.body.iDEC1), isNaN, null);
    var iFREQ0 = condAss(parseInt(req.body.iFREQ0), isNaN, 0);
    var iFREQ1 = condAss(parseInt(req.body.iFREQ1), isNaN, null);
    var retFITS = req.body.retFITS !== "undefined";

    clienthttp.getAverage(relFITSFilePath, iRA0, iRA1, iDEC0, iDEC1, iFREQ0, iFREQ1, retFITS, (error, response, body) => {
      logger.info("getAverage callback : entering");
      self.setHeader('Content-Type', 'application/json');
      logger.info(`result body = ${JSON.stringify(body)}`);
      self.send(JSON.stringify({ data: body }));
      logger.info("getAverage callback : exiting");
    });
  }

  else if (method === "getSpectrum") {
    var iRA = req.body.iRA;
    var iDEC = req.body.iDEC

    clienthttp.getSpectrum(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, iRA, iDEC, null, null, (error, response, body) => {
      logger.info("getSpectrum callback : entering");
      self.setHeader('Content-Type', 'application/json');
      self.send(JSON.stringify({ data: body }));
      logger.info(body);
      logger.info("getSpectrum callback : exiting");
    });
  }

  else if (method === "getAverageSpectrum") {
    logger.info("About to execute the code for getAverageSpectrum");
    var iRA0 = req.body.iRA0;
    var iDEC0 = req.body.iDEC0;
    var iRA1 = req.body.iRA1;
    var iDEC1 = req.body.iDEC1;

    var retFITS = useSAMP;
    clienthttp.getAverageSpectrum(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, iDEC0, iDEC1, iRA0, iRA1, retFITS, (error, response, body) => {
      logger.info("getAverageSpectrum callback : entering");
      //logger.info("error = " + JSON.stringify(error));
      if (error) {
        var message = error.toString();
        logger.info(message);
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ "status": false, "message": message }));
      }
      else if (body["status"] == false) {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ "status": false, "message": body["message"] }));
      }
      else {
        try{
          let tempFile = body["result"]["averageSpectrumFits"];
          logger.info("sending the name of the tmp FITS file along with the averageSpectrum");
          res.send(JSON.stringify({
            "status": true,
            "message": "",
            "result": {
              "absFITSFilePath": "/" + tempFile,
              "averageSpectrum": body["result"]["averageSpectrum"]
            }
          }));
        }catch(error){
          res.send(JSON.stringify({
            "status": false,
            "message": "Error while reading result from getAverageSpectrum",
            "result": {}
          }));
        }

      }
      logger.info("getAverageSpectrum callback : exiting");
    });
  }

  else if (method === "getSumOverSliceRectArea") {
    clienthttp.getSumOverSliceRectArea(
      req.body.relFITSFilePath,
      req.body.sessionID ? req.body.sessionID : 0,
      req.body.iFREQ ? parseInt(req.body.iFREQ) : 0,
      req.body.iRA0 ? parseInt(req.body.iRA0) : 0,
      req.body.iRA1 ? parseInt(req.body.iRA1) : null,
      req.body.iDEC0 ? parseInt(req.body.iDEC0) : 0,
      req.body.iDEC1 ? parseInt(req.body.iDEC1) : null,
      (error, response, body) => {
        logger.info("getSumOverSliceRectArea callback : entering");
        if (error) {
          var message = error.toString();
          logger.info(message);
          res.send(JSON.stringify({ "status": false, "message": message }));
        }
        else if (response["statusCode"] == 500) {
          self.send(JSON.stringify({ "status": false, "message": response["body"] }));
        }
        else if (body["status"] == false) {
          self.send(JSON.stringify({ "status": false, "message": body["message"] }));
        }
        else {
          self.send(JSON.stringify({ "status": true, "message": "", "result": body["result"] }))
        }
        logger.info("getSumOverSliceRectArea callback : entering");
      }
    );
  }
  else if (method === "createFits") {
    logger.info("createFits callback : entering");

    var iRA = req.body.iRA;
    var iDEC = req.body.iDEC;
    var relFITSFilePath = req.body.relFITSFilePath
    clienthttp.createFits(relFITSFilePath, iRA, iDEC, (error, response, body) => {
      var text = "sending the name of the tmp FITS file along with the createSpectrum";
      handleClientHttpRes(error, res, body,text);
      console.log("createFits callback : exiting");
    });
  }

  else if (method === "createFITSSliceImage") {
    logger.info("createFITSSliceImage callback : entering");

    var iFREQ = req.body.iFREQ;
    var relFITSFilePath = req.body.relFITSFilePath
    clienthttp.createFITSSliceImage(relFITSFilePath, iFREQ, (error, response, body) => {
      var text = "sending the name of the tmp FITS image top file along with the createFITSliceImage";
      handleClientHttpRes(error, res, body,text);
      console.log("createFITSSliceImage callback : exiting");
    });
  }

  else if (method === "createFITSSumSliceImage") {
    logger.info("createFITSSumSliceImage callback : entering");

    var iFREQ0 = req.body.iFREQ0;
    var iFREQ1 = req.body.iFREQ1;
    var relFITSFilePath = req.body.relFITSFilePath
    clienthttp.createFITSSumSliceImage(relFITSFilePath, iFREQ0,iFREQ1, (error, response, body) => {
      var text ="sending the name of the tmp FITS image bottom file along with the createFITSSumSliceImage" ;
      handleClientHttpRes(error,res,body,text);
      console.log("createFITSSumSliceImage callback : exiting");
    });
  }

  else if (method === "createSmoothCube") {
    logger.info("createSmoothCube callback : entering");

    var nbox = req.body.nbox;
    var relFITSFilePath = req.body.relFITSFilePath
    clienthttp.createSmoothCube(relFITSFilePath, nbox, (error, response, body) => {
      var text = "sending the name of the tmp smooth cube file along with the createSmoothCube";
      handleClientHttpRes(error,res,body,text);
      logger.info("createSmoothCube callback : exiting");
    });
  }

  else if (method === "getObjects") {
    // Peform a distinct query against the a field Header.OBJECT . But don't forget to reject the null Header.OBJECT !
    Fitsinfo.distinct('Header.OBJECT', { 'Header.OBJECT': { $ne: null } }, function (err, objects) {
      if (err) {
        logger.info(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({
          data: objects.sort(function (a, b) {
            var aA = a.toUpperCase();
            var bB = b.toUpperCase();
            return (aA < bB) ? -1 : (aA > bB) ? 1 : 0;
          })
        }));
      }
    });
  }

  else if (method === "getFiles") {
    var object = req.body.object;

    // Peform a distinct query against the a field Path with field Header.OBJECT == object
    Fitsinfo.distinct('Path', { "Header.OBJECT": object }, function (err, files) {
      if (err) {
        logger.info(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: files }));
      }
    });
  }

  else if (method === "getFitsHeader") {
    var relFITSFilePath = req.body.relFITSFilePath;

    // Find the corresponding entry in DB
    //{fields: {"_id": 0}
    Fitsinfo.find({ "Path": relFITSFilePath }, { "_id": 0 }, function (err, fitsHeader) {
      if (err) {
        logger.info(err);
      } else {
        self.setHeader('Content-Type', 'application/json');
        self.send(JSON.stringify({ data: fitsHeader[0] }));
      }
    });
  }
  // requests server to write a json object containing a summary of a cube data for testing purpose
  // only if test mode is enabled in .yafits.bashrc config file
  else if (method === "saveTest") {
    if(testMode){
      let testData = req.body.testData;
      let fileName = req.body.fileName;
      let cubeType = req.body.cubeType;

      clienthttp.saveTest(fileName, testData, cubeType, (error, response, body) => {
        if (error) {
          var message = error.toString();
          logger.info(message);
        }else{
          res.send(body);
        }        
      });
    }
  }


  else if (method === "getHistory") {
    self.setHeader('Content-Type', 'application/json');
    self.send(JSON.stringify({ data: req.user.recentFiles }));
  }
  else {
    logger.info("Do not know what to do here with " + JSON.stringify(req.body, null, 4));
  }

  logger.info("router.post('/', function(req, res, next) { : exiting");

});

/*
** 1) Triggers a request to the FITS server for the header of a FITS file
** 2) Redirects the browser to the visualization ( visit ) page of the given FITS file.
*/
router.post('/setData',
  function (req, res, next) {
    logger.info("router.post('/setData', function(req, res, next) { : entering");
    // we need a FITS header
    clienthttp.setData(req.body.relFITSFilePath, req.sessionID ? req.sessionID : 0, (error, response, body) => {
      logger.info("setData callback : entering");
      logger.info(JSON.stringify(response));

      if (error) {
        var message = error.toString();
        logger.info(message);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({ "status": false, "message": message }));
      }
      else {
        res.send(body);
        next();
      }
      logger.info("setData callback : exiting");
    });
    logger.info("router.post('/setData', function(req, res, next) { : exiting");
  }
);

router.post('/png', function (req, res, next) {
  logger.info("router.post('/png', function(req, res, next) { : entering");

  clienthttp.getOneSliceAsPNG(req.body.relFITSFilePath, req.body.si, req.body.ittName, req.body.lutName, req.body.vmName, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
    logger.info("getOneSliceAsPNG callback : entering");
    if (error) {
      logger.info(error);
    }
    else if (body["status"] == false) {
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({ "status": false, "message": body["message"] }));
    }
    else {
      res.send(body);
      next();
    }
    logger.info("getOneSliceAsPNG callback : exiting");
  });
  logger.info("router.post('/png', function(req, res, next) { : exiting");
});

router.post('/sumpng', function (req, res, next) {
  logger.info("router.post('/sumpng', function(req, res, next) {: entering");

  clienthttp.getSummedSliceRangeAsPNG(req.body.relFITSFilePath, req.body.si0, req.body.si1, req.body.ittName, req.body.lutName, req.body.vmName, req.body.sessionID ? req.body.sessionID : 0, (error, response, body) => {
    logger.info("getSummedSliceRangeAsPNG callback entering");
    if (error) {
      logger.info(error);
    }
    else {
      //result["path_to_png"] = result["path_to_png"].toString(); // ??? why do I get an object instead of a string . I have no idea; this does not happen with post('/png') !!!
      res.send(body);
    }
    logger.info("getSummedSliceRangeAsPNG callback : exiting");
  });
  logger.info("router.post('/sumpng', function(req, res, next) {: exiting");
});

router.post('/getContours', function (req, res, next) {
  logger.info("router.post('/getContours', function(req, res, next) {: entering");
  clienthttp.getContours(req.body.relFITSFilePath, req.body.optParams, (error, response, body) => {
    logger.info("getContours callback entering");
    if (error) {
      logger.info(error);
    }
    else {
      res.send(body);
    }
    logger.info("getContours callback : exiting");
  });
  logger.info("router.post('/getContours', function(req, res, next) {: exiting");
});

router.post('/measureContour', function (req, res, next) {
  logger.info("router.post('/measureContour', function(req, res, next){: entering");
  clienthttp.measureContour(req.body.relFITSFilePath, req.body.iFREQ, req.body.contour, req.body.level, (error, response, body) => {
    logger.info("measureContour callback entering");
    if (error) {
      logger.info(error);
    }
    else {
      res.send(body);
    }
    logger.info("measureContour callback exiting");
  });
  logger.info("router.post('/measureContour', function(req, res, next){: exiting");
});

router.post('/measureBox', function (req, res, next) {
  logger.info("router.post('/measureBox', function(req, res, next){: entering");
  clienthttp.measureBox(req.body.relFITSFilePath, req.body.iFREQ, req.body.iRA0, req.body.iRA1, req.body.iDEC0, req.body.iDEC1, (error, response, body) => {
    logger.info("measureBox callback entering");
    if (error) {
      logger.info(error);
    }
    else {
      logger.info(body);
      res.send(body);
    }
    logger.info("measureBox callback exiting");
  });
  logger.info("router.post('/measureBox', function(req, res, next){: exiting");
});

router.get("/getYtObj", function (req, res, next) {
  logger.info('router.get("/", function(req, res, next) { : entering ');
  clienthttp.getYtObj(req.query.relFITSFilePath, req.query.iRA0, req.query.iRA1, req.query.iDEC0,
    req.query.iDEC1, req.query.iFREQ0, req.query.iFREQ1, (error, response, body) => {
      logger.info("getYtObj callback : entering");
      if (error) {
        logger.info(error);
      }
      else {
        logger.info("body result " + body["result"]);
        var gltfFile = OBJ_ROOT_DIR + "/" + body["result"] + ".gltf";

        //create gltfFile
        obj2gltf(OBJ_ROOT_DIR + "/" + body["result"] + ".obj").then(function (gltf) {
          const data = Buffer.from(JSON.stringify(gltf));
          fs.writeFileSync(gltfFile, data);
        }).then(function () {
          logger.info(gltfFile);
          res.render("getYtObj", { gltfFile: URLRoot(req.headers.referer) + "/" + body["result"] + ".gltf" });
        });

      }
      logger.info("getYtObj callback : exiting");
    });
});

module.exports = router;
