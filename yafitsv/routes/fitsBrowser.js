var express = require('express');
const { logger } = require('../logger');
var router = express.Router();
const request = require('request');

var yafitssHost = process.env.YAFITSS_HOST;
var yafitssPort = process.env.YAFITSS_PORT;

logger.info(`Hi I'm fitsBrowser. I'll collaborate with ...${yafitssHost}:${yafitssPort} ...for the FITS files services`);

var clienthttp = {
  server : "http://"+yafitssHost+":"+yafitssPort+"/artemix",
  getEntries : function(relKey, callback) {
    let input = {
      "relKey" : relKey
    };
    request.post(this.server+"/getEntries", {json:true, body: input}, function(error, response, body) {
      callback(error, response, body)
    });
  }
};

/***********************/
/*     GET routes     */
/***********************/
router.get("/", function (req, res, next) {
    logger.info('router.get("/", function (req, res, next) { : entering');
    res.render("fitsBrowser", {});
    console.log('router.get("/", function (req, res, next) { : exiting');
});

router.get("/getEntries", function(req, res, next){
  logger.info('router.get("/", function(req, res, next){ : entering');
  logger.info("req.query = " + JSON.stringify(req.query, 0, 4));
  var relKey = req.query["key"];
  clienthttp.getEntries(relKey, (error, response, body) => {
    logger.info("getEntries callback: entering");
    logger.info("error " + JSON.stringify(error, 0, 4));
    //logger.info("response " + JSON.stringify(response, 0, 4));

    if (error) {
      var message = error.toString();
      logger.info(message);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify({"status" : false, "message": message }));
    }
    else if (response["statusCode"] == 500) {
      res.send(JSON.stringify({"status": false, "message": response["body"]}))
    }
    else {
      res.send(JSON.parse(body["result"]));
    }
    logger.info("getEntries callback : exiting");
  });
  logger.info('router.get("/", function(req, res, next){ : exiting');
});

module.exports = router;
