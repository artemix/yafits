var express = require('express');
const { logger } = require('../logger');
var router = express.Router();
const request = require('request');
router.get("/", function (req, res, next) {
    logger.info('router.get("/", function (req, res, next) { : entering');
    res.render("test", {});
    console.log('router.get("/", function (req, res, next) { : exiting');
});


module.exports = router;
