var express = require('express');
const { logger } = require('../logger');
var router = express.Router();
const request = require('request');

var yafitssHost = process.env.YAFITSS_HOST;
var yafitssPort = process.env.YAFITSS_PORT;

logger.info(`Hi I'm monitor. I'll collaborate with ...${yafitssHost}:${yafitssPort} ...for the FITS files services`);

/*
** Prepare the communication with the FITS server ( yafitss )
*/
var clienthttp = {
  server : "http://"+yafitssHost+":"+yafitssPort+"/artemix",
  getDataBlockInfos : function(callback) {
    request.post(this.server+"/getDataBlockInfos", function(error, response, body){
      callback(error, response, body);
    });
  }
};

/*
** Define the http nodejs route
*/
router.get("/", function(req, res, next) {
  logger.info("monitor router.get('/', function(req, res, next) { : entering" );
  clienthttp.getDataBlockInfos((error, response, body) =>  {
    logger.info("getDataBlockInfos callback : entering");
    if (error ){
      var message = error.toString();
      res.setHeader('Content-type', 'application/json');
      res.send(JSON.stringify({"status":false, "message": message}));
    }
    else if (response["statusCode"] == 500) {
      res.send(JSON.stringify({status: false, message: response["body"]}));
    }
    else if (response["body"]["status"] == false) {
      res.render("error", {message: response["body"]["message"], error : {"status" : "", "stack" : ""}});
    }
    else {
      let x = JSON.parse(body)
      res.render("monitor", { when: x["result"]["when"], maxidle: x["result"]["maxidle"], hostname: x["result"]["hostname"], cpu: x["result"]["cpu"], memory: x["result"]["memory"], dataBlockInfos: x["result"]["dataBlocks"]});
    }
    logger.info("getDataBlockInfos callback : exiting");
  });
  logger.info("monitor router.get('/', function(req, res, next) { : exiting" );
});


module.exports = router;
