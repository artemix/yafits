#!/usr/bin/env python3
#
# This script maintains an sqlite table 'fitsinfos' in an sql database also named 'fitsinfos' created an updated accordingly 
# with the collection of FITS files found under a top level directory.
#
# The sqlite table named 'fitsinfos' has the following columns
# * the path to the fits file relative to the directory containing all the fits files (string)
# * the size as a number of bytes of the fits file ( Number )
# * a subset of the fits file header stored as an sqlite json extension
#
# The application's execution depends only on two environment variables :
#
# YAFITS_FITSDIR : the top level directory of the FITS files collection.
# YAFITS_SQLITE_DIR : the directory that containes SQLite database 'fitsinfos.db'
#
# The application's activity is logged in a file fitsinfos.log located in the current directory.
#
# Author : Michel Caillat
#
# Date : May 2020
#        Oct 2021
#
import astropy.io.fits
import re
import sys
import os
import astropy
import datetime
import time
import json
from astropy.io import fits
import logging
import sqlite3
import sys
import traceback
from typing import List
from optparse import OptionParser

logging.basicConfig(format='%(asctime)s %(message)s',
                    level=logging.INFO, filename="fs2sqlite.log")

class Context :
    def __init__(self):
        #
        # First the FITS files collection top level directory
        #
        self.fits_root_dir = os.environ['YAFITS_FITSDIR']
        
        #
        # Then the directory containing fitsinfos.db 
        #
        self.sqlite_dir = os.environ['YAFITS_SQLITE_DIR']
        
        self.sqlite_db = f'{self.sqlite_dir}/fitsinfos.db'
        self.sqlite_table = 'fitsinfos'
        
def _2uid(name):
    z = name
    if name.startswith('uid___'):
        m = re.search('^uid___A[0-9]+_X[0-9a-z]+(_X[0-9a-z]+)?', name)
        z = m.group(0).replace("___", "://").replace("_", "/")
    return z

def main():
#    logging.disable(logging.INFO)

    try :
        context = Context()
    except KeyError as err:
        logging.exception(f'{err} . Ensure that the environment variables "YAFITS_FITSDIR" and "YAFITS_SQLITE_DIR" are properly defined')
        sys.exit()
    except Exception as err:
        logging.exception(f'{err}')
        sys.exit()
        

    # parser = OptionParser(
    #     usage=f"usage: %prog [options]", version="%prog 1.0")

    #options, args = parser.parse_args()

    logging.info(f"Starting to update the FITS headers table '{context.sqlite_table}' in the sqlite data_base '{context.sqlite_db}'")

    # Connect and create table if required.
    try :
        conn = sqlite3.connect(context.sqlite_db)
        c = conn.cursor()
        table = context.sqlite_table
        columns_names = "Path, Size, Header"
        columns_declaration = "Path VARCHAR(200), Size BIGINT, Header JSON"
        index_declaration = "Path"
        index_columns = "Path"

        # Create if needed
        c.execute(f"CREATE TABLE IF NOT EXISTS {table} ({columns_declaration})")
        c.execute(
            f"CREATE UNIQUE INDEX IF NOT EXISTS {index_declaration} ON {table}({index_columns})")
    
    except Exception as err:
        logging.error(f"{err}")
        sys.exit()
    
    FITS_ROOT = context.fits_root_dir
    if FITS_ROOT.endswith('/'):
        FITS_ROOT = FITS_ROOT[:-1]

    logging.info("About to process all fits files under '%s'." % FITS_ROOT)

    # Prepare tj
    sql = f"INSERT OR REPLACE INTO {table}({columns_names}) VALUES (?, ?, ?)"

    for root, dirs, files in os.walk(FITS_ROOT, followlinks=True):
        if root.find("BIG") > 1:
            continue

        for name in files:
            fullpath = ("%s/%s" % (root, name))
            #
            # Determine the path relative to ALMA_FITS_ROOT
            #
            path = fullpath[len(FITS_ROOT)+1:]

            if name.endswith('.fits') :

                # Determine the size -> size
                #
                try :
                    size = os.path.getsize(fullpath)
                except Exception as e:
                    logging.exception(str(e))
                    logging.exception("Ignored")
                    continue

                #
                # Restructure the header into a dictionary
                #
                try:
                    hdulist = fits.open(fullpath)
                    header = {}
                    for k in hdulist[0].header:
                        if not k in ['HISTORY', '', 'COMMENT']:
                            try:
                                header[k] = hdulist[0].header[k]
                            except astropy.io.fits.verify.VerifyError:
                                logging.error(
                                    "Could not parse value of keyword '%s', forced its value to 'unknown'." % k)
                                header[k] = 'unknown'
                except IOError:
                    logging.error(
                        "Fits file '%s' could not be opened !" % fullpath)
                    header = None

                values = (path, size, json.dumps(header))
                try:
                    c.execute(sql, values) 
                except Exception as e:
                    logging.info(str(e))

                logging.info(f"Processed {path}")
    
    conn.commit()
    logging.info(f'{conn.total_changes} rows inserted' )
    conn.close()
        
    logging.info("Finished to update the FITS headers sqlite table.")
        
if __name__ == '__main__':
    try :
        main()
    except Exception as e:
        logging.exception(f"{e}")
