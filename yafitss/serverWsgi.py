#!/usr/bin/env python
from bottle import route, run, request, default_app, response, static_file, Bottle

import astropy

import sys
import socket
import getopt
import os
import traceback
import numpy as np

import json
#import logging
#from logging.handlers import RotatingFileHandler

from functools import wraps
from datetime import datetime
import dataManager as dataManager
from result import Result
import log
import pathManager as pm
import timer as time

#
# The definition below is just a work in progress. Do not consider it for the time being
#
def log_to_logger(fn):
    '''
    Wrap a Bottle request so that a log line is emitted after it's handled.
    (This decorator can be extended to take the desired logger as a param.)
    '''
    @wraps(fn)
    def _log_to_logger(*args, **kwargs):
        global logger
        request_time = datetime.now()
        actual_response = fn(*args, **kwargs)
        # modify this to log exactly what you need:
        logger.info('%s %s %s %s %s' % (request.remote_addr,
                                        request_time,
                                        request.method,
                                        request.url,
                                        response.status))
        return actual_response
    return _log_to_logger


"""
@api {NOOP} noAPI
@apiGroup _Introduction
@apiDescription
The yafitss http server.

Summary
-------
The server accepts a number of GET and POST calls allowing to work remotely, as an HTTP client, on a collection of FITS files containing images.

Technical aspects of our server
-------------------------------
The server is a bottle HTTP server. It accepts requests ( GET or POST ), extracts their parameters, converts
them from the string type to the appropriate type, delegates the processing to a dataManager layer from which
it receives the result to be sent back to the HTTP client in **json** form.

Conventions
-----------
Input parameters
----------------

Each method GET or POST of an HTTP server expects zero, one or more parameters which are passed via a dictionary structure, i.e a sequence of ``key:value`` pairs. Of course this is true for `yafitss`, but in order to give the server the flavour of a domain specific language a convention for the names of the keys is adopted.\
The list below enumerates those names, the types and the semantics of their associated values. Indeed types are not real types as in the usual sense since the values are always passed as strings; they are indications on the form those values should have:

 * ``relFITSFilePath`` {String} the path of the FITS file of interest relative to the root directory of the FITS files
 * ``iRA`` {uInt} an indice along the RA axis
 * ``iDEC`` {uInt} an indice along the DEC axis
 * ``RAinDD`` {Float} a right ascension expressed in decimal degrees
 * ``DECinDD`` {Float} a declination expressed in decimal degrees
 * ``iRA0`` {uInt} the lower bound of a range of indices along the RA axis
 * ``iRA1`` {uInt} the upper bound of a range of indices along the RA axis           let pipeIndex = result.indexOf('|'); 
            let format = result.substring(0, pipeIndex); 
            let data = result.substring(pipeIndex+1);
            console.log(`pipeIndex = ${pipeIndex}`);
            console.log(`format = ${format}`);
            let [status, message, flatras] = python_struct.unpack(format, buffer.Buffer.from(result.substring(pipeIndex+1)));
 * ``iRAstep`` {uInt} a step in a range of indices along the RA axis
 * ``iDEC0`` {uInt} the lower bound of a range of indices along the DEC axis
 * ``iDEC1`` {uInt} the upper bound of a range of indices along the DEC axis
 * ``iDECstep`` {uInt} a step in a range of indices along the DEC axis
 * ``iFREQ`` {uInt} an indice along the FREQ axis
 * ``iFREQ0`` {uInt} the lower bound of a range of indices along the FREQ axis
 * ``iFREQ1`` {uInt} the upper bound of a range of indices along the FREQ axis

Returned values
---------------
API calls return systematically  a compound result with the following structure :

```
 {
 "status": True of False,

 "message": <some text mostly used when something went wrong>,

 "result" : <the expected result or None>
 }
 ```

 Consequently given a returned value stored in a variable `result`, a good practice is to proceed as follows:

 * `if result["status"] :`
    * retrieve the payload in `result["result"]`
 * `else :`
    * retrieve an error message in `result["message"]`
"""

#ARTEMIXDataDir = None
#FITSRootDir = None
#SAMPDataDir = None
baseUrl = "/artemix"

logger = None
timer = None
dm = None
app = None



#####################################################################
#                                                                   #
#                            Utilities                              #
#                                                                   #
#####################################################################

def enable_cors(fn):
    """
    enable cross domain ajax requests
    """
    def _enable_cors(*args, **kwargs):
    # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

        if request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)

    return _enable_cors

def getFloatValue(value):
    if value is not None and str(value) != "null":
        return float(value)
    return None

def getIntValue(value):
    if value is not None and str(value) != "null":
        return int(float(value))
    return None

def getBoolValue(value):
    if value is not None and str(value) != "null":
        return value == "true"
    return None

def cleanNanInList(arr):
    """
    	Replaces NaN values by None in array
    """
    result = arr
    result[np.isnan(result)] = None
    return result

def rebuildFilename(filename):
    """
        Given filename ( string )
        returns a string where each space character present in filename is replaced by '+' character.
    """
    result = byteify(filename.replace(" ", "+"))
    if ((result[0] == '"' and result[len(result) - 1] == '"') or ( result[0] == "'" and result[len(result) - 1] == "'" )):
        result = result[1:-1]
    return result

def byteify(input):
    """
        A function to transform the content of a dictionary a unicode value into its ascii's
        see https://stackoverflow.com/questions/956867/how-to-get-string-objects-instead-of-unicode-from-json
    """
    result = None
    if isinstance(input, dict):
        result = {byteify(key): byteify(value)
                for key, value in input.items()}
    elif isinstance(input, list):
        result = [byteify(element) for element in input]
    elif isinstance(input, str):
        result =  input.encode('utf-8').decode('utf-8')
    elif isinstance(input, bytes):
        result = input.encode('utf-8').decode('utf-8')
    else:
        result = input
    #logging.debug("result = %r" % result)
    return result

#####################################################################
#                                                                   #
#                   Routes definitions                              #
#                                                                   #
#####################################################################

"""
@api {GET} artemix/ping ping - Check server readiness
@apiName Check server readiness
@apiGroup server

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {status: True, message : 'Up and waiting', 'result': }
"""

@route(baseUrl+"/ping", name='upAnWaiting', method='GET')
@enable_cors
def upAnWaiting():
    """/ping.

    Check that the server is alive and waiting

    Returns:
            A dict with "status" == True and "message" == "Up and Waiting"
    """
    logger.debug("upAndWaiting - wrapper : entering")
    logger.debug("upAndWaiting - wrapper : exiting")
    return "{status: %r, message : 'Up and waiting', 'result': %r}" % (True, request)


"""
@api {GET} artemix/degToHMSDMS degToHMSDMS - Converts a (RA,DEC) pair from decimal degrees to HMS DMS
@apiName degToHMSDMS
@apiGroup conversions

@apiParam {String} relFITSFilePath  the path to the FITS file of interest.
@apiParam {Float} RAinDD  RA in decimal degree
@apiParam {Float} DECinDD  DEC in decimale degree


@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
     {'status': True, 'message': '', 'result': '12h30m50.1624s +12d23m08.2758s'}
"""
@route( baseUrl+'/degToHMSDMS', name='degToHMSDMS', method='GET')
@enable_cors
def degToHMSDMS():
    """
        Given two values expressed in degrees one for a Right Ascension (ra) and one for a Declination
        returns their respectives conversion in Hour Minute Second and Degree Minute Second
    """
    logger.debug("degToHMSDMS - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        RAinDD = getFloatValue(request.GET['RAinDD'])
        DECinDD = getFloatValue(request.GET['DECinDD'])
        result = (dm.degToHMSDMS(relFITSFilePath, RAinDD, DECinDD))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("degToHMSDMS - wrapper : exiting")

    return result



"""
@api {GET} artemix/rangeToHMS rangeToHMS - Generates a sequence of HMS from a range of RA indexes
@apiName rangeToHMS
@apiGroup conversions

@apiParam {String} relFITSFilePath  the path to the FITS file of interest.
@apiParam {uInt} iRAO  the lower index of the range along the RA axis.
@apiParam {uInt} iRA1 the upper index of the range along the RA axis.
@apiParam {uInt} iRAstep  the step along the RA axis.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
      {'status': True, 'message': '', 'result': ['12h30m50.8158s +12d23m07.6437s', '12h30m50.1196s +12d23m07.6439s', '12h30m49.4234s +12d23m07.6439s', '12h30m48.7272s +12d23m07.6439s']}
"""
@route( baseUrl+'/rangeToHMS', name='rangeToHMS', method='GET')
@enable_cors
def rangeToHMS():
    logger.debug("rangeToHMS - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iRA0 = getIntValue(request.GET['iRA0'])
        iRA1 = getIntValue(request.GET['iRA1'])
        iRAstep = getIntValue(request.GET['iRAstep'])
        result = (dm.rangeToHMS(relFITSFilePath, iRA0, iRA1, iRAstep))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("rangeToHMS - wrapper : exiting")

    return result

"""
@api {GET} artemix/rangeToDMS Generates a sequence of DMS from a range of DEC indexes
@apiName rangeToDMS
@apiGroup conversions

@apiParam {String} relFITSFilePath  the path to the FITS file of interest.
@apiParam {uInt} iDECO  the lower index of the range along the DEC axis.
@apiParam {uInt} iDEC1 the upper index of the range along the DEC axis.
@apiParam {uInt}  iDECstep the step along the DEC axis.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{'status': True, 'message': '', 'result': ['12h30m50.8158s +12d23m07.6437s', '12h30m50.8158s +12d23m17.8437s', '12h30m50.8158s +12d23m28.0437s', '12h30m50.8158s +12d23m38.2437s']}
"""
@route( baseUrl+'/rangeToDMS', name='rangeToDMS', method='GET')
@enable_cors
def rangeToDMS():
    logger.debug("rangeToDMS - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iDEC0 = getIntValue(request.GET['iDEC0'])
        iDEC1 = getIntValue(request.GET['iDEC1'])
        iDECstep = getIntValue(request.GET['iDECstep'])
        result =(dm.rangeToDMS(relFITSFilePath, iDEC0, iDEC1, iDECstep))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("rangeToDMS - wrapper : exiting")

    return result
"""
@api {GET} artemix/setData setData - Loads a FITS file in server's memory and return its header.
@apiName setData
@apiGroup data selection

@apiParam {String} relFITSFilePath the path to the FITS file of interest. If it's already loaded, just returns the header.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': '{"SIMPLE": true, "BITPIX": -32, "NAXIS": 4, "NAXIS1": 800, "NAXIS2": 800, "NAXIS3": 24, "NAXIS4": 1, "EXTEND": true, "BSCALE": 1.0, "BZERO": 0.0, "BMAJ": 7.385119795799e-05, "BMIN": 5.762678467565e-05, "BPA": 15.67044067383, "BTYPE": "Intensity", "OBJECT": "M87", "BUNIT": "Jy/beam", "EQUINOX": 2000.0, "RADESYS": "FK5", "LONPOLE": 180.0, "LATPOLE": 12.39112331, "PC01_01": 1.0, "PC02_01": 0.0, "PC03_01": 0.0, "PC04_01": 0.0, "PC01_02": 0.0, "PC02_02": 1.0, "PC03_02": 0.0, "PC04_02": 0.0, "PC01_03": 0.0, "PC02_03": 0.0, "PC03_03": 1.0, "PC04_03": 0.0, "PC01_04": 0.0, "PC02_04": 0.0, "PC03_04": 0.0, "PC04_04": 1.0, "CTYPE1": "RA---SIN", "CRVAL1": 187.70593075, "CDELT1": -1.416666666667e-05, "CRPIX1": 401.0, "CUNIT1": "deg", "CTYPE2": "DEC--SIN", "CRVAL2": 12.39112331, "CDELT2": 1.416666666667e-05, "CRPIX2": 401.0, "CUNIT2": "deg", "CTYPE3": "FREQ", "CRVAL3": 230436102960.0, "CDELT3": -76899199.37878, "CRPIX3": 1.0, "CUNIT3": "Hz", "CTYPE4": "STOKES", "CRVAL4": 1.0, "CDELT4": 1.0, "CRPIX4": 1.0, "CUNIT4": "", "PV2_1": 0.0, "PV2_2": 0.0, "RESTFRQ": 230538000000.0, "SPECSYS": "BARYCENT", "ALTRVAL": 132507.283331, "ALTRPIX": 1.0, "VELREF": 258, "TELESCOP": "ALMA", "OBSERVER": "jtan", "DATE-OBS": "2015-08-16T19:00:41.616000", "TIMESYS": "UTC", "OBSRA": 187.70593075, "OBSDEC": 12.39112331, "OBSGEO-X": 2225142.180269, "OBSGEO-Y": -5440307.370349, "OBSGEO-Z": -2481029.851874, "DATE": "2015-12-08T18:24:15.408000", "ORIGIN": "CASA 4.5.0-REL (r35147)", "INSTRUME": "Unknown"}'}
"""
@route( baseUrl+'/setData', name='setData', method='GET')
@enable_cors
def setData():
    logger.debug("setData - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        result = (dm.setData(relFITSFilePath))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("setData - wrapper : exiting")
    return result.toJSON()

"""
@api {GET} artemix/getDimensions Returns the shape of the data.
@apiName getDimensions
@apiGroup data infos

@apiParam {String} relFITSFilePath the path to the FITS file of interest

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': [24, 800, 800]}

"""
@route( baseUrl+'/getDimensions', name='getDimensions', method='GET')
@enable_cors
def getDimensions():
    logger.debug("getDimensions - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        result = (dm.getDimensions(relFITSFilePath))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getDimensions - wrapper : exiting")

    return result.toJSON()


# """
#     Given a multidimensional array l
#     return a flattened representation of l
# """
# @route( baseUrl+'/flatten', name='flatten', method='GET')
# @enable_cors
# def flatten():

#     l = request.GET['l']

#     return json.dumps(dm.flatten(l))


"""
@api {GET} artemix/getPixelValueAtiRAiDEC Returns the value of a pixel located by a pair (iRA, iDEC)
@apiName getPixelValueAtiRAiDEC
@apiGroup values and projections

@apiParam {String} relFITSFilePath the path to the FITS file of interest
@apiParam {uInt} iRA index along the RA axis
@apiParam {uInt} iDEC index along the DEC axis

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': -0.00207878858782351}

"""
@route(baseUrl+'/getPixelValueAtiRAiDEC', name="getPixelValueAtiRAiDEC", method='GET')
@enable_cors
def getPixelValueAtiRAiDEC():
    logger.debug("getPixelValueAtiRAiDEC - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iRA = getIntValue(request.GET['iRA'])
        iDEC = getIntValue(request.GET['iDEC'])
        result = dm.getPixelValueAtiRAiDEC(
            relFITSFilePath, iRA, iDEC)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" % (repr(e), "".join(
            traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getPixelValueAtiRAiDEC - wrapper : exiting")
    return result.toJSON()


"""
@api {GET} artemix/getPixelValueAtiFreqiRAiDEC Returns the value of a pixel located by a triple (iRA, iDEC, iFREQ)
@apiName getPixelValueAtiFreqiRAiDEC
@apiGroup values and projections

@apiParam {String} relFITSFilePath the path to the FITS file of interest
@apiParam {uInt} iRA index along the RA axis
@apiParam {uInt} iDEC index along the DEC axis
@apiParam {uInt} iFREQ index along the FREQ axis

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': -0.00207878858782351}

"""
@route( baseUrl+'/getPixelValueAtiFreqiRAiDEC', name="getPixelValueAtiFreqiRAiDEC", method='GET')
@enable_cors
def getPixelValueAtiFreqiRAiDEC():
    logger.debug("getPixelValueAtiFreqiRAiDEC - wrapper : entering" )
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iFREQ = getIntValue(request.GET['iFREQ'])
        iRA = getIntValue(request.GET['iRA'])
        iDEC = getIntValue(request.GET['iDEC'])
        result = dm.getPixelValueAtiFreqiRAiDEC(relFITSFilePath, iFREQ, iRA, iDEC)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getPixelValueAtiFreqiRAiDEC - wrapper : exiting" )
    return result.toJSON()


"""
@api {POST} artemix/getPixelsValueAtiFreqiRAiDEC Returns the values of a collection of pixels whose positions are defined in an array of triples (iRA, iDEC, iFREQ)
@apiName getPixelsValueAtiFreqiRAiDEC
@apiGroup values and projections

@apiParam {String} relFITSFilePath the path to the FITS file of interest
@apiParam {json} iPositions the array of positions. Each position must be an array of three integers values for iFreq, iRA, iDEC in that order.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': [-0.00207878858782351]}

"""
@route(baseUrl+'/getPixelsValueAtiFreqiRAiDEC', name="getPixelsValueAtiFreqiRAiDEC", method='POST')
@enable_cors
def getPixelsValueAtiFreqiRAiDEC():
    logger.debug("getPixelsValueAtiFreqiRAiDEC - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        iPositions = json.loads(body['iPositions'])
        logger.debug(f"{iPositions}")
        result = dm.getPixelsValueAtiFreqiRAiDEC(relFITSFilePath, iPositions)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" % (repr(e), "".join(
            traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getPixelsValueAtiFreqiRAiDEC - wrapper : exiting")
    return result.toJSON()

"""
@api {GET} artemix/getSlice Returns a (RA, DEC) plane for a given frequency.
@apiName getSlice
@apiGroup  data selection

@apiDescription The result is returned in a list of list ( 2D data ) with RA varying more
rapidly than DEC. The result may contain nan values.

@apiParam {String} relFITSFilePath the path to the FITS file of interest
@apiParam {uInt} iFREQ index along the FREQ axis of the (RA, DEC) plane to return

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': [[...]...[...]]}


"""
@route( baseUrl+'/getSlice', name='getSlice', method='GET')
@enable_cors
def getSlice():
    logger.debug("getSlice - wrapper : entering" )

    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iFREQ = int(request.GET['iFREQ'])
        result = (dm.getSlice(relFITSFilePath, iFREQ))

    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getSlice - wrapper : exiting" )

    return result.toJSON()

"""
    Given one pair, x and y, of integer coordinates in the RAxDEC plane and
    a frequency range defined by a pair of integers iFREQ0 and iFRE1
    returns the values contained in the data cube at [iFREQ0:iFRE1, x, y]
"""
@route( baseUrl+'/getSpectrum', name='getSpectrum', method='GET')
@enable_cors
def getSpectrum():
    logger.debug("getSpectrum - wrapper : entering" )
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iRA = getIntValue(request.GET['iRA'])
        iDEC = getIntValue(request.GET['iDEC'])
        iFREQ0 = getIntValue(request.GET['iFREQ0'])
        iFREQ1 = getIntValue(request.GET['iFREQ1'])
        result = (dm.getSpectrum(relFITSFilePath, iRA, iDEC, iFREQ0, iFREQ1))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getSpectrum - wrapper : exiting")

    return result.toJSON()

"""
@api {GET} getAverageSpectrum Returns a spectrum whose values are obtained by sums on (RA, DEC) planes.
@apiName getAverageSpectrum
@apiGroup values and projections
@apiDescription
Given two pairs -- `iRA0,iRA1` and `iDEC0,iDEC1` -- defining a rectangular area in the RAxDEC plane
and a frequency range defined by a pair of integers `iFREQ0`,`iFREQ1`, a call to this API
returns the result of `numpy.nansum(datacube[iFREQ0:iFRE1, iDEC0:iDEC1, iRA0:iRA1], (1,2))`
multiplied by the appropriate value to take into account the telescope's beam when its relevant
(radio astronomical data)

The result is returned as a dictionary with two keys whose values are defined below :

* `"averageSpectrum"` : the average spectrum as a list of float values
* `"averageSpectrumFits"` : the average spectrum as a FITS table. Note this part is
returned **if and only if** the parameter `retFITS` has been passed with a True value. This optional
product is mainly intended for distribution via SAMP.

@apiParam {String} relFITSFilePath the path to the FITS file of interest.
@apiParam {uInt} iRAO  the lower index of the range along the RA axis.
@apiParam {uInt} iRA1  the upper index of the range along the RA axis.
@apiParam {uInt} iDECO the lower index of the range along the DEC axis.
@apiParam {uInt} iDEC1 the upper index of the range along the DEC axis.
@apiParam {uInt} iFREQO the lower index of the range along the FREQ axis.
@apiParam {uInt} iFREQ1 the upper index of the range along the FREQ axis.
@apiParam  {Boolean} retFITS True means returns the spectrum also as a FITS table.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{'status': True, 'message': '', 'result': {'averageSpectrum': [-0.20683714747428894, 0.06167888641357422, -0.01623634621500969, 0.024083947762846947, -0.02355683408677578, -0.05549465864896774, -0.08872086554765701, -0.08671190589666367, -0.06696601957082748, 0.05688297748565674, -0.13291282951831818, -0.21775048971176147, -0.24340537190437317, -0.10746107250452042, -0.20933619141578674, -0.19364875555038452, -0.2382306307554245, -0.28746917843818665, -0.2542598247528076, -0.2730403542518616, -0.28176575899124146, -0.17414641380310059, -0.30788716673851013, -0.39479535818099976], 'averageSpectrumFits': None}}
"""
@route( baseUrl+'/getAverageSpectrum', name='getAverageSpectrum', method='GET')
@enable_cors
def getAverageSpectrum():
    logger.debug("getAverageSpectrum - wrapper : entering" )

    relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])

    try:
        iDEC0 = getIntValue(request.GET['iDEC0'])
        iDEC1 = getIntValue(request.GET['iDEC1'])
        iRA0 = getIntValue(request.GET['iRA0'])
        iRA1 = getIntValue(request.GET['iRA1'])
        result = (dm.getAverageSpectrum(relFITSFilePath, iDEC0, iDEC1, iRA0, iRA1))
    except Exception as e:
        logger.debug("getAverageSpectrum - Exception")
        logger.debug(e)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getAverageSpectrum - wrapper : exiting" )

    try : 
        json = result.toJSON()
        return json
    except Exception as e :
        logger.debug("Error while encoding JSON")
        logger.debug(e)
        return result.toJSON()

"""
@api {GET} artemix/getSumOverSliceRectArea Weighted sum over a rectangular area in a RA,DEC plane
@apiName getSumOnSliceRectArea
@apiGroup values and projections
@apiDescription Given two pairs of indexes -- `iRA0,iRA1` and `iDEC0,iDEC1` -- defining a rectangular area in the RA,DEC plane
and a frequency defined by an integer iFREQ returns the result of `numpy.nansum(datacube[iFREQ, iDEC0:iDEC1, iRA0:iRA1], (1,2))`
multiplied by the appropriate value to take into account the telescope's beam when it's relevant ( radio astronomical data ).

The result is a float value.

@apiParam {String} relFITSFilePath the path to the FITS file of interest.
@apiParam {uInt} iRAO  the lower index of the range along the RA axis.
@apiParam {uInt} iRA1  the upper index of the range along the RA axis.
@apiParam {uInt} iDECO  the lower index of the range along the DEC axis.
@apiParam {uInt} iDEC1  the upper index of the range along the DEC axis.
@apiParam {uInt} iFREQ index along the FREQ axis.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
 {'status': True, 'message': '', 'result': -0.24340537190437317}

"""
@route( baseUrl+'/getSumOverSliceRectArea', name='getSumOverSliceRectArea', method='GET')
@enable_cors
def getSumOverSliceRectArea():
    logger.debug("getSumOverSliceRectArea - wrapper : entering" )
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iDEC0 = getIntValue(request.GET['iDEC0'])
        iDEC1 = getIntValue(request.GET['iDEC1'])
        iRA0 = getIntValue(request.GET['iRA0'])
        iRA1 = getIntValue(request.GET['iRA1'])
        iFREQ = getIntValue(request.GET['iFREQ'])
        result = (dm.getSumOverSliceRectArea(relFITSFilePath,  iFREQ, iRA0, iRA1, iDEC0, iDEC1))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getSumOverSliceRectArea - wrapper : exiting" )

    return result.toJSON()

"""
    Given two pairs -- iRA0,iRA1 and iDEC0,iDEC1 -- defining a rectangular area the RAxDEC plane
    and a frequency range defined by a pair of integers iFREQ0 and iFREQ1
    returns the result of numpy.nansum(datacube[iFREQ0:iFREQ1, iDEC0:iDEC1, iRA0:iRA1], 0)
    multiplied by 4 * log(2) * cdelt1 * pi / 180 / 4.86e-6 * cdelt2 * pi / 180 / 4.86e-6
"""
@route( baseUrl+'/getAverage', name='getAverage', method='GET')
@enable_cors
def getAverage():
    logger.debug("getAverage - wrapper : entering" )
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        iDEC0 = getIntValue(request.GET['iDEC0'])
        iDEC1 = getIntValue(request.GET['iDEC1'])
        iRA1 = getIntValue(request.GET['iRA1'])
        iRA0 = getIntValue(request.GET['iRA0'])
        iFREQ0 = getIntValue(request.GET['iFREQ0'])
        iFREQ1 = getIntValue(request.GET['iFREQ1'])
        #retFITS = getBoolValue(request.GET['retFITS'])
        result = dm.getAverage(relFITSFilePath, iFREQ0, iFREQ1, iDEC0, iDEC1, iRA0, iRA1)
    except Exception as e:
        logger.debug(f'{e}')
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getAverage - wrapper : exiting" )
    return result.toJSON()

"""
Create a FITS file whose type is TABLE and content is the spectrum at position (iRA, iDEC)
along with the frequencies/velocities.

Returns the content of the file.
"""
# @route( baseUrl+'/createFits', name='createFits', method='POST')
# @enable_cors
# def createFits():
#     logger.debug("createFits - wrapper : entering" )
#     try:
#         header = request.json['header']
#         relFITSFilePath = request.json['relFITSFilePath']
#         iRA = request.json['iRA']
#         iDEC = request.json['iDEC']
#     except Exception as e:
#         logger.debug(f'{e}')
#         raise
#     response.content_type = "application/json; charset=utf-8"
#     logger.debug("createFits - wrapper : exiting" )
#     return json.dumps(dm.createFits(relFITSFilePath, iRA, iDEC))

"""
@api {GET} artemix/getHeader getHeader - Returns the header.
@apiName getHeader
@apiGroup data infos
@apiDescription Returns the header of the FITS file of interest as a dictionary.

@apiParam {String} relFITSFilePath the path to the FITS file of interest.
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{'status': True, 'message': '', 'result': '{"SIMPLE": true, "BITPIX": -32, "NAXIS": 4, "NAXIS1": 800, "NAXIS2": 800, "NAXIS3": 24, "NAXIS4": 1, "EXTEND": true, "BSCALE": 1.0, "BZERO": 0.0, "BMAJ": 7.385119795799e-05, "BMIN": 5.762678467565e-05, "BPA": 15.67044067383, "BTYPE": "Intensity", "OBJECT": "M87", "BUNIT": "Jy/beam", "EQUINOX": 2000.0, "RADESYS": "FK5", "LONPOLE": 180.0, "LATPOLE": 12.39112331, "PC01_01": 1.0, "PC02_01": 0.0, "PC03_01": 0.0, "PC04_01": 0.0, "PC01_02": 0.0, "PC02_02": 1.0, "PC03_02": 0.0, "PC04_02": 0.0, "PC01_03": 0.0, "PC02_03": 0.0, "PC03_03": 1.0, "PC04_03": 0.0, "PC01_04": 0.0, "PC02_04": 0.0, "PC03_04": 0.0, "PC04_04": 1.0, "CTYPE1": "RA---SIN", "CRVAL1": 187.70593075, "CDELT1": -1.416666666667e-05, "CRPIX1": 401.0, "CUNIT1": "deg", "CTYPE2": "DEC--SIN", "CRVAL2": 12.39112331, "CDELT2": 1.416666666667e-05, "CRPIX2": 401.0, "CUNIT2": "deg", "CTYPE3": "FREQ", "CRVAL3": 230436102960.0, "CDELT3": -76899199.37878, "CRPIX3": 1.0, "CUNIT3": "Hz", "CTYPE4": "STOKES", "CRVAL4": 1.0, "CDELT4": 1.0, "CRPIX4": 1.0, "CUNIT4": "", "PV2_1": 0.0, "PV2_2": 0.0, "RESTFRQ": 230538000000.0, "SPECSYS": "BARYCENT", "ALTRVAL": 132507.283331, "ALTRPIX": 1.0, "VELREF": 258, "TELESCOP": "ALMA", "OBSERVER": "jtan", "DATE-OBS": "2015-08-16T19:00:41.616000", "TIMESYS": "UTC", "OBSRA": 187.70593075, "OBSDEC": 12.39112331, "OBSGEO-X": 2225142.180269, "OBSGEO-Y": -5440307.370349, "OBSGEO-Z": -2481029.851874, "DATE": "2015-12-08T18:24:15.408000", "ORIGIN": "CASA 4.5.0-REL (r35147)", "INSTRUME": "Unknown"}'}
"""
@route( baseUrl+'/getHeader', name='getHeader', method='GET')
@enable_cors
def getHeader():
    logger.debug("getHeader - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        result = (dm.getHeader(relFITSFilePath))
    except Exception as e:
        logger.debug(f'{e}')
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getHeader - wrapper : exiting")
    return result.toJSON()


"""
@api {GET} artemix/RADECRangeInDegrees RADECRangeInDegrees - Returns the RA,DEC range.
@apiName RADECRangeInDegrees
@apiGroup data infos
@apiDescription Returns the RA,DEC range of the data. The result is
returned as a pair of pairs of float values `[[ra0, dec0], [ra1, dec1], [ra2, dec2]]`
expressing angles in decimal degrees where :

* `ra0` corresponds to the index 0 along the RA axis.
* `ra1` corresponds to the index NAXIS1-1 along the RA axis.
* `dec0` corresponds to the index 0 along the DEC axis.
* `dec1` corresponds to the index NAXIS2-1 along the DEC axis.
* `ra2` corresponds to the index max(NAXIS1, NAXIS2)-1 along the RA axis.
* `dec2` corresponds to the index max(NAXIS1, NAXIS2)-1 along the DEC axis.

@apiParam {String} relFITSFilePath the path to the FITS file of interest
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
    {'status': True, 'message': '', 'result': [[187.71173244085927, 12.385456581760074], [187.700143312178, 12.396775748749931], [187.700143312178, 12.396775748749931]]}
"""
@route( baseUrl+'/RADECRangeInDegrees', name='RADECRangeInDegrees', method='GET')
@enable_cors
def RADECRangeInDegrees():
    logger.debug("RADECRangeInDegrees - wrapper : entering")
    try :
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        result = (dm.RADECRangeInDegrees(relFITSFilePath))
    except Exception as e:
        logger.debug(f'{e}')
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("RADECRangeInDegrees - wrapper : exiting")

    return result.toJSON()

"""
@api {POST} artemix/getOneSliceAsPNG Generates a PNG representation of a RA,DEC plane.
@apiName getOneSliceAsPNG
@apiGroup imagery
@apiDescription

* Generates a PNG image of a `RA,DEC` plane defined by an index, `iFREQ`, along the FREQ axis.

   * The image is built from the pixels values used as entries in the composition of an "intensity transfert table" (`ittName`), a "look up table" (`lutName`) and a "video mode" (`vmName`).
   * The image is written to the server's filesystem in a space which can be browsed.
   * The PNG file is named after `relFITSFilePath, iFREQ, ittName, lutName, vmName`.
   * The PNG images use 256 colours palettes.

   * Valid names values for `ittName, lutName, vmName` can be retrieved with the API
`renderingCapabilities`.

* Returns a dictionary with two entries : `{'data_steps': .... , 'path_to_png' : ...}`
    * `'data_steps'` : an association (dictionary) between the 256 RGB triples used in the PNG image and the 256 levels of the sampling of the physical values present in the `RA,DEC` plane. Example of an element of that dictionary : `"255_121_0": -0.037923078107483243`
    * `'path_to_png'` : the path to the PNG file relative to some root directory defined in the server's configuration. The important point is that it can be used as an URL, e.g.
    for visualization purpose a on a browser.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
'{"status": true, "message": "", "result": {"data_steps": {"255_0_41": -0.051212918013334274, ... ,"255_0_191": 0.06175072118639946}, "path_to_png": "2013/2013.1.00073.S/science_goal.uid___A001_X12f_X20b/group.uid___A001_X12f_X20c/member.uid___A001_X12f_X20d/product/M87_CO_v_0_2_1_image.pbcor/12.minmax.gist_rainbow.direct.png"}}'

@apiParam {String} relFITSFilePath The path to the FITS file of interest
@apiParam {uInt} iFREQ An index along the `FREQ` axis.
@apiParam {String} [ittName] The intensity transformation table name.
@apiParam {String} [lutName] The look up table name.
@apiParam {String} [vmName] The video mode name.
"""
@route( baseUrl+'/getOneSliceAsPNG', name='getOneSliceAsPNG', method='POST')
@enable_cors
def getOneSliceAsPNG():
    logger.debug("getOneSliceAsPNG - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        iFREQ = getIntValue(body['iFREQ'])

        kwargs = {}
        for x in ['ittName', 'lutName', 'vmName']:
            if x in body :
                kwargs[x]=body[x]
        result = (dm.getOneSliceAsPNG(iFREQ, relFITSFilePath, **kwargs))
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getOneSliceAsPNG - wrapper : exiting")

    logger.debug(result)
    return result.toJSON()

"""
@api {POST} artemix/getSummedSliceRangeAsPNG Generates a PNG representation of a sum of RA,DEC planes.
@apiName getSummedSliceRangeAsPNG
@apiGroup imagery
@apiDescription

* Generates a PNG image of the sum of `RA,DEC` planes with indexes contained in a range `[iFREQ0, iFREQ1]` along the `FREQ` axis.

   * The image is built from the pixels values used as entries in the composition of an "intensity transfert table" (`ittName`), a "look up table" (`lutName`) and a "video mode" (`vmName`).
   * The image is written to the server's filesystem in a space which can be browsed.
   * The PNG file is named after `relFITSFilePath, iFREQ0, iFREQ1, ittName, lutName, vmName`.
   * The PNG images use 256 colours palettes.

   * Valid names values for `ittName, lutName, vmName` can be retrieved with the API
`renderingCapabilities`.

* Returns a dictionary with two entries : `{'data_steps': .... , 'path_to_png' : ...}`
    * `'data_steps'` : an association (dictionary) between the 256 RGB triples used in the PNG image and the 256 levels of the sampling of the physical values present in the `RA,DEC` plane. Example of an element of that dictionary : `"255_121_0": -0.037923078107483243`
    * `'path_to_png'` : the path to the PNG file relative to some root directory defined in the server's configuration. The important point is that it can be used as an URL, e.g.
    for visualization purpose a on a browser.

@apiParam {String} relFITSFilePath The path to the FITS file of interest
@apiParam {uInt} iFREQ An index along the `FREQ` axis.
@apiParam {String} [ittName] The intensity transformation table name.
@apiParam {String} [lutName] The look up table name.
@apiParam {String} [vmName] The video mode name.

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
'{"status": true, "message": "", "result": {"data_steps": {"255_0_41": -56.15247344970703, ..., "255_0_191": 77.84288787841797}, "path_to_png": "2013/2013.1.00073.S/science_goal.uid___A001_X12f_X20b/group.uid___A001_X12f_X20c/member.uid___A001_X12f_X20d/product/M87_CO_v_0_2_1_image.pbcor/6-18.minmax.gist_rainbow.direct.png"}}'
"""
@route( baseUrl+'/getSummedSliceRangeAsPNG', name='getSummedSliceRangeAsPNG', method='POST')
@enable_cors
def getSummedSliceRangeAsPNG():
    logger.debug("getSummedSliceRangeAsPNG - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        logger.debug("getSummedSliceRangeAsPNG(): body = %r" % json.loads(request.body.read()) )
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        iFREQ0 = getIntValue(body['iFREQ0'])
        iFREQ1 = getIntValue(body['iFREQ1'])

        kwargs = {}
        for x in ['ittName', 'lutName', 'vmName']:
            if x in body :
                kwargs[x]=body[x]
        result =  dm.getSummedSliceRangeAsPNG(relFITSFilePath, iFREQ0, iFREQ1,  **kwargs);
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getSummedSliceRangeAsPNG - wrapper : exiting")

    return result.toJSON()

@route( baseUrl+'/getContours', name='getContours', method='POST')
@enable_cors
def getContours():
    logger.debug("getContours - wrapper : entering")
    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        optParams = json.loads(body["optParams"])
        iFREQ =  getIntValue(optParams['iFREQ']) if 'iFREQ' in optParams else None
        iDEC0 =  getIntValue(optParams['iDEC0']) if 'iDEC0' in optParams else None
        iDEC1 =  getIntValue(optParams['iDEC1']) if 'iDEC1' in optParams else None
        iRA0 =  getIntValue(optParams['iRA0']) if 'iRA0' in optParams else None
        iRA1 =  getIntValue(optParams['iRA1']) if 'iRA1' in optParams else None

        kwargs = {}
        if 'levels' in optParams:
            levels = [getFloatValue(level) for level in optParams['levels']]
            kwargs['levels'] = levels

        if 'quantiles' in optParams:
            quantiles = [getFloatValue(level) for level in optParams['quantiles']]
            kwargs['quantiles'] = quantiles

        if 'numberOfBins' in optParams:
            numberOfBins = getIntValue(optParams['numberOfBins'])
            kwargs['numberOfBins'] = numberOfBins

        if 'histBinsMethod' in optParams:
            kwargs['histBinsMethod'] = optParams['histBinsMethod']

        logger.debug("kwargs = %r" % kwargs)
        result = dm.getContours(relFITSFilePath, iFREQ, iDEC0, iDEC1, iRA0, iRA1, **kwargs)

    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        logger.debug(message)
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getContours - wrapper : exiting")

    return result.toJSON()

@route( baseUrl+'/measureContour', name='measureContour', method='POST')
@enable_cors

def measureContour():
    logger.debug("measureContour - wrapper : entering")
    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        iFREQ = getIntValue(body['iFREQ'])
        contour = np.asarray(json.loads(body['contour']))
        level = getFloatValue(body['level'])
        result = dm.measureContour(relFITSFilePath, iFREQ, contour, level)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        logger.debug(message)
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("measureContour - wrapper : exiting")
    return result.toJSON()

@route( baseUrl+'/measureBox', name='measureBox', method='POST')
@enable_cors

def measureBox():
    logger.debug("measureContour - wrapper : entering")
    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        iFREQ = getIntValue(body['iFREQ'])
        iRA0 = getIntValue(body['iRA0']) if 'iRA0' in body else None
        iRA1 = getIntValue(body['iRA1']) if 'iRA1' in body else None
        iDEC0 = getIntValue(body['iDEC0']) if 'iDEC0' in body else None
        iDEC1 = getIntValue(body['iDEC1']) if 'iDEC1' in body else None
        result = dm.measureBox(relFITSFilePath, iFREQ, iRA0, iRA1, iDEC0, iDEC1)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        logger.debug(message)
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("measureContour - wrapper : exiting")
    return result.toJSON()

"""
    Given the values of a spectrum 'ydata' passed with the corresponding frequency values 'xdata'
    at an (x,y) given position ( integer indexes ) in the RAxDEC plan of a cube, generate a FITS file containing that
    spectrum. Supposedly be used for interoperability based on SAMP. A typical interlocutor is CASSIS

    @header the FITS Header of the file contaning the input data cube.
    @relFITSFilePath the path to the input data cube on disk.
    @iRA a single integer value containing the index along the RA axis
    @iDEC a single integer value containing the index along the DEC axis
"""
"""
@api {POST} /artemix/createFITS Creates a FITS table containing a spectrum.
@apiName createFITS
@apiGroup misc
@apiDescription Creates a FITS ASCII table whose content is a spectrum defined by a position in the
`RA,DEC` plane. Returns the FITS table as a sequence of bytes.

@apiParam {String} relFITSFilePath The path to the FITS file of interest
@apiParam {uInt} iRA index along the RA axis
@apiParam {uInt} iDEC index along the DEC axis

@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
 {'status': True, 'message': '', 'result': "SIMPLE  =                    T / conforms to FITS standard                      BITPIX  =                    8 / array data type                                NAXIS   =                    0 / number of array dimensions                     EXTEND  =                    T                                                  DATE    = '2015-12-08T18:24:15.408000'                                          DATE-OBS= '2015-08-16T19:00:41.616000'                                          TELESCOP= 'ALMA    '                                                            OBSERVER= 'jtan    '                                                            RESTFRQ =       230538000000.0                                                  SPECSYS = 'BARYCENT'                                                            COMMENT Spectrum of a single pixel of the cube                                  END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             XTENSION= 'TABLE   '           / ASCII table extension                          BITPIX  =                    8 / array data type                                NAXIS   =                    2 / number of array dimensions                     NAXIS1  =                   30 / length of dimension 1                          NAXIS2  =                   24 / length of dimension 2                          PCOUNT  =                    0 / number of group parameters                     GCOUNT  =                    1 / number of groups                               TFIELDS =                    2 / number of table fields                         TTYPE1  = 'Frequency'                                                           TFORM1  = 'E15.7   '                                                            TUNIT1  = 'Hz      '                                                            TBCOL1  =                    1                                                  TTYPE2  = 'Flux    '                                                            TFORM2  = 'E15.7   '                                                            TUNIT2  = 'Jy/beam '                                                            TBCOL2  =                   16                                                  END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               2.3043611E+11  3.2496294E-03  2.3035920E+11  3.1511623E-03  2.3028230E+11  2.3847336E-03  2.3020541E+11  2.2302533E-03  2.3012850E+11  2.0003885E-03  2.3005161E+11  1.2175933E-03  2.2997470E+11  1.1119752E-03  2.2989781E+11  3.6920310E-04  2.2982091E+11 -3.7513475E-04  2.2974402E+11 -3.3260061E-04  2.2966711E+11 -1.1299318E-03  2.2959020E+11 -1.6285286E-03  2.2951331E+11 -2.0787886E-03  2.2943641E+11 -1.5112817E-03  2.2935952E+11 -2.7610569E-03  2.2928261E+11 -2.2096562E-03  2.2920572E+11 -2.5501542E-03  2.2912881E+11 -2.2054571E-03  2.2905192E+11 -2.6812006E-03  2.2897502E+11 -2.6687880E-03  2.2889811E+11 -3.7882254E-03  2.2882122E+11 -3.9494671E-03  2.2874431E+11 -3.7729631E-03  2.2866742E+11 -4.9771518E-03                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "}
"""
@route( baseUrl+'/createFits', name='createFits', method='POST')
@enable_cors
def createFits():
    logger.debug("createFits - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = body['relFITSFilePath']
        iRA = getIntValue(body['iRA'])
        iDEC = getIntValue(body['iDEC'])
        result =  dm.createFits(relFITSFilePath, iRA, iDEC)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)


    response.content_type = "application/json; charset=utf-8"
    logger.debug("createFits - wrapper : exiting")

    return result.toJSON()

@route( baseUrl+'/createFITSSliceImage', name='createFITSSliceImage', method='POST')
@enable_cors
def createFITSSliceImage():
    logger.debug("createFITSSliceImage - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = body['relFITSFilePath']
        iFREQ = getIntValue(body['iFREQ'])
        result =  dm.createFITSSliceImage(relFITSFilePath, iFREQ)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("createFITSSliceImage - wrapper : exiting")

    return result.toJSON()

@route( baseUrl+'/createFITSSumSliceImage', name='createFITSSumSliceImage', method='POST')
@enable_cors
def createFITSSumSliceImage():
    logger.debug("createFITSSumSliceImage - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = body['relFITSFilePath']
        iFREQ0 = getIntValue(body['iFREQ0'])
        iFREQ1 = getIntValue(body['iFREQ1'])
        result =  dm.createFITSSumSliceImage(relFITSFilePath, iFREQ0,iFREQ1)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("createFITSSumSliceImage - wrapper : exiting")

    return result.toJSON()

@route( baseUrl+'/createSmoothCube', name='createSmoothCube', method='POST')
@enable_cors
def createSmoothCube():
    logger.debug("createSmoothCube - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = body['relFITSFilePath']
        nbox = getIntValue(body['nbox'])
        result =  dm.createSmoothCube(relFITSFilePath, nbox)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("createSmoothCube - wrapper : exiting")
    
    return result.toJSON()

"""
    Returns all the entries directly located under a given subdirectory.
    The subdirectory must be passed as a full path implicitely
    rooted at FITSFilePrefix.
    i.e. :
    "/" will be interpreted as FITSFilePrefix+"/"
    "/2018" will be interpreted as FITSFilePrefix+"/2018"
    etc...
    The result is a JSON array of dictionaries , on the basis of one dictionary per inode:
    [{'title': filename of the entry, 'key': the FITSFilePrefix rooted path of the entry, 'folder' : true (resp. false) if the inode is (resp. is not) a subdir, 'lazy': true}]
"""
@route( baseUrl+'/getEntries', name='getEntries', method='POST')
@enable_cors
def getEntries() :
    logger.debug("getEntries - wrapper : entering")
    try:
        body = byteify(json.loads(request.body.read()))
        relKey = body['relKey']
        result = dm.getEntries(relKey)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getEntries - wrapper : exiting")
    return result.toJSON()

"""
@api {GET} artemix/renderingCapabilities Useful informations for the production of PNG files.
@apiName renderingCapabilities
@apiGroup imagery
@apiDescription Returns informations useful for calls to APIs dedicated to the generation of PNG files.

The returned information is structured as follows :

{

'itts': _list of itt names_, 'default_itt_index': _index to default itt name_,

'luts': _list of lut names_, 'default_lut_index': _index to default lut name_,

'vmodes': _list of video mode names_, 'default_vmode_index': _index to default video mode name_

}
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{'status': True, 'message': '', 'result': {'itts': ['minmax', 'percent98'], 'default_itt_index': 0, 'luts': ['Greys', 'RdYlBu', 'hsv', 'gist_ncar', 'gist_rainbow', 'gist_gray', 'Spectral', 'jet', 'plasma', 'inferno', 'magma', 'afmhot', 'gist_heat'], 'default_lut_index': 2, 'vmodes': ['direct', 'inverse'], 'default_vmode_index': 0}}
"""
@route( baseUrl+'/renderingCapabilities', name='renderingCapabilities', method='GET')
@enable_cors
def renderingCapabilities():
    logger.debug("renderingCapabilities - wrapper : entering")
    try:
        result = dm.renderingCapabilities()
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("renderingCapabilities - wrapper : exiting")
    return result.toJSON()

"""
@api {POST} artemix/getDataBlockInfos getDataBlockInfos -
@apiName getDataBlockInfos
@apiGroup server
@apiDescription Returns informations about the server's characteristics and the DataBlock objects present in memory. A DataBlock
is an object created when a FITS file is read in memory; it encapsulates the FITS file content
plus a number of other fields.
@apiSuccessExample {json} Success-Response:
    HTTP/1.1 200 OK
{}

"""
@route( baseUrl+'/getDataBlockInfos', name='getDataBlockInfos', method='POST')
@enable_cors
def getDataBlockInfos():
    logger.debug("getDataBlockInfos - wrapper : entering")
    try:
        result = dm.getDataBlockInfos()
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getDataBlockInfos wrapper : exiting")
    return result.toJSON()

"""
  Erase the DataBlocks idle for a duration greater than DataBlock.getMaxIdle
  (env var YAFITSS_MAXIDLE)
"""
@route( baseUrl+'/purgeDataBlocks', name='purgeDataBlocks', method='POST')
@enable_cors
def purgeDataBlocks():
    logger.debug("purgeDataBlocks - wrapper : entering")
    try:
        result = dm.purgeDataBlocks()
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)
    response.content_type = "application/json; charset=utf-8"
    logger.debug("purgeDataBlocks- wrapper : exiting")
    return result.toJSON()

"""
  Writes a file containing a summary of opened cube for testing purpose
"""
@route( baseUrl+'/saveTest', name='saveTest', method='POST')
@enable_cors
def saveTest():
    logger.debug("saveTest - wrapper : entering")
    try:
        body = byteify(json.loads(request.body.read()))
        fileName = body['fileName']
        data = body['testData']
        cubeType = body['cubeType']

        if cubeType in ["1d", "2d", "3d"]:
            if not os.path.isdir(os.path.join(pm.OBJFilePrefix, cubeType)):
                os.mkdir(os.path.join(pm.OBJFilePrefix, cubeType))
            path = os.path.join(pm.OBJFilePrefix, cubeType, fileName.replace(".fits", "") + ".json" )
            with open(path, 'w') as f:
                json.dump(json.loads(data), f, indent=4)

            result = Result().ok("file written")
    except Exception as e:
        logger.debug(e)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        message = "%s ! %s" %  (type(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        traceback.print_exc()
        result = Result().wrong(message)
    response.content_type = "application/json; charset=utf-8"
    logger.debug("saveTest : exiting")
    return result.toJSON()


"""
    Given relObjFilePath

    produces a Obj file representing the surface of the fits image created by yt

    returns a dictionary { "path_to_obj": relOBJFilePath} where :

    * relOBJFilePath a path to the OBJ file relative to the root directory of OBJ files.
"""
@route( baseUrl+'/getYtObj', name='getYtObj', method='POST')
@enable_cors
def getYtObj():
    logger.debug("getYtObj - wrapper : entering")

    try:
        body = byteify(json.loads(request.body.read()))
        relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
        coord={"iRA0":getIntValue(body['iRA0']),"iRA1":getIntValue(body['iRA1']),"iDEC0":getIntValue(body['iDEC0']),"iDEC1":getIntValue(body['iDEC1']),"iFREQ0":getIntValue(body['iFREQ0']),"iFREQ1":getIntValue(body['iFREQ1'])}
        result = dm.getYtObj(relFITSFilePath,body['product'], coord)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" %  (repr(e), "".join(traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    response.content_type = "application/json; charset=utf-8"
    logger.debug("getYtObj - wrapper : exiting")

    return result.toJSON()

"""
Download a (supposedly) png file whose path appears after the prefix /static/png
"""
@route('/static/png/<filepath:path>')
def send_png(filepath):
    logger.debug("/static/png/<filepath:path>): entering")
    logger.debug(f"filepath=${filepath}")
    logger.debug("/static/png/<filepath:path>): exiting")
    return static_file(filepath, root=dataManager.PNGFilePrefix)

"""
Download a (supposedly) fits file whose path appears after the prefix /static/fits
dataManager.FITSFilePrefix points on data provided by the application manager
"""
@route('/static/fits/<filepath:path>')
def send_fits(filepath):
    logger.debug('/static/fits/<filepath:path>: entering')
    logger.debug(f"filepath=${filepath}")
    logger.debug('/static/fits/<filepath:path>: exiting')
    return static_file(filepath, root=dataManager.FITSFilePrefix, download=True)

"""
Download a (supposedly) fits file whose path appears after the prefix /static/spectrum
dataManager.SPECTRUMFilePrefix points on data generated by yafits
"""
@route('/static/spectrum/<filepath:path>')
def send_spectrum(filepath):
    logger.debug('/static/spectrum/<filepath:path>: entering')
    logger.debug(f"filepath=${filepath}")
    logger.debug('/static/spectrum/<filepath:path>: exiting')
    return static_file(filepath, root=dataManager.SPECTRUMFilePrefix, download=True)

@route('/apidoc/<filepath:path>')
#@route('/static/apidoc/<filepath:path>')
def send_apidoc(filepath):
    return static_file(filepath, root=dataManager.ApiDocPrefix)

# @route(baseUrl+'/ras', name='ras', method='POST')
# @enable_cors
# def ras():
#     logger.debug("ras - wrapper : entering")
#     try:
#         body = byteify(json.loads(request.body.read()))
#         logger.debug(f'{body}')
#         relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
#         result = dm.ras(relFITSFilePath)
#     except Exception as e:
#         exc_type, exc_value, exc_traceback = sys.exc_info()
#         traceback.print_exc()
#         message = "%s ! %s" % (repr(e), "".join(
#             traceback.format_list(traceback.extract_tb(exc_traceback))))
#         result = Result().wrong(message)

#     tbr = result.toStructOfFloat() #Attention ! This statement MUST imperatively precede the add_header 
#     response.content_type = "application/binary"
#     response.add_header('Python-Struct-Format', result.python_struct_format)
#     logger.debug([b for b in tbr[0:40]])
#     logger.debug("ras - wrapper : exiting")

#     return tbr

"""
@route(baseUrl+'/ras', name='ras', method='GET')
@enable_cors
def ras():
    logger.debug("ras - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        result = dm.ras(relFITSFilePath)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" % (repr(e), "".join(
            traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    # Attention ! This statement MUST imperatively precede the add_header
    tbr = result.toStructOfFloat().hex();
    response.content_type = "application/octet-stream"

    response.add_header('Python-Struct-Format', result.python_struct_format)
    logger.debug([b for b in tbr[0:40]])
    logger.debug("ras - wrapper : exiting")

    return tbr
"""
# @route(baseUrl+'/decs', name='decs', method='POST')
# @enable_cors
# def decs():
#     logger.debug("decs - wrapper : entering")
#     try:
#         body = byteify(json.loads(request.body.read()))
#         logger.debug(f'{body}')
#         relFITSFilePath = rebuildFilename(body['relFITSFilePath'])
#         result = dm.decs(relFITSFilePath)
#     except Exception as e:
#         exc_type, exc_value, exc_traceback = sys.exc_info()
#         traceback.print_exc()
#         message = "%s ! %s" % (repr(e), "".join(
#             traceback.format_list(traceback.extract_tb(exc_traceback))))
#         result = Result().wrong(message)

#     # Attention ! This statement MUST imperatively precede the add_header
#     tbr = result.toStructOfFloat()
#     response.content_type = "application/binary"
#     response.add_header('Python-Struct-Format', result.python_struct_format)
#     logger.debug([b for b in tbr[0:40]])
#     logger.debug("decs - wrapper : exiting")

#     return tbr

"""
@route(baseUrl+'/decs', name='decs', method='GET')
@enable_cors
def decs():
    logger.debug("decs - wrapper : entering")
    try:
        relFITSFilePath = rebuildFilename(request.GET['relFITSFilePath'])
        result = dm.decs(relFITSFilePath)
    except Exception as e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_exc()
        message = "%s ! %s" % (repr(e), "".join(
            traceback.format_list(traceback.extract_tb(exc_traceback))))
        result = Result().wrong(message)

    # Attention ! This statement MUST imperatively precede the add_header
    tbr = result.toStructOfFloat().hex()
    response.content_type = "application/octet-stream; charset=ASCII"
    response.add_header('Python-Struct-Format', result.python_struct_format)
    logger.debug([b for b in tbr[0:40]])
    logger.debug("decs - wrapper : exiting")

    return tbr
"""
#
#    End of the server functions definitions
#
#


"""
    start local bottle server
    @port port number
    @debug True if debug enabled
"""
def main(argv):

    global logger
    global dm
    global app

    try :
        print (argv)
        opts, args = getopt.getopt(argv[1:], "p:", ["port="])
        print (opts)
    except getopt.GetoptError:
        print ("Bad command")
        sys.exit(2)

    port = 4251
    for opt, arg in opts:
        if opt in ('-p', '--port'):
            port = int(arg)

    # Do the logging stuff
    theLogFile = f'{pm.logDir}yafitss-{port}.log'
    logger = log.getLogger(argv[0], theLogFile)

    hostname = socket.gethostname()
    logger.debug(f"{argv[0]} just starting server on {hostname}:{port}")
    logger.debug(f"Using astropy version {astropy.version.version}")

    # a timer for measuring execution time
    # the file is cleared at each server restart
    try : 
        os.remove(f'{pm.logDir}yafitss-bench.log')
    except Exception as e:
        traceback.print_exc()
    timerLogger = log.getLogger("timer", f'{pm.logDir}yafitss-bench.log')

    timer = time.Timer(timerLogger)

    # Obtain a data manager implementation
    dm = dataManager.DataManagerImpl(logger, timer)

    app = default_app()
    run(host=hostname, port=port, debug=True, quiet=False)

    return

if __name__ == '__main__':
    main(sys.argv)
