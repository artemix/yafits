import os
import logging
import logging.handlers

class LogManager:
    def __init__(self):        
        self.__maxLogSize = self.__getMaxLogSize()
        self.__logBackupCount = self.__getLogBackupCount()

    @property
    def maxLogSize(self):
        return self.__maxLogSize

    @property
    def logBackupCount(self):
        return self.__logBackupCount

    def attachRotatingFileHandler(self, logger: logging.Logger, filePath: str):
        """Attaches a RotatingFileHandler to the given logger. The output will be written
           in filePath

           Parameters
           ----------

            logger : logging.Logger
                     a Logger object

            filepath : str
                       The path of the file where log content will be written

        
        """
        try:
            ch = logging.handlers.RotatingFileHandler(
                filePath, mode='a', maxBytes=self.__maxLogSize, backupCount=self.__logBackupCount)
            # create formatter
            formatter = logging.Formatter(
                '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            # add formatter to ch
            ch.setFormatter(formatter)
            # add ch to logger
            logger.addHandler(ch)
        except Exception as e:
            logger.debug(e)    

    def __getMaxLogSize(self) -> int:
        """Returns maximum size of log file
        
            Returns
            -------

            int : maximum size of log file in bytes
        """
        maxLogSizeDflt = 1024 * 1024
        maxLogSize = 0
        try:
            maxLogSize = int(os.getenv('YAFITS_MAXLOGSIZE'))            
            if maxLogSize < 0:
                raise
            else:
                return maxLogSize
        except Exception as e:
            maxLogSize = maxLogSizeDflt
            return maxLogSize

    def __getLogBackupCount(self) -> int:           
        """Returns number of log files keeped before deletion

            Returns
            -------

            int : number of log files keeped before deletion
        """

        logBackupCountDflt = 2
        logBackupCount = 0
        try:
            logBackupCount = int(os.getenv('YAFITS_LOGBACKUPCOUNT'))
            if logBackupCount < 0:
                raise
            else:
                return logBackupCount
        except Exception as e:
            logBackupCount = logBackupCountDflt
            return logBackupCount
    


def getLogger(name: str, filePath: str) -> logging.Logger:
    """Returns a Logger object saving data in the file located at filePath
       and identified as name by a RotatingFileHandler

       Parameters
       ----------

       name : str
              indentifier of logger for the RotatingFileHandler to which it will be attached

       filePath : str
              path where log content will be written

       Returns
       -------

       logging.Logger : a Logger object instance 
    """
    # Do the logging stuff
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logManager = LogManager()
    logManager.attachRotatingFileHandler(logger, filePath)

    return logger
