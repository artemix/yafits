import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join("..", "..", "..")))
sys.path.insert(0, os.path.abspath(os.path.join("..", "..", "..", "yafitss")))

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Yafitss-doc'
copyright = '2024, Yafits'
author = 'Yafits'

version = '0.1'
release = '0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx.ext.coverage',
    'sphinx.ext.napoleon'
]

#autodoc_mock_imports = ["dask", "bottle", "timer", "pyvo", "astropy", "numpy", "matplotlib", "cv2"]

templates_path = ['_templates']
exclude_patterns = []




# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
