import sys
import logging
import timer
sys.path.append("..")
import unittest

from dataBlock import DataBlock

class TestConvertSize(unittest.TestCase):
    def test_0(self):
        self.assertEqual(DataBlock.convert_size(0), '0B')


    def test_kb(self):
        self.assertEqual(DataBlock.convert_size(1024), '1.0 KB')
        self.assertEqual(DataBlock.convert_size(2048), '2.0 KB')

    def test_Mb(self):
        self.assertEqual(DataBlock.convert_size(10485760), '10.0 MB')

    def test_Gb(self):
        self.assertEqual(DataBlock.convert_size(3221225472), '3.0 GB')

class TestRenderingCapabilities(unittest.TestCase):
    def setUp(self):
        pass

    def test_itts(self):
        self.assertEqual("minmax" in DataBlock.getRenderingCapabilities()["itts"], True)
        self.assertEqual("percent95" in DataBlock.getRenderingCapabilities()["itts"], True)
        self.assertEqual("percent98" in DataBlock.getRenderingCapabilities()["itts"], True)
        self.assertEqual("percent99" in DataBlock.getRenderingCapabilities()["itts"], True)

    def test_default_itt_index(self):
        default_itt = DataBlock.getDefaults()[1]
        self.assertEqual(DataBlock.getRenderingCapabilities()["default_itt_index"],  
                         DataBlock.getRenderingCapabilities()["itts"].index(default_itt))

    def test_default_lut_index(self):
        default_lut = DataBlock.getDefaults()[0]
        self.assertEqual(DataBlock.getRenderingCapabilities()["default_lut_index"],  
                         DataBlock.getRenderingCapabilities()["luts"].index(default_lut))
        
    def test_default_lut_index(self):
        default_vmode = DataBlock.getDefaults()[2]
        self.assertEqual(DataBlock.getRenderingCapabilities()["default_vmode_index"],  
                         DataBlock.getRenderingCapabilities()["vmodes"].index(default_vmode))
        
    def test_defaults(self):
        self.assertEqual(DataBlock.getDefaults()[0],  DataBlock.getDefaultPaletteName()) 
        self.assertEqual(DataBlock.getDefaults()[1],  DataBlock.getDefaultTransformationName()) 
        self.assertEqual(DataBlock.getDefaults()[2],  DataBlock.getDefaultVideoModeName()) 

class TestInfod(unittest.TestCase):
    def setUp(self):
        log = logging.getLogger("test")
        time = timer.Timer(logging.getLogger("timer"))
        self.datablock = DataBlock(log, time)

    def test_0(self):
        fields = DataBlock.getDataBlockInfoNames()
        for field in fields:
            self.assertTrue(field in self.datablock.infod)

    def test_1(self):
        fields = DataBlock.getDataBlockInfoNames()
        self.assertEqual(len(self.datablock.infod.keys()), len(fields))


class TestSetdata3DIRAM(unittest.TestCase):
    def setUp(self):
        log = logging.getLogger("test")
        time = timer.Timer(logging.getLogger("timer"))
        self.datablock = DataBlock(log, time)
        self.result = self.datablock.setData("//restricted/testing/3d-iram-gildas-30m-rand-12co10-cube-2.fits")
        

    def test_load(self):
        self.assertTrue(self.result.status)

    def test_header_telescop(self):
        self.assertEqual(self.datablock.header["TELESCOP"], "30M+PDBI")

    def test_header_instrume(self):
        self.assertEqual(self.datablock.header["INSTRUME"], "Unknown")

    def test_header_origin(self):
        self.assertEqual(self.datablock.header["ORIGIN"], "GILDAS CUBE")

    def test_header_specsys(self):
        self.assertEqual(self.datablock.header["SPECSYS"], "LSRK")    

    def test_header_restfrq(self):
        self.assertEqual(self.datablock.header["RESTFRQ"], 0.1152712018000E+12)

    def test_header_radesys(self):
        self.assertEqual(self.datablock.header["RADESYS"], "ICRS")

    def test_header_cunit3(self):
        self.assertEqual(self.datablock.header["CUNIT3"], "m/s")


    #def test_1(self):
    #    fields = DataBlock.getDataBlockInfoNames()
    #    self.assertEqual(len(self.datablock.infod.keys()), len(fields))

def suite():
    suite = unittest.TestSuite()
    suite.addTest(TestConvertSize('test_convert_size'))
    suite.addTest(TestRenderingCapabilities('test_rendering_capabilities'))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())