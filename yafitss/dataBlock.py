#
# A class to pack the content of a FITS file (IMAGE) and related data.
# --------------------------------------------------------------------
#
# Michel Caillat
# Observatoire de Paris - LERMA
# 27/06/2019
#
import math
from astropy.io import fits
from astropy import wcs
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.coordinates import FK5 
from astropy.time import Time
from astropy import constants as const
import numpy as np
import json
import copy
from PIL import Image, PngImagePlugin
import dask
import dask.array as da
import dask.delayed as dd

import uuid
import traceback
from datetime import datetime
import os
import re
from functools import reduce
from matplotlib import pyplot as plt
import copy
import pathManager as pm
import lutColors as lutc
import timer as time
import cv2

from result import Result
from typing import Dict

def cmap2palette (palette_name):
    x = plt.get_cmap(palette_name)
    return [tuple(map(lambda x: int(round(x*255)), x(i)[0:3])) for i in range(x.N)]

def getLegendFilePath(pngFilePath):
    filename = pngFilePath.split(".png")
    return filename[0]+"_legend.png"

def isNanList(checked_list):
    """Returns true if list contains only nan values

    Parameters
    ----------
    checked_list : list

    Returns
    -------
    boolean
    
    """
    result = True
    for value in checked_list:
        if not math.isnan(value):
            result = False
            break

    return result




class DataBlock:
    """A class to pack the content of a FITS file (IMAGE) and related data.
        
    Attributes
    ----------
    __palette_names : string list
        the palette names
    __palette : ???
        to
    __default_palette_name : str
        the default palette name
    __transformation_names : str list
        the transformation names
    __default_transformation_name : str
        the default transformation name
    __video_mode_names : str list
        the video mode names
    __default_video_mode_name : str
        the default video mode name
    __renderingCapabilities : dictionary
        the rendering capabilities
    __dateTimeFormat : str
        date time format
    __naxisn_rexp : str
        a regulare expression to catch the NAXISn
    __max_idle : float
        maximum idleness duration for an instance of DataBlock, 
        retrieved from the env var YAFITSS_MAXIDLE
    """
    __palette_names = ["afmhot", "gist_gray", "gist_heat", "gist_ncar", "gist_rainbow", "Greys", "hsv",
                        "inferno", "iram", "jet", "magma", "plasma","RdYlBu",  "Spectral", "viridis"]
    __palette = {}#{palette_name: cmap2palette(palette_name) for palette_name in __palette_names}
    __default_palette_name = "iram"

    __transformation_names = ["minmax", "percent99", "percent98", "percent95"]
    __default_transformation_name = "percent99"

    __video_mode_names = ["direct", "inverse"]
    __default_video_mode_name = "direct"

    __renderingCapabilities = {
        "itts": __transformation_names,
        "default_itt_index": __transformation_names.index(__default_transformation_name),
        "luts": __palette_names,
        "default_lut_index": __palette_names.index(__default_palette_name),
        "vmodes": __video_mode_names,
        "default_vmode_index": __video_mode_names.index(__default_video_mode_name)
    }

    # size of displayed images in pixels in yafitsv
    __image_width = 512
    __image_height = 512

    __dateTimeFormat = "%m/%d/%Y - %H:%M:%S"

    __naxisn_rexp = re.compile("NAXIS[1-9]")

    __max_idle = float(os.environ["YAFITSS_MAXIDLE"])

    #===========================================================================
    # Class methods - Setters and getters
    @classmethod
    def getPaletteFromName(cls, name):
        return DataBlock.__palette[name] if name in DataBlock.__palette_names else  DataBlock.__palette[DataBlock.__default_palette_name]

    @classmethod
    def getDefaultPaletteName(cls):
        return DataBlock.__default_palette_name
    
    @classmethod
    def getDefaultTransformationName(cls):
        return DataBlock.__default_transformation_name
    
    @classmethod
    def getDefaultVideoModeName(cls):
        return DataBlock.__default_video_mode_name

    @classmethod
    def getDefaults(cls):
        return (DataBlock.__default_palette_name, DataBlock.__default_transformation_name, DataBlock.__default_video_mode_name)

    @classmethod
    def setFITSFilePrefix(cls, FITSFilePrefix):
        DataBlock.__FITSFilePrefix = FITSFilePrefix

    @classmethod
    def setPNGFilePrefix(cls, PNGFilePrefix):
        DataBlock.__PNGFilePrefix = PNGFilePrefix

    @classmethod
    def setOBJFilePrefix(cls, OBJFilePrefix):
        DataBlock.__OBJFilePrefix = OBJFilePrefix

    @classmethod
    def setSMOOTHFilePrefix(cls, SMOOTHFilePrefix):
        DataBlock.__SMOOTHFilePrefix = SMOOTHFilePrefix

    @classmethod
    def setIMGFilePrefix(cls, IMGFilePrefix):
        DataBlock.__IMGFilePrefix = IMGFilePrefix

    @classmethod
    def getFITSFilePrefix(cls):
        return DataBlock.__FITSFilePrefix

    @classmethod
    def getPNGFilePrefix(cls):
        return DataBlock.__PNGFilePrefix

    @classmethod
    def getOBJFilePrefix(cls):
        return DataBlock.__OBJFilePrefix

    @classmethod
    def getSMOOTHFilePrefix(cls):
        return DataBlock.__SMOOTHFilePrefix
    
    @classmethod
    def getIMGFilePrefix(cls):
        return DataBlock.__IMGFilePrefix

    @classmethod
    def getRenderingCapabilities(cls):
        return DataBlock.__renderingCapabilities

    @classmethod
    def getDefaultITTName(cls):
        return DataBlock.__default_transformation_name

    @classmethod
    def getDefaultLUTName(cls):
        return DataBlock.__default_palette_name

    @classmethod
    def getDefaultVMName(cls):
        return DataBlock.__default_video_mode_name

    @classmethod
    def convert_size(cls, sizeInBytes):
        """Convert size in byte format
        Parameters
        ----------
        cls : DataBlock
        sizeInBytes : int

        Returns
        -------
        string
            a byte format

        """
        if sizeInBytes == 0:
            return "0B"
        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(sizeInBytes, 1024)))
        p = math.pow(1024, i)
        s = round(sizeInBytes / p, 2)
        return "%s %s" % (s, size_name[i])

    @classmethod
    def getDataBlockInfoNames(cls):
        return ["path", "creationTime", "lastAccess", "naxisn", "size", "idle"]

    @classmethod
    def getMaxIdle(cls):
        return DataBlock.__max_idle

    @classmethod
    def getDateTimeFormat(cls):
        return DataBlock.__dateTimeFormat

    #===========================================================================
    # CTOR
    #

    def __init__(self, logger, timer):
        """
            Parameters
            ----------
            __logger : logger
                tracks events that happen when class is called
        """
        self.__logger = logger
        self.__relFITSFilePath = None
        self.__data = None      # The data part of the FITS file
        self.__header = None      # The FITS file header as a dictionary
        self.__sliceMargin = None
        self.__wcs2 = None      # WCS object
        self.__wcs3 = None      # WCS object
        self.__convert = None
        self.__cdelt = None
        self.__lastAccess = None    # the last time a method was called on self
        self.__creationTime = None      # the date of birth
        self.__sizeInBytes = None
        self.__naxisn = None
        self.__logger.debug("A DataBlock has been just built")



        self.__timer = timer
        self.__timerPrefix = uuid.uuid4()

        self.__infod = {
            "path": lambda: self.__relFITSFilePath,
            "creationTime": lambda: self.__creationTime.strftime(DataBlock.getDateTimeFormat()),
            "lastAccess": lambda: self.__lastAccess.strftime(DataBlock.getDateTimeFormat()),
            "naxisn": lambda: self.__naxisn,
            "size": lambda: DataBlock.convert_size(self.__sizeInBytes),
            "idle": None
        }

        self.__statistics ={}

    #===========================================================================
    #
    # Properties
    #
    @property
    def relFITSFilePath(self):
        return self.__relFITSFilePath

    @property
    def header(self):
        return self.__header

    @property
    def lastAccess(self):
        return self.__lastAccess

    @property
    def creationTime(self):
        return self.__creationTime

    @property
    def sizeInBytes(self):
        return self.__sizeInBytes

    @property
    def naxisn(self):
        return self.__naxisn

    @property
    def infod(self):
        return self.__infod
#
#===========================================================================
#
# Public setters
#

    def setLastAccess(self):
        """Set value of lastAccess on self
        """
        self.__lastAccess = datetime.now()

    #
    # 
    #
    def setData(self, relFITSFilePath: str) -> Result:
        """Given a FITS file path: read the content of the FITS file in memory and populates the instance variables accordingly

            Parameters
            ----------
            relFITSFilePath : str
                a relative fits file path

            Returns
            -------
            Result
                a formated result

            Raises
            ------
            KeyError
                if the __header["RESTFRQ"] does not exist
            Exception
                Error while opening fits file

        """
        # unique prefix to follow log when several files are opened at the same time

        self.__timer.addComment(f"Start working on {os.path.basename(relFITSFilePath)}", self.__timerPrefix)
        self.__timer.start()
        self.__logger.debug("setData: entering")
        self.__logger.debug( "Setting data: %s" % relFITSFilePath)
        absFITSFilePath = pm.getAbsFITSFilePath(relFITSFilePath)
        self.__logger.debug(f"Full path is {absFITSFilePath}")
        hdu_list = None
        try:
            hdu_list = fits.open(absFITSFilePath)
            self.__logger.debug(f"Opened f{absFITSFilePath}")
            # MUSE detecttion.
            data_index = "PRIMARY"
            primary_header = hdu_list[data_index].header
            if "EXTEND" in primary_header and \
                primary_header["EXTEND"] and \
                "INSTRUME" in primary_header and \
                primary_header["INSTRUME"].strip() == "MUSE" and \
                "DATA" in hdu_list :
                    data_index = "DATA"
                    self.__header = hdu_list[data_index].header
                    self.__header["ORIGIN"] = primary_header["ORIGIN"].strip()
                    self.__header["INSTRUME"] = primary_header["INSTRUME"].strip()
                    self.__header["TELESCOP"] = primary_header["TELESCOP"].strip()
                    self.__header["OBSERVER"] = primary_header["OBSERVER"].strip()
                    self.__header["CDELT1"] = self.__header["CD1_1"]
                    self.__header["CDELT2"] = self.__header["CD2_2"]
                    if "CD3_3" in self.__header:
                        self.__header["CDELT3"] = self.__header["CD3_3"]
                    self.__header["RADESYS"] = primary_header["RADECSYS"].strip()
                    self.__header["DATE-OBS"] = primary_header["DATE"].strip()
                    self.__header["DATE"] = primary_header["DATE"].strip()
                    self.__logger.debug("We are in MUSE")
            # This is NOT a MUSE observation
            else:
                self.__header = hdu_list[data_index].header

            numDims = len(hdu_list[data_index].data.shape)
            if numDims > 4 or numDims < 2 :
                self.__logger.debug("setData: Exiting")
                return Result().wrong("Unacceptable value of numDims: %s" % numDims)

            if numDims == 4:
                # 1D spectrum
                if hdu_list[data_index].data.shape[0:3]==(1,1,1):
                    self.__data = da.transpose(hdu_list['PRIMARY'].data, [3,2,1,0])
                    #self.__data = da.transpose(hdu_list[data_index].data, [0,3,2,1]) 
                else : 
                    self.__data = da.from_array(hdu_list[data_index].data[0], chunks = (hdu_list[data_index].data[0].shape[0], 128, 128))

            elif numDims == 3:
                self.__data = da.from_array(hdu_list[data_index].data, chunks = (hdu_list[data_index].data.shape[0], 128, 128))
            elif numDims == 2:
                self.__data = da.from_array(hdu_list[data_index].data, chunks = (128, 128))
 
            shp = self.__data.shape
            if numDims > 2 :
                if shp[1] > shp[2]:
                    self.__sliceMargin = da.from_array(np.nan * np.ones(shape=(shp[1], shp[1] - shp[2])), chunks=(128, 128))
                elif shp[1] < shp[2]:
                    self.__sliceMargin = da.from_array(np.nan * np.ones(shape=(shp[2] - shp[1], shp[2])), chunks=(128, 128))
                else:
                    pass
            else :
                if shp[0] > shp[1]:
                    self.__sliceMargin = da.from_array(np.nan * np.ones(shape=(shp[0], shp[0] - shp[1])), chunks=(128, 128))
                elif shp[0] < shp[1]:
                    self.__sliceMargin = da.from_array(np.nan * np.ones(shape=(shp[1] - shp[0], shp[1])), chunks=(128, 128))
                else:
                    pass

            #
            # Header "normalization"
            #
            if "TELESCOP" not in self.__header:
                self.__header["TELESCOP"] = "Unknown"

            if "INSTRUME" not in self.__header:
                self.__header["INSTRUME"] = "Unknown"

            if "OBSERVER" not in self.__header:
                self.__header["OBSERVER"] = "John Doe"

            if "ORIGIN" not in self.__header:
                self.__header["ORIGIN"] = "Unknown"

            if "SPECSYS" not in self.__header:
                self.__header["SPECSYS"] = "Unknown"

            if "RESTFRQ" not in self.__header:
                if "RESTFREQ" in self.__header:
                    self.__header["RESTFRQ"] = self.__header["RESTFREQ"]
                else:
                    self.__header["RESTFRQ"] = 0.0

            if "RADESYS" not in self.__header:
                self.__header["RADESYS"] = "ICRS"

            if self.__header["ORIGIN"] == "GILDAS Consortium":
                if "CUNIT3" not in self.__header:
                    self.__header["CUNIT3"] = "m/s"
                if "DATE-OBS" not in self.__header:
                    self.__header["DATE-OBS"] = self.__header["DATE"]
                self.__header["TELESCOP"] = "NOEMA"

            if "CUNIT3" in self.__header and  self.__header["CUNIT3"] == "M/S":
                self.__header["CUNIT3"] = "m/s"

            if "BUNIT" not in self.__header:
                self.__header["BUNIT"] = "Undefined"
            elif self.__header["INSTRUME"] == "SITELLE" \
                and self.__header["BUNIT"] == "FLUX":
                self.__header["BUNIT"] = "erg/s/cm^2/A/arcsec^2"
            elif self.__header["BUNIT"] == "JY/BEAM":
                self.__header["BUNIT"] == "Jy/beam"

            if "BTYPE" not in self.__header:
                self.__header["BTYPE"] = "Unknown"

            # convert EQUINOX from 1950 to 2000 as we can only display the latter
            """if self.__header['EQUINOX'] == 1950:
                # RA/DEC and CRVAL1/CRVAL2 should be equal but we treat them separetely
                # in case there is a special case we did not thing about
                coords = self.__changeEquinox1950To2000(self.__header["RA"], self.__header["DEC"])
                self.__header['EQUINOX'] = "2000  # ! EQUINOX = 1950 in source file - RA/DEC/CRVAL1/CRVAL2 have been modified accordingly"
                self.__header["RA"] = coords.ra.degree
                self.__header["DEC"] = coords.dec.degree

                coords = self.__changeEquinox1950To2000(self.__header["CRVAL1"], self.__header["CRVAL2"])
                self.__header["CRVAL1"] = coords.ra.degree
                self.__header["CRVAL2"] = coords.dec.degree"""


            #
            # This section has been developped especially for one big file used for the SKA DC2
            #
            if 'ORIGIN' in self.__header and self.__header['ORIGIN'].startswith('Miriad fits') and numDims == 3:
                if 'CTYPE3' in self.__header and self.__header['CTYPE3'] == 'FREQ':
                    self.__header['CUNIT1'] = self.__dVal(self.__header, 'CUNIT1', 'deg')
                    self.__header['CUNIT2'] = self.__dVal(self.__header, 'CUNIT2', 'deg')
                    self.__header['CUNIT3'] = self.__dVal(self.__header, 'CUNIT3', 'Hz')

            self.__logger.debug("Working on '%s'" % (relFITSFilePath))

            # 
            # Let's get rid of "HISTORY", "COMMENT" and "" entries in the header
            # *None* prevents throwing error
            self.__header.pop("HISTORY", None)
            self.__header.pop("COMMENT", None)
            self.__header.pop("", None)

            try:
                self.__logger.debug("Got RESTFRQ %f" % self.__header["RESTFRQ"])
            except KeyError:
                self.__header["RESTFRQ"] = self.__header["RESTFREQ"]

            self.__wcs2 = wcs.WCS(self.__header, naxis=2)
            if self.__header["INSTRUME"] != "SITELLE" and self.__header["NAXIS"] > 2:
                self.__wcs3 = wcs.WCS(self.__header, naxis=3)

            pi180 = 3600 #math.pi / 180 / 4.86e-6

            self.__convert = 1.0
            self.__cdelt = 1.0
            if self.__header["BUNIT"].startswith("Jy/beam"):
                try:
                    bmaj = self.__header["BMAJ"] * pi180
                    bmin = self.__header["BMIN"] * pi180
                    self.__convert = math.pi * bmaj * bmin
                    self.__cdelt =  4 * math.log(2) * math.fabs(self.__header["CDELT1"] * pi180) * math.fabs(self.__header["CDELT2"] * pi180)
                except KeyError:
                    pass

            self.__relFITSFilePath = relFITSFilePath

            # date of birth == lastAccess == now
            self.__lastAccess = self.__creationTime = datetime.now()
            naxisnValues = [self.__header[key] for key in self.__header if DataBlock.__naxisn_rexp.match(key)]
            self.__sizeInBytes = reduce(lambda x, y: x*y, naxisnValues) * abs(self.__header["BITPIX"]) / 8
            self.__naxisn = reduce(lambda x, y: "%sx%s"%(x,y), naxisnValues)

            result = self.getHeader()

            if numDims == 4 and hdu_list[data_index].data.shape[0] > 1:
                result.message=f"This dataset has NAXIS4 == {hdu_list[data_index].data.shape[0]} . yafits does not fully handle 4D data. As a default behaviour yafits will display the cube [0, :, : , :]"

        except Exception as e:
            message = f"Error while opening file {relFITSFilePath}: {e}"
            self.__logger.debug(message)
            self.__logger.debug(f"Traceback is : {traceback.format_exc()}")
            print(e)
            traceback.format_exc()
            result = Result().wrong(message)

        if hdu_list:
            hdu_list.close()
        self.__logger.debug("setData: Exiting")
        self.__timer.stop("setData", self.__timerPrefix)
        return result


    #===========================================================================
    #
    # Private methods
    #
    def __changeEquinox1950To2000(self, ra, dec):
        """ Converts coordinates from J1950 to J2000 referential

            Parameters
            ----------
            ra: float
                right ascension in degrees
            
            dec: float
                 declination in degrees

            Returns
            -------
            Result
                a SkyCoord object in J2000 coordinates

            Raises
            ------
            KeyError
                if the __header["RESTFRQ"] does not exist
            Exception
                Error while opening fits file
        """
        fk5c = SkyCoord(ra, dec, unit="deg", frame=FK5(equinox=Time('J1950')))
        fk5_2000 = FK5(equinox=Time(2000, format='jyear'))
        return fk5c.transform_to(fk5_2000)


    def __getPixelValueAtiFreqiRAiDEC(self, iFreq: int, iRA: int, iDEC: int) -> dict:
        """ Returns value of a pixel given coordinates in freq, ra and dec axis

            Parameters
            ----------

            iFreq : int
                coordinate in Freq axis

            iRA : int
                coordinate in RA axis

            iDEC : int
                coordinate in DEC axis

            Returns
            -------
            Result
                a dict object containing the value, unit and type (btype value)

        """

        self.__logger.debug("__getPixelValuAtiFreqiRAiDEC: entering")
        self.__logger.debug("iFreq = {:d}, iRA = {:d}, iDEC = {:d}".format(iFreq, iRA, iDEC) )
        result = self.__data[iFreq, iDEC, iRA].compute()
        if np.isnan(result):
            result = None
        else:
            result = {"value": float(result), "unit": self.__header["BUNIT"], "type": self.__header["BTYPE"]}
        self.__logger.debug("__getPixelValuAtiFreqiRAiDEC: exiting")
        return result

    def __getPixelValueAtiRAiDEC(self, iRA: int, iDEC: int) -> dict: 
        """ Returns value of a pixel given coordinates in ra and dec axis

            Parameters
            ----------

            iRA : int
                coordinate in RA axis

            iDEC : int
                coordinate in DEC axis

            Returns
            -------
            Result
                a json object containing the value, unit and type (btype value)

        """

        self.__logger.debug("__getPixelValuAtiRAiDEC: entering")
        self.__logger.debug("iRA = {:d}, iDEC = {:d}".format(iRA, iDEC))
        result = self.__data[iDEC, iRA].compute()
        if np.isnan(result):
            result = None
        else:
            result = {"value": float(result), "unit": self.__header["BUNIT"], "type": self.__header["BTYPE"]}

        self.__logger.debug("__getPixelValuAtiRAiDEC: exiting")
        return result

    def __getFreqsAtiRAiDEC(self, iRA, iDEC):
        self.__logger.debug("__getFreqsAtiRAiDEC: entering")
        x = [[iRA, iDEC, iFreq] for iFreq in range(self.__header["NAXIS3"])]
        result = [crdnn[2] for crdnn in self.__wcs3.all_pix2world(x, 0)]
        self.__logger.debug("__getFreqsAtiRAiDEC: exiting")
        return result

    def __getSumOverSliceRectArea_0(self, iFREQ, iRA0=None, iRA1=None, iDEC0=None, iDEC1=None):
        self.__logger.debug("__getSumOverSliceRectArea_0: entering")

        if self.__header["NAXIS"] > 2:
            result = (dask.array.nansum(self.__data[iFREQ, iDEC0:iDEC1, iRA0:iRA1]) / self.__convert * self.__cdelt).compute().tolist()
        else :
            result = (dask.array.nansum(self.__data[iDEC0:iDEC1, iRA0:iRA1]) / self.__convert * self.__cdelt).compute().tolist()

        self.__logger.debug("__getSumOverSliceRectArea__0: exiting")

        return result

    def __getAverage_0(self, iFREQ0=None, iFREQ1=None, iDEC0=None, iDEC1=None, iRA0=None, iRA1=None):
        self.__logger.debug("__getAverage_0: entering")
        self.__logger.debug("shape length: %d" % len(self.__data.shape))

        if iRA0 == None:
            iRA0 = 0
        if iRA1 == None:
            iRA1 = self.__data.shape[2]
        if iDEC0 == None:
            iDEC0 = 0
        if iDEC1 == None:
            iDEC1 = self.__data.shape[1]
        if iFREQ0 == None:
            iFREQ0 = 0
        if iFREQ1 == None:
            iFREQ1 = self.__data.shape[0]

        self.__logger.debug(f"{iFREQ0}, {iFREQ1}, {iDEC0}, {iDEC1}, {iRA0}, {iRA1}")

        if self.__header["ORIGIN"].startswith("GILDAS"):
            self.__logger.debug("Gildas data")
            cdelt = math.fabs(self.__header["CDELT3"])/1000.
            self.__logger.debug("CDELT = %f"%cdelt)
        elif (self.__header["ORIGIN"].startswith("CASA") or self.__header["ORIGIN"].startswith("Miriad")
              or self.__header["INSTRUME"].upper() == "MEERKAT"):
            self.__logger.debug("ALMA data")
            cunit3 = self.__header["CUNIT3"]
            if cunit3 == "m/s":
                cdelt = math.fabs(self.__header["CDELT3"]) / 1000.0
            elif cunit3 == "Hz":
                #cdelt = 3e+5 * math.fabs(self.__header["CDELT3"] / self.__header["RESTFRQ"])
                #ALTRVAL
                cdelt = 3e+5 * math.fabs(self.header["CDELT3"] / self.header["CRVAL3"])
            else:
                cdelt = 0.0
        elif self.__header["INSTRUME"] == "SITELLE":
            cdelt = 1.0
        elif self.__header["INSTRUME"] == "MUSE":
            cdelt = 1.0
        else:
            cdelt = 0.0


        if self.__header["NAXIS"] > 2:
            result = (dask.array.sum(dask.array.nan_to_num(self.__data[iFREQ0:iFREQ1+1, iDEC0:iDEC1+1, iRA0:iRA1+1]), 0)*cdelt)
        else:
            result = (self.__data[iDEC0:iDEC1+1, iRA0:iRA1+1]*cdelt)

        self.__collectStatistics("%d-%d"%(iFREQ0, iFREQ1), result.compute())

        self.__logger.debug(f"Returns a {type(result)}")
        self.__logger.debug("__getAverage_0: exiting")

        result = dask.array.where(result== 0, np.NaN, result)
        return result # result is a dask array

    def __getPercentile(self, a, percent):
        return np.nanpercentile(a, percent)

    def __convertOneSlice2PNG (self, PNGPath, sliceData, transformation_name, palette_name, video_mode_name):
        self.__logger.debug("__convertOneSlice2PNG: entering")
        self.__logger.debug(f"sliceData is a {type(sliceData)}")

        self.__logger.debug(str(sliceData.shape[0])+ "  " +str(sliceData.shape[1]))

        #palette = DataBlock.getPaletteFromName(palette_name)
        #N = len(palette)
        #p_low = None
        #p_high = None

        self.__logger.debug("transformation : %s"%transformation_name)
        if transformation_name == "percent99":
            self.__logger.debug("A '%s' transformation will be applied " % "percent99")
            p_high = self.__getPercentile(sliceData, 99.9)
            p_low = self.__getPercentile(sliceData, 0.1)
        elif transformation_name == "percent98":
            self.__logger.debug("A '%s' transformation will be applied " % "percent98")
            p_high = self.__getPercentile(sliceData, 98)
            p_low = self.__getPercentile(sliceData, 2)
        elif transformation_name == "percent95":
            self.__logger.debug("A '%s' transformation will be applied " % "percent95")
            p_high = self.__getPercentile(sliceData, 95)
            p_low = self.__getPercentile(sliceData, 5)
        elif transformation_name == "minmax":
            p_low, p_high = self.__min_max(sliceData)

        # we work on a copy of the data
        img_i = copy.deepcopy(sliceData)
        for irow in range(img_i.shape[1]//2):
            img_i[irow,:], img_i[img_i.shape[1] - 1 - irow,:] = img_i[img_i.shape[1] - 1 - irow,:], img_i[irow,:].copy()
        self.__writePNG(PNGPath, img_i, video_mode_name, palette_name, p_low, p_high)
        self.__logger.debug("__convertOneSlice2PNG: exiting")

    def __writePNG(self, PNGPath, img_i, video_mode_name, palette_name, vmin=None, vmax=None):
        """Writes a PNG file and its color bar at given path

            Parameters
            ----------
            PNGPath : string
                      path where png file will be written on disk
            img_i : array
                    data array

            video_mode_name : string
                              name of video mode ( direct or inverse)

            palette_name : string
                           name of color palette for color map

            vmin : float
                   minimum value for color bar

            vmax : float
                   maximum value for colorbar

        """
        plt.clf()
        plt.axis('off')

        palette = ""

        if video_mode_name == "direct":
            palette = palette_name
        elif video_mode_name == "inverse":
            palette = palette_name + "_r"
        else:
            raise Exception("Unknown video mode name {}".format(video_mode_name))

        # creates a mask where value are nan, typically for non square images
        masked_img_i = np.ma.masked_where(np.isnan(img_i),img_i)
        lut_factory = lutc.LutFactory()
        cmap = lut_factory.getLut(palette)
        cmap.set_bad(color='white')
        shape = img_i.shape

        if os.path.isfile(PNGPath):
            os.remove(PNGPath)

        legend_path = getLegendFilePath(PNGPath)

        if os.path.isfile(legend_path):
            os.remove(legend_path)

        #plt.imsave(fname=PNGPath, arr=masked_img_i, cmap=cmap, format='png', vmin=vmin, vmax=vmax, pil_kwargs={"compress_level":2, "optimize" :False})
        #mpb = plt.pcolormesh(img_i,cmap=cmap, vmin=vmin, vmax=vmax)

        # calculate image size
        dpi = 600
        #height, width = self.__image_width, self.__image_height = shape[0], shape[1]
        
        self.__logger.debug("### SHAPE")
        self.__logger.debug(shape)
        width, height = shape[0], shape[1] #= __image_width, __image_height
        figsize = width / float(dpi), height / float(dpi)
        fig = plt.figure(figsize=figsize)
        #hide the axes
        ax = fig.add_axes([0, 0, 1, 1])
        ax.axis('off')


        if vmin is not None and vmax is not None:
            plt.imshow(masked_img_i, cmap=cmap, aspect="equal", interpolation="nearest", vmin=vmin, vmax=vmax, extent=(0, 127, 0, 127))
        else :
            plt.imshow(masked_img_i, aspect=None, interpolation="equal", cmap=palette, extent=(0, 127, 0, 127))

        plt.savefig(PNGPath, pad_inches=0, dpi=dpi)

        # start working on colorbar
        mpb = plt.pcolormesh(img_i,cmap=cmap, vmin=vmin, vmax=vmax)     
           
        #plt.imsave(fname=PNGPath, arr=masked_img_i, cmap=cmap, format='png', vmin=vmin, vmax=vmax, pil_kwargs={"compress_level":9, "optimize" :False})
        #mpb = plt.pcolormesh(img_i,cmap=cmap, vmin=vmin, vmax=vmax)
        # draw a new figure and replot the colorbar there
        fig,ax = plt.subplots()
        plt.colorbar(mpb,ax=ax, orientation="horizontal")
        ax.remove()
        plt.savefig( legend_path, format="jpg", bbox_inches="tight", pad_inches=0)


    def __convertSummedSliceRange2PNG(self, iFREQ0, iFREQ1, sliceData, transformation_name, palette_name, video_mode_name):
        self.__logger.debug("__convertSummedSliceRange2PNG: entering")
        PNGPath = "%s/%d-%d.%s.%s.%s.png" % (self.__PNGDir, iFREQ0, iFREQ1, transformation_name, palette_name, video_mode_name)
        result = self.__convertOneSlice2PNG(PNGPath, sliceData, transformation_name, palette_name, video_mode_name)
        self.__logger.debug("__convertSummedSliceRange2PNG: exiting")
        return result

    def __min_max(self, dataArray):
#        data_max = np.nanmax(dataArray).astype(float).compute()
#        data_min = np.nanmin(dataArray).astype(float).compute()
        data_max = np.nanmax(dataArray).astype(float)
        data_min = np.nanmin(dataArray).astype(float)
#        return (data_min.item(), data_max.item())
        return data_min, data_max

    def __getForcedTransformationName(self,  transformation_name):
        self.__logger.debug("__getForcedTransformationName: entering")
        result = transformation_name
        self.__logger.debug("__getForcedTransformationName: exiting")
        return result
    

    def __squareSliceData (self, sliceData):
        self.__logger.debug("__squareSliceData: entering")
        #
        # sliceData : a dask array
        #
        self.__logger.debug(f"Considers a {type(sliceData)}")
        sliceShape = sliceData.shape
        if sliceShape[0] == sliceShape[1]:
            squaredSliceData = sliceData
        else:
            tmparr = [sliceData, self.__sliceMargin]
            if sliceShape[0] > sliceShape[1]:
                axis = 1
            else:
                axis = 0

            squaredSliceData = da.concatenate(tmparr, axis=axis)
            
        self.__logger.debug(f"Returns a {type(squaredSliceData)}")
        self.__logger.debug("__squareSliceData: exiting")
        return squaredSliceData
        


    def __addWCStoPNG(self, absPNGFilePath, headerInfos):
        self.__logger.debug("__addWCStoPNG: entering")
        im = Image.open(absPNGFilePath)
        info = PngImagePlugin.PngInfo()
        naxis2 = max(headerInfos["NAXIS1"], headerInfos["NAXIS2"])
        naxis1 = naxis2

        try :
            rotation =  "\n CROTA1  = "+str(headerInfos["CROTA1"])+\
                        "\n CROTA2  = "+str(headerInfos["CROTA2"])
        except KeyError as e:
            rotation =""

        try:
            equinox = headerInfos["EQUINOX"]
        except KeyError as e:
            equinox = 2000

        info.add_text("zTXtcomment",
            #"SIMPLE  = T "+
            #"\n BITPIX = "+str(headerInfos["BITPIX"])+
            #"\n NAXIS = 2 "+
            "\n NAXIS1  = "+str(naxis1)+            
            "\n NAXIS2  = "+str(naxis2)+
            "\n CTYPE1  = "+str(headerInfos["CTYPE1"])+
            "\n CRVAL1  = "+str(headerInfos["CRVAL1"])+
            "\n CDELT1 = "+str(headerInfos["CDELT1"])+
            "\n CRPIX1  = "+str(headerInfos["CRPIX1"])+
            "\n CTYPE2  = "+str(headerInfos["CTYPE2"])+
            "\n CRVAL2  = "+str(headerInfos["CRVAL2"])+
            "\n CDELT2 = "+str(headerInfos["CDELT2"])+
            "\n CRPIX2  = "+str(headerInfos["CRPIX2"])+
            "\n RADESYS= "+str(headerInfos["RADESYS"])+
            "\n EQUINOX = "+str(equinox)+
            rotation)

        im.save(absPNGFilePath, "PNG", pnginfo=info, compress_level=1)
        self.__logger.debug("__addWCStoPNG: exiting")

    def __getSpectrumAtiRAiDEC(self, iRA, iDEC):
        self.__logger.debug("__getSpectrumAtiRAiDEC: entering")
        result = self.__data[:, iDEC, iRA]
        self.__logger.debug("__getSpectrumAtiRAiDEC: exiting")
        return result

    def __dVal(self, d, kw, default):
        if kw in d:
            return d[kw]
        else:
            return default
        
    def addHeaderToFits(self, iRA,iDEC,iRA1,iDEC1,data_spectrum, spectrumUnit):
        """ Create new fits HDU from the one given by the origin fits file
            Parameters
            ----------
            iRA : str
                first x coordinate
            iDEC : str
                first y coordinate
            iRA1 : str
                second x coordinate
            iDEC1 : str
                second y coordinate
            data_spectrum : list
                a table (x,y) of data used to create the spectrum
            spectrumUnit : str
                selected unit of the average or simple spectrum

            Returns
            -------
            hdu : dictionary
                Primary HDU of the spectrum

        """
        xpix = int(iRA)
        ypix = int(iDEC)
        xpix2 = int(iRA1)
        ypix2 = int(iDEC1)

        w = self.__wcs2
        pixcrd = np.array([[xpix, ypix], [0,0]])
        world = w.wcs_pix2world(pixcrd,0)
        
        # Read the data at the correct Pixel
        data=data_spectrum
        spectrum = np.empty((len(data), 1, 1),dtype = np.float32)
        spectrum[:, 0, 0] = data 

        # Update the new header with required keywords for GILDAS
        #header=hdulist[0].header
        header=self.__header
        # Put this data into the new fits object
        hdu = fits.PrimaryHDU(spectrum)
        hdu.header['NAXIS'] = 3
        
        if "DATE" in header:
            hdu.header['DATE'] = header["DATE"]
        if("DATE-OBS" in header):
            hdu.header['DATE-OBS'] = header["DATE-OBS"]
        if("TELESCOP" in header):
            hdu.header['TELESCOP'] = header["TELESCOP"]
        if("OBSERVER" in header):
            hdu.header['OBSERVER'] = header["OBSERVER"]
        if (spectrumUnit.startswith("Jy/beam")):
            hdu.header['COMMENT'] = "Single pixel at xpix="+str(xpix)+" and ypix="+str(ypix)
        if (spectrumUnit=="Jy"):
            hdu.header['COMMENT'] = "Box at xbox1="+str(xpix)+" and xbox2="+str(xpix2)+" ybox1="+str(ypix)+" and ybox2="+str(ypix2)
        # SITELLE and MUSE case are not taken into account
        #if ("erg/s/cm" in spectrumUnit):
        #    hdu.header['COMMENT'] = "Spectrum unit "

        if hdu.header["BITPIX"] != -32:
            hdu.header['BLANK'] = 2147483647
        hdu.header['BSCALE'] = 1
        hdu.header['BZERO'] = 0
        if 'BMIN' in header:
            hdu.header['BMIN'] = header['BMIN']   
            
        if 'BMAJ' in header: 
            hdu.header['BMAJ'] = header['BMAJ']     
            hdu.header['BPA'] = header['BMAJ']
                    
        #if("DATAMIN" in header):
        #    hdu.header['DATAMIN'] = header["DATAMIN"]
        #    hdu.header['DATAMAX'] = header["DATAMAX"]
        #else:
            #if(not np.nanmin(spectrum)):
        hdu.header['DATAMIN'] = np.nanmin(spectrum)
            #if(not np.nanmax(spectrum)):
        hdu.header['DATAMAX'] = np.nanmax(spectrum)

        hdu.header['CTYPE3'] = header["CTYPE3"]
        #hdu.header['DELTAV']  =  -3e8*header["CDELT3"]/header["CRVAL3"]
        hdu.header['CTYPE1'] = header["CTYPE1"]
        hdu.header['CRVAL1'] = header["CRVAL1"]
        hdu.header['CRPIX1'] = int(header["CRPIX1"])
        hdu.header['CDELT1'] = header["CDELT1"]
        hdu.header['CRVAL3'] = header["CRVAL3"] #header["CRVAL3"]+header["NAXIS3"]/2.*header["CDELT3"]
        hdu.header['CRPIX3'] = header["CRPIX3"] #int(header["NAXIS3"]/2.)
        hdu.header['CDELT3'] = header["CDELT3"]
        hdu.header['CUNIT3'] = self.__dVal(header, 'CUNIT3', '')
        hdu.header['RESTFRQ'] = header["RESTFRQ"]
        hdu.header['RESTFREQ'] = header["RESTFRQ"]
        """
        if (header["CTYPE3"].startswith("FREQ")):
            hdu.header['CTYPE3'] = header["CTYPE3"]
            hdu.header['DELTAV']  =  -3e8*header["CDELT3"]/header["CRVAL3"]
            hdu.header['CTYPE1'] = header["CTYPE1"]
            hdu.header['CRVAL1'] = header["CRVAL1"]
            hdu.header['CRPIX1'] = int(header["CRPIX1"])
            hdu.header['CDELT1'] = header["CDELT1"]
            hdu.header['CRVAL3'] = header["CRVAL3"]+header["NAXIS3"]/2.*header["CDELT3"]
            hdu.header['CRPIX3'] = int(header["NAXIS3"]/2.)
            hdu.header['CDELT3'] = header["CDELT3"]
            hdu.header['CUNIT3'] = self.__dVal(header, 'CUNIT3', 'Hz')
            hdu.header['RESTFRQ'] = header["RESTFRQ"]
            hdu.header['RESTFREQ'] = header["RESTFRQ"]

        
        if (header["CTYPE3"]=="VRAD"):             
            hdu.header['CTYPE3'] = "FREQ"
            hdu.header['CRVAL3'] = header["RESTFRQ"]
            hdu.header['CDELT3'] = -header["CDELT3"]/299792458.*header["RESTFRQ"]
            hdu.header['CUNIT3'] = "Hz"
            hdu.header['CRPIX3'] = header["CRPIX3"] #int(header["NAXIS3"]/2.)
            hdu.header['DELTAV']  =  header["CDELT3"]
            hdu.header['RESTFRQ'] = 0
            hdu.header['RESTFREQ'] = 0
        
        """
        if("EQUINOX" in header):
            hdu.header['EQUINOX'] = header["EQUINOX"]
        else:
            hdu.header['EQUINOX'] = 2000
            
        hdu.header['SPECSYS'] = self.__dVal(header, 'SPECSYS', 'UNKNOWN')
        hdu.header['BUNIT'] = spectrumUnit

        hdu.header['CUNIT1'] = self.__dVal(header,'CUNIT1', 'deg')   
        hdu.header['CUNIT2'] = self.__dVal(header,'CUNIT2', 'deg')

        hdu.header['CTYPE1'] = header["CTYPE1"]
        hdu.header['CRVAL1'] = np.float64(world[0][0])
        hdu.header['CDELT1'] = 1.
        hdu.header['CRPIX1'] = 0
        
        hdu.header['CTYPE2'] = header["CTYPE2"]
        hdu.header['CRVAL2'] = np.float64(world[0][1])
        hdu.header['CDELT2'] = 1.
        hdu.header['CRPIX2'] = 0
        hdu.header['OBJECT'] = self.__dVal(header, 'OBJECT', 'Unknown')
        if("LINE" in header):
            hdu.header['LINE'] = header["LINE"]
        else:
            hdu.header['LINE']   = 'LINE Unknown'

        if "VELO-LSR" in header:
            hdu.header['VELO-LSR'] = header["VELO-LSR"]
        else:
            hdu.header['VELO-LSR']=  0.000000000000E+000
        hdu.header['IMAGFREQ'] =  0. 
        hdu.header['TSYS'] =  1
        hdu.header['OBSTIME'] =  1.
        hdu.header['SCAN-NUM']=  1
        hdu.header['TAU-ATM'] =  1
        hdu.header['NPHASE']  =  2
        hdu.header['DELTAF1'] =  0
        hdu.header['PTIME1']  =  1
        hdu.header['WEIGHT1'] =  0
        hdu.header['DELTAF2'] =  0
        hdu.header['PTIME2']  =  1
        hdu.header['WEIGHT2'] =  0 
        hdu.header['GAINIMAG'] =  1 
        hdu.header['BEAMEFF'] =  1 
        hdu.header['FORWEFF'] =  1 
        hdu.header['ORIGIN'] =  self.__dVal(header, "ORIGIN", "UNKNOWN")
        hdu.header['TIMESYS'] = 'UTC'
        hdu.header['DATE-RED'] =  self.__dVal(header, "DATE", "UNKNOWN")
        hdu.header['ELEVATIO']=  0. 
        hdu.header['AZIMUTH'] =  0 
        hdu.header['UT'] = '00:00:00.000'
        hdu.header['LST'] = '00:00:00.000'

        #nu=hdu.header['CRVAL3']
        #if 'BMIN' in header and 'BMAJ' in header:
        #    theta1=hdu.header['BMAJ']*math.pi/180/2./math.sqrt(math.log(2.0))
        #    theta2=hdu.header['BMIN']*math.pi/180/2./math.sqrt(math.log(2.0))
        #    lbda=2.99792458e8/nu
        #    jyperk=2.0*1.38e3*math.pi*theta1*theta2/lbda**2

        #    hdu.header['JYPERK'] = jyperk

        hdu.header['XPIX1'] = xpix
        hdu.header['YPIX1'] = ypix
        hdu.header['XPIX2'] = xpix2
        hdu.header['YPIX2'] = ypix2

        return hdu

    def __createFITSSpectrumFromData(self, iRA, iRA1,typeSpectrum, iDEC,iDEC1,spectrum,spectrumUnit):
        """ Create a temporary fits file corresponding to a spectrum
            
            Parameters
            ----------
            iRA : str
                first x coordinate of the pixel
            iRA1 : str
                second x coordinate of the pixel
            typeSpectrum : str
                summed spectrum or simple spectrum
            iDEC : str
                firs y coordinate of the pixel
            iDEC1 : str
                second y coordinate of the pixel
            spectrum : ???
            spectrumUnit : str
                selected unit of the average or simple spectrum

            Returns
            -------
            tempFile : str
                temporary fits file
        """
        self.__logger.debug("__createFITSSpectrumFromData: entering")

        if isNanList(spectrum):
            raise Exception("Spectrum is empty")

        try : 
            self.__logger.debug(type(spectrum))
            self.__logger.debug(spectrum)
            hdu =  self.addHeaderToFits(iRA,iDEC,iRA1,iDEC1,spectrum, spectrumUnit)
            hdu.update_header()
            tempFile = typeSpectrum + str(uuid.uuid1()) + ".fits"

            path = os.path.join(pm.SPECTRUMFilePrefix, tempFile)
            hdu.writeto(path,overwrite=True)

            self.__logger.debug("__createFITSSpectrumFromData: exiting")
            return tempFile   
        except fits.verify.VerifyError as e:
            raise e

    def __createFITS0(self, iRA, iDEC):
        """ Retrieve the data to be published via SAMP, frequencies or velocities
            
            Parameters
            ----------
            iRA : str
                Right Ascension
            iDEC : str
                Declinaison
            
            Returns
            -------
            result : str
                a temporary file 
        """
        self.__logger.debug("__createFITS0: entering")
        try:
            #
            # Retrieve the data to be published via SAMP
            # frequencies ( or velocities )
            spectrum = self.__getSpectrumAtiRAiDEC(iRA, iDEC)
            result = self.__createFITSSpectrumFromData(iRA,iRA,"spectrum",iDEC,iDEC,spectrum, self.__header["BUNIT"]) 
        except Exception as e:
            result = Result().wrong("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("Traceback: %s"  % traceback.format_exc())
        self.__logger.debug("__createFITS0: exiting")
        return result

    def createFITSSliceImage0(self, iFREQ) -> Result:
        result = {"status": False, "message": "", "result": None}
        try:            
            image_fits = self.createFITSImage(iFREQ,iFREQ,"imageTop",1)
            result = Result().ok(image_fits) 
        except Exception as e:
            result = Result().wrong("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("Traceback: %s"  % traceback.format_exc())
        self.__logger.debug("__createFITSSliceImage0: exiting")
        return result
    
    def createFITSSumSliceImage0(self, iFREQ0,iFREQ1) -> Result:
        self.__logger.debug("__createFITSSumSliceImage0: entering")
        result = {"status": False, "message": "", "result": None}
        try:
            header = self.__header
            image_fits = self.createFITSImage(iFREQ0,iFREQ1,"imageBottom",1)
            result = Result().ok(image_fits) 
        except Exception as e:
            result = Result().wrong("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("Traceback: %s"  % traceback.format_exc())
        self.__logger.debug("__createFITSSumSliceImage0: exiting")
        return result

    #write fits image
    def createFITSImage(self, iFREQ,iFREQ1,imageType, step=1):
        # Open the original FITS cube and get its header
        Header=self.__header
        hdu=None

        if(imageType == "imageTop"):
            hdu= self.createOneSlice(iFREQ,Header,step)
        else:
            hdu= self.createSummedSlice(iFREQ,iFREQ1,Header,step)
        hdu.update_header()


        name = self.__relFITSFilePath.split('/')[-1]
        relIMGFileDir = ('/').join(self.__relFITSFilePath[2:].split('/')[:-1])
        if("IMG/" in relIMGFileDir):
            relIMGFileDir = relIMGFileDir.replace("IMG",'')

        absIMGFileDir = pm.IMGFilePrefix+relIMGFileDir
    
        try:
            self.__IMGDir = absIMGFileDir
            if not os.path.exists(self.__IMGDir):
                os.makedirs(self.__IMGDir)
        except Exception as e:
                self.__logger.debug("Failed to open '%s'. Message was '%s'" % (self.__IMGDir, e))
                self.__logger.debug("Traceback: %s" %traceback.format_exc())
                return Result().wrong("Problem while creating the '%s' file: '%s'" % (imageType,e) )

        try:
            tempFile = imageType+'_'+str(iFREQ)+'_'+str(iFREQ1)+'_'+name
            path = self.__IMGDir + "/" + tempFile
            if not os.path.exists(path):
                hdu.writeto(path, overwrite=True)
        except Exception as e:
                self.__logger.debug("Failed to open '%s'. Message was '%s'" % (path, e))
                self.__logger.debug("Traceback: %s" %traceback.format_exc())
                return Result().wrong("Problem while creating the '%s' file: '%s'" % (imageType,e))
        
        relAbsFile = relIMGFileDir+"/"+tempFile
        return relAbsFile


    #create hdu for summed slice
    def createSummedSlice(self, iFREQ0, iFREQ1,Header, step):        
        data=self.__getAverage_0(iFREQ0, iFREQ1)

        image = np.empty((1,1,data.shape[0],data.shape[1]), dtype=np.float32 )
        if(Header["BITPIX"]==64):
            image = np.empty((1,1,data.shape[0],data.shape[1]), dtype=np.float64)
        else:
            image = np.empty((1,1,data.shape[0],data.shape[1]), dtype=np.float32 )
        image[0,0,:,:]=data

        hdu = fits.PrimaryHDU(image)
        hdu.header = copy.copy(Header)

        if("DATAMIN" in Header):
            hdu.header['DATAMIN'] = Header["DATAMIN"]
            hdu.header['DATAMAX'] = Header["DATAMAX"]
        else:
            #if(not np.nanmin(spectrum)):
            hdu.header['DATAMIN'] = np.nanmin(image)
            #if(not np.nanmax(spectrum)):
            hdu.header['DATAMAX'] = np.nanmax(image)
        
        bunit = hdu.header['BUNIT'].strip()
        if (Header["CTYPE3"]=="VRAD"): 
            v=Header["CRVAL3"]+(iFREQ0-1)*Header["CDELT3"]
            v1=Header["CRVAL3"]+(iFREQ1-1)*Header["CDELT3"]
            vHigh=max(v,v1)
            vMin=min(v,v1)
            Dv=vMin+(vHigh-vMin)/2.
            hdu.header['RESTFREQ'] = Header['RESTFRQ']-(Dv/const.c.value)*Header['RESTFRQ']
            hdu.header['DELTAV']=(vHigh-vMin)
            hdu.header['CDELT3']=(vHigh-vMin)
            hdu.header['CRVAL3']=Dv
            #hdu.header['CRVAL3']=0
            if bunit=="Jy/beam" or bunit.startswith("K"):
                hdu.header['BUNIT'] = f"{bunit}.km/s"
            # if iFREQ0 != iFREQ1:
            #     hdu.header["BUNIT"]="Jy/beam.km/s"
            # else: 
            #     hdu.header["BUNIT"]="Jy.km/s"
    
        if (Header["CTYPE3"].startswith("FREQ")):
            iFreqMin=min(iFREQ0,iFREQ1)
            iFreqMax=max(iFREQ0,iFREQ1)
            iMid=iFreqMin+int((iFreqMax-iFreqMin)/2.)
            Freq=Header["CRVAL3"]+(iFREQ0-1)*Header["CDELT3"]
            Freq1=Header["CRVAL3"]+(iFREQ1-1)*Header["CDELT3"]
            FreqMin=min(Freq,Freq1)
            FreqMax=max(Freq,Freq1)
            Df=FreqMax-FreqMin
            hdu.header['RESTFRQ'] = Header["CRVAL3"]+(iMid-1)*Header["CDELT3"]
            hdu.header['CDELT3']=Df
            hdu.header['CRVAL3']=hdu.header['RESTFRQ']
            if bunit == "Jy/beam" or bunit.startswith("K"):
                hdu.header['BUNIT'] = f"{bunit}.km/s"
            # if iFREQ0 != iFREQ1:
            #     hdu.header["BUNIT"]="Jy/beam.km/s"
            # else: 
            #     hdu.header["BUNIT"]="Jy/beam"

        if hdu.header["BITPIX"] != -32:
            hdu.header['BLANK'] = 2147483647
        hdu.header['BSCALE'] = 1
        hdu.header['BZERO'] = 0
        if("RADESYS" in Header):
            hdu.header['RADESYS'] = Header["RADESYS"]
        else:
            hdu.header['RADESYS'] = "ICRS"
        return hdu

    #create hdu for one slice    
    def createOneSlice(self,iFREQ,Header,step):
        # if one frequence
        data=self.__getSlice(iFREQ,step)
        image = np.empty((1,1,data.shape[0],data.shape[1]), dtype=np.float32 )
        if(Header["BITPIX"]==64):
            image = np.empty((1,1,data.shape[0],data.shape[1]), dtype=np.float64)
        else:
            image = np.empty((1,1,data.shape[0],data.shape[1]), dtype=np.float32 )
        image[0,0,:,:]=data

        hdu = fits.PrimaryHDU(image)
        
        # work on a deep copy of the header
        # if we work with its reference directly, it is modified below
        hdu.header = copy.copy(Header)
    
        if("DATAMIN" in Header):
            hdu.header['DATAMIN'] = Header["DATAMIN"]
            hdu.header['DATAMAX'] = Header["DATAMAX"]
        else:
            #if(not np.nanmin(image)):
            hdu.header['DATAMIN'] = np.nanmin(image)
            #if(not np.nanmax(image)):
            hdu.header['DATAMAX'] = np.nanmax(image)

        if (Header["CTYPE3"]=="VRAD"): 
            Dv=Header["CRVAL3"]+(iFREQ-1)*Header["CDELT3"]
            hdu.header['RESTFREQ'] = Header['RESTFREQ']-(Dv/const.c.value)*Header['RESTFREQ']
            hdu.header['CRVAL3']=Dv
    
        if (Header["CTYPE3"].startswith("FREQ")):
            hdu.header['RESTFRQ'] = Header["CRVAL3"]+(iFREQ-1)*Header["CDELT3"]
            hdu.header['CRVAL3'] = hdu.header['RESTFRQ']

        if hdu.header["BITPIX"] != -32:
            hdu.header['BLANK'] = 2147483647
        hdu.header['BSCALE'] = 1
        hdu.header['BZERO'] = 0
        if("RADESYS" in Header):
            hdu.header['RADESYS'] = Header["RADESYS"]
        else:
            hdu.header['RADESYS'] = "ICRS"
        return hdu
        
    #create the smooth    
    def slice_smbox_cube(self,incube,nbox):
        inchan=np.shape(incube)[0]
        outchan=int(inchan/nbox)
        outcube=np.zeros(shape=(outchan,np.shape(incube)[1],np.shape(incube)[2]))
        for i in range(outchan):
            outcube[i,:,:]=sum(incube[i*nbox:(i+1)*nbox,:,:])/(nbox)
        return outcube

    #write fits smooth
    def createSmoothCube(self,nbox):
        path=""
        # Open the original FITS cube and get its header
        hdu1=fits.open(pm.getSMOOTHFilePath(self.relFITSFilePath))
        Header=hdu1[0].header
        if(Header["NAXIS"] == 4):
            data=hdu1[0].data[0]
        else:
            data= hdu1[0].data
        hdu1.close()

        out=self.slice_smbox_cube(data,nbox)
        if(Header["NAXIS"] == 4):
            smooth_cube=np.zeros(shape=(1,np.shape(out)[0],np.shape(out)[1],np.shape(out)[2]))
            smooth_cube[0]=out
        else:
            smooth_cube=np.zeros(shape=(np.shape(out)[0],np.shape(out)[1],np.shape(out)[2]))
            smooth_cube=out
        
        hdu = fits.PrimaryHDU(smooth_cube)
        hdu.header = copy.copy(Header)
        hdu.header['CDELT3']=Header["CDELT3"]*nbox
        hdu.header['CRPIX3']=int(hdu.header['CRPIX3']/nbox)+1

        hdu.update_header()
        

        #create smoothdir
        name = self.__relFITSFilePath.split('/')[-1]
        relSMOOTHFileDir = ('/').join(self.__relFITSFilePath[2:].split('/')[:-1])
        if("SMOOTH/" in relSMOOTHFileDir):
            relSMOOTHFileDir = relSMOOTHFileDir.replace("SMOOTH/",'')
        absSMOOTHFileDir = pm.SMOOTHFilePrefix+relSMOOTHFileDir
       
        try:
            self.__SMOOTHDir = absSMOOTHFileDir
            self.__logger.debug(self.__SMOOTHDir)
            if not os.path.exists(self.__SMOOTHDir):
                os.makedirs(self.__SMOOTHDir)
        except Exception as e:
                self.__logger.debug("Failed to open '%s'. Message was '%s'" % (self.__SMOOTHDir, e))
                self.__logger.debug("Traceback: %s" %traceback.format_exc())
                return Result().wrong("Problem while creating the '%s' file: '%s'" % ("smoothcube",e) )

        try:
            #tempFile = 'smoothCube_'+str(nbox)+'_' + str(uuid.uuid1()) + ".fits"
            tempFile = 'smoothCube_'+str(nbox)+"_"+ name
            #path = self.SMOOTH_DIR + "/" + tempFile
            path = self.__SMOOTHDir + "/" + tempFile
            if not os.path.exists(path):
                hdu.writeto(path, overwrite=True)
        except Exception as e:
                self.__logger.debug("Failed to open '%s'. Message was '%s'" % (path, e))
                self.__logger.debug("Traceback: %s" %traceback.format_exc())
                return Result().wrong("Problem while creating the '%s' file: '%s'" % ("smoothcube",e) )
       
        relAbsFile = relSMOOTHFileDir+"/"+tempFile
        return relAbsFile
        
        #return createAbsFile(hdu,'smoothCube_'+str(nbox),"SMOOTH/")

    def createSmoothCube0(self,nbox) -> Result:
        self.__logger.debug("__createSmoothCube0: entering")
        result = {"status": False, "message": "", "result": None}
        try:
            smoothCube = self.createSmoothCube(nbox)
            result = Result().ok(smoothCube) 
        except Exception as e:
            result = Result().wrong("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("An exception occurred with message '%s'" % str(e))
            self.__logger.debug("Traceback: %s"  % traceback.format_exc())
        self.__logger.debug("__createSmoothCube0: exiting")
        return result

    #===========================================================================
    # Public getters and accessors.
    #
    def getlastAccess(self) -> Result:
        result = Result().ok(self.__lastAccess)
        return result

    def getHeader(self) -> Result:
        result = Result().ok(json.dumps(dict(self.__header)))
        return result

    def getDimensions(self) -> Result:
        result = Result().ok(self.__data.shape)
        return result

    def __collectStatistics(self, key, data): # data is expected to be a numpy array
        self.__logger.debug("__collectStatistics : entering")
        self.__logger.debug(f"input data are {type(data)}")
        if key in self.__statistics:
            self.__logger.debug("Statistics are already collected for key %s" % key)
        else:
            self.__logger.debug("Statistics have to be collected for key %s" % key)
            self.__statistics[key]={}
            normfactor = np.count_nonzero(~np.isnan(data)) #reduce(lambda a,b : a*b, self.__data.shape)
            if normfactor > 0 :
                results = []
                t = dd(np.nanmin)(data); results.append(t)
                t = dd(np.nanmax)(data); results.append(t)
                t = dd(np.nanmean)(data); results.append(t)
                t = dd(np.nanstd)(data); results.append(t)
                t = dd(np.histogram)(data[~np.isnan(data)],bins=100 ); results.append(t)

                results = dask.compute(*results)
                self.__statistics[key]["allNaN"] = False
                self.__statistics[key]["min"] = results[0].item()
                self.__statistics[key]["max"] = results[1].item()
                self.__statistics[key]["mean"] = results[2].item()
                self.__statistics[key]["stdev"] = results[3].item()
                self.__statistics[key]["histogram"] = [x.tolist() for x in results[4]]
                population = self.__statistics[key]["histogram"][0]
                bins = self.__statistics[key]["histogram"][1]
                aux = [0 for x in range(len(bins))]
                for i in range (1, len(aux)):
                    aux[i] = aux[i-1]+population[i-1]
                self.__statistics[key]["cumuldist"] = list(map(lambda x: x/normfactor, aux))
            else:
                self.__statistics[key]["allNaN"] = True
                self.__logger.info("The whole plane is filled with NaN values, no stats")
                
        self.__logger.debug("__collectStatistics : exiting")


    def __getSlice(self, iFREQ, step=1):
        self.__logger.debug("__getSlice: entering")

        result = None
        numDimensions = len(self.__data.shape)

        if numDimensions == 2:
            result = self.__data[0:self.__data.shape[0]:step, 0:self.__data.shape[1]:step]
        elif numDimensions == 3:
            if iFREQ == None:
                iFREQ = int(self.__data.shape[0] / 2)
            result = self.__data[iFREQ, 0:self.__data.shape[1]:step, 0:self.__data.shape[2]:step]

        #
        # Collect some statistics if not already done
        #
        self.__collectStatistics("%d"%iFREQ, result.compute())
        self.__logger.debug("__getSlice: exiting")
        return result # A DASK array

    def getSlice(self, iFREQ, step=1) -> Result:
        self.__logger.debug("getSlice: entering")
        result = Result()
        numDimensions = len(self.__data.shape)

        try :
            if numDimensions > 3 or numDimensions < 2:
                message = f"Can't process data with such a shape: {self.__data.shape}"
                result.wrong(message)
            else:
                x = self.__getSlice(iFREQ, step)
                self.__logger.debug(f"type self.__data = {type(self.__data)}, type x = {type(x)}")
                x = x.compute()
                b = np.isnan(x)
                x[b]=None
                tmp={}
                tmp["slice"]=x.tolist()
                tmp["result"]["statistics"]=self.__statistics["%d"%iFREQ]
                result.ok(tmp)
        except Exception as e:
            result.wrong(f"An exception occurred with message {e}")

        self.__logger.debug("getSlice: exiting")
        return result

    def getSpectrum(self, iRA=None, iDEC=None, iFREQ0=None, iFREQ1=None) -> Result:
        self.__timer.start()
        self.__logger.debug( "getSpectrum: entering")

        if iRA == None:
            iRA = 0
        if iDEC == None:
            iDEC = 0
        if iFREQ0 == None:
            iFREQ0 = 0
        if iFREQ1 == None:
            iFREQ1 = self.__data.shape[0]

        try : 
            #nonanArray = np.nan_to_num(self.__data[iFREQ0:iFREQ1, iDEC, iRA].compute())
            #result = nonanArray.tolist()
            nanArray = self.__data[iFREQ0:iFREQ1, iDEC, iRA].compute()
            result = nanArray.tolist()
            self.__logger.debug( "getSpectrum: exiting")
            return Result().ok(result)
        # nothing here
        except IndexError as e :
            return Result().wrong("IndexError")
        except Exception as e:
            return Result.wrong("An exception occurred :" + str(e))
        finally:
            self.__timer.stop("getSpectrum", self.__timerPrefix)

    def getPixelValueAtiRAiDEC(self, iRA: int, iDEC: int) -> Result:
        self.__logger.debug("getPixelValueAtiRAiDEC: entering")
        if len(self.__data.shape) != 2:
            self.__logger.debug("getPixelsValueAtiRAiDEC: exiting")
            return Result().wrong("Data have an inappropriate shape '{}' for a 2D request ".format(self.__data.shape))
        
        try:
            result = Result().ok(self.__getPixelValueAtiRAiDEC(iRA, iDEC))
        except Exception as e:
            result = Result().wrong("{0} - {1}".format(type(e), e.args))

        self.__logger.debug("getPixelValueAtiRAiDEC: exiting")
        return result
        

    def getPixelValueAtiFreqiRAiDEC(self, iFreq, iRA, iDEC) -> Result:
        self.__logger.debug("getPixelValueAtiFreqiRAiDEC: entering")
        if len(self.__data.shape) != 3:
            self.__logger.debug("getPixelsValueAtiFreqiRAiDEC: exiting")
            return Result().wrong("Data have an inappropriate shape '{}' for a 3D request ".format(self.__data.shape))
        
        try:
            result = Result().ok(self.__getPixelValueAtiFreqiRAiDEC(iFreq, iRA, iDEC))
        except IndexError as e:
            result = Result().wrong("IndexError, there is no value here".format(type(e), e.args))
        except Exception as e:
            result = Result().wrong("{0} - {1}".format(type(e), e.args))

        self.__logger.debug("getPixelValueAtiFreqiRAiDEC: exiting")
        return result

    def getPixelsValueAtiFreqiRAiDEC(self, iPositions) -> Result:
        self.__logger.debug("getPixelsValueAtiFreqiRAiDEC: entering")
        if len(self.__data.shape) != 3:
            self.__logger.debug("getPixelsValueAtiFreqiRAiDEC: exiting")
            return Result().wrong("Data have an inappropriate shape '{}' for a 3D request ".format(self.__data.shape))

        try:
            accum = []
            for (iFreq, iRA, iDEC) in iPositions:
                accum.append(
                    self.__getPixelValueAtiFreqiRAiDEC(iFreq, iRA, iDEC))
            result = Result().ok(accum)
        except Exception as e:
            result = Result().wrong("{0} - {1}".format(type(e), e.args))

        self.__logger.debug("getPixelsValueAtiFreqiRAiDEC: exiting")
        return result

    def getAverageSpectrum(self, iDEC0=None, iDEC1=None, iRA0=None, iRA1=None) -> Result:

        self.__logger.debug("### getAverageSpectrum")

        self.__timer.start()

        # check that coordinates are in the limits of the plan
        # force them inside if this is not the case
        if iRA0 == None or iRA0 < 0:
            iRA0 = 0
        if iRA0 > self.__data.shape[2]:
            iRA0 = self.__data.shape[2]

        if iRA1 == None or iRA1 > self.__data.shape[2]:
            iRA1 = self.__data.shape[2]
        if iRA1 < 0:
            iRA1 = 0

        if iDEC0 == None or iDEC0 < 0:
            iDEC0 = 0
        if iDEC0 > self.__data.shape[1]:
            iDEC0 = self.__data.shape[1]

        if iDEC1 == None or iDEC1 > self.__data.shape[1]:
            iDEC1 = self.__data.shape[1]
        if iDEC1 < 0:
            iDEC1 = 0


        # special case when there is only one pixel
        # selection will be [iRA0, iRA1[ [iDEC0, iDEC1[
        # excluding the case where everything = 0
        if iRA0 == iRA1 and iDEC0 == iDEC1 and iRA0!=0 and iDEC0!=0 :
            iRA1 = iRA0+1
            iDEC1 = iDEC0+1

        averageSpectrum = None
        averageSpectrumFits = None
        with_dask=True

        # NEW STYLE
        try :
            if with_dask:
                self.__logger.debug("WITH DASK")
                if self.__header["BUNIT"] == "Jy/beam":
                    averageSpectrum = (dask.array.nansum(dask.array.nan_to_num(self.__data[:, iDEC0:iDEC1, iRA0:iRA1]), (1,2)) / self.__convert * self.__cdelt).compute().tolist()
                    averageSpectrumFits = self.__createFITSSpectrumFromData(iRA0,iRA1,"averageSpectrum",iDEC0,iDEC1,averageSpectrum, "Jy")
                elif self.__header["BUNIT"].startswith("K"):
                    # afficher IDEC0, IDEC1, iRA0, iRA1
                    # spectre scalé à 1.5 au dessus
                    #NaN are replaced by 0, required to get spectrum but gives a wrong mean value
                    #averageSpectrum = (dask.array.nanmean(dask.array.nan_to_num(self.__data[:, iDEC0:iDEC1, iRA0:iRA1]), (1,2)) / self.__convert * self.__cdelt).compute().tolist()
                    averageSpectrum = (dask.array.nanmean(self.__data[:, iDEC0:iDEC1, iRA0:iRA1], (1,2)) / self.__convert * self.__cdelt).compute().tolist()
                    averageSpectrumFits = self.__createFITSSpectrumFromData(iRA0,iRA1,"averageSpectrum",iDEC0,iDEC1,averageSpectrum, self.__header["BUNIT"])
                    # NaN are replaced by 0 because json standards does not support NaN which causes
                    # a json read error in NodeJS
                    averageSpectrum = dask.array.nan_to_num(averageSpectrum).tolist()
                else:
                    averageSpectrum = (dask.array.nanmean(dask.array.nan_to_num(self.__data[:, iDEC0:iDEC1, iRA0:iRA1]), (1,2)) / self.__convert * self.__cdelt).compute().tolist()
                    #averageSpectrumFits = self.__createFITSSpectrumFromData(iRA0,iRA1,"averageSpectrum",iDEC0,iDEC1,averageSpectrum, self.__header["BUNIT"])      
                    averageSpectrumFits = None
            else:
                self.__logger.debug("NO DASK")
                averageSpectrum = np.nansum(np.nan_to_num(self.__data[:, iDEC0:iDEC1, iRA0:iRA1]), (1,2)) / self.__convert * self.__cdelt
                averageSpectrumFits = self.__createFITSSpectrumFromData(iRA0,iRA1,"averageSpectrum",iDEC0,iDEC1,averageSpectrum, self.__header["BUNIT"])

            result0 = {"averageSpectrum": averageSpectrum, "averageSpectrumFits": averageSpectrumFits}
            result = Result().ok(result0)
            self.__logger.debug("getAverageSpectrum: exiting")
            self.__timer.stop("getAverageSpectrum", self.__timerPrefix)
            return result
        except Exception as e : 
            result = Result().wrong("An exception occurred with message '%s'" % str(e))
            return result

    def getSumOverSliceRectArea(self, iFREQ, iRA0=None, iRA1=None, iDEC0=None, iDEC1=None) -> Result:
        self.__logger.debug("getSumOnSliceRectArea: entering")
        if (len(self.__data.shape) == 2 and iFREQ >= 1) or (len(self.__data.shape) == 3 and iFREQ >= self.__data.shape[0]):
            result = Result().wrong("Invalid slice index '%d'." % (iFREQ))
        else:
            result = Result().ok(self.__getSumOverSliceRectArea_0(iFREQ, iRA0, iRA1, iDEC0, iDEC1))

        self.__logger.debug("getSumOnSliceRectArea: exiting")
        return result

    def getAverage(self, iFREQ0=None, iFREQ1=None, iDEC0=None, iDEC1=None, iRA0=None, iRA1=None) -> Result:
        """
        Returns value at given position in summed slice. 
        If value is NaN or does not exist, it is replaced by None 
        to get a valid json document after conversion

        Parameters
        ----------
        iFREQ0 : int
                 Frequency of first slice
        iFREQ1 : int 
                 Frequency of last slice
        iDEC0 : int
                iDEC min
        iDEC1 : int
                iDEC max
        iRA0 : int
               iRA min
        iRA1 : int
               iRA max

        Return
        -------
        result : array

        """
        self.__logger.debug("getAverage: entering")
        result = self.__getAverage_0(iFREQ0, iFREQ1, iDEC0, iDEC1, iRA0, iRA1).compute().tolist()
        
        # None value appended to array it is empty
        if(np.size(result, 1)) == 0 :
           result[0].append(None)
        # NaN value replaced by None for json conversion
        elif np.isnan(result[0][0]):
            result[0][0] = None

        self.__logger.debug("returned value = %s"%result)
        self.__logger.debug("getAverage: exiting")
        return Result().ok(result)

    
    def getOneSliceAsPNG(self, iFREQ,  ittName=__default_transformation_name, lutName=__default_palette_name, vmName=__default_video_mode_name) -> Result:
        """
        Get One slice as a png image
        Parameters
        ----------
        iFREQ : float

        Return
        -------
        result : dictionary
        """
        self.__logger.debug("getOneSliceAsPNG: entering.")
        self.__timer.start()
        relPNGFileDir = ('.').join(self.__relFITSFilePath.split('.')[:-1])
        absPNGFileDir = pm.PNGFilePrefix+"/"+relPNGFileDir

        ittName = self.__getForcedTransformationName(ittName)
        sliceData = None
        data = self.__data
        shape = data.shape

        if (iFREQ >= shape[0]):
            self.__logger.debug("getOneSliceAsPNG: exiting.")
            return Result().wrong("No such slice index '%d' . Max. possible value is '%d'" % (iFREQ, shape[1]))

        if (len(shape) > 4):
            self.__logger.debug("getOneSliceAsPNG: exiting.")
            return Result().wrong("Can't process data with more than 4 dimensions")

        sliceData = self.__getSlice(iFREQ)
        relPNGFilePath = ("%s/%d.%s.%s.%s.png" %
                          (relPNGFileDir, iFREQ, ittName, lutName, vmName))
        absPNGFilePath = pm.PNGFilePrefix + "/" + relPNGFilePath

        self.__logger.debug(
            "File '%s' does not exists; it must be created..." % absPNGFilePath)

        try:
            self.__PNGDir = absPNGFileDir
            if not os.path.exists(self.__PNGDir):
                os.makedirs(self.__PNGDir)
        except Exception as e:
            self.__logger.debug(
                "Failed to open '%s'. Message was '%s'" % (self.__PNGDir, e))
            self.__logger.debug("Traceback: %s" %
                                traceback.format_exc())
            self.__logger.debug("getOneSliceAsPNG: exiting.")
            return Result().wrong("Problem while creating the PNG file: '%s'" % e)

        self.__logger.debug(
            "... in cache directory is %s" % (self.__PNGDir))
        try:
            # Realize the dask array as a numpy array.
            squaredSliceData = self.__squareSliceData(sliceData).compute()
            self.__convertOneSlice2PNG(absPNGFilePath, squaredSliceData, ittName, lutName, vmName)

            h = self.__header
            if "CDELT1" in h and "CDELT2" in h:
                x = {"BITPIX":   h["BITPIX"],
                     "NAXIS1":   squaredSliceData.shape[0],
                     "NAXIS2":   squaredSliceData.shape[0],
                     "CRPIX1":   h["CRPIX1"],
                     "CRPIX2":   h["CRPIX2"],
                     "CRVAL1":   h["CRVAL1"],
                     "CRVAL2":   h["CRVAL2"],
                     "CRTYPE1":  h["CTYPE1"],
                     "CRTYPE2":  h["CTYPE2"],
                     "RADESYS":  h["RADESYS"],
                     #"CD1_1":    h["CDELT1"],
                     #"CD2_2":    h["CDELT2"]
                    }
            self.__addWCStoPNG(absPNGFilePath, h)
            self.__logger.debug("File '%s' has been created" % absPNGFilePath)

        except Exception as e:
            self.__logger.debug(
                            "Failed to convert slice #%d. Error was '%r'" % (iFREQ, e))
            self.__logger.debug("Traceback: %s" %
                                traceback.format_exc())
            self.__logger.debug("getOneSliceAsPNG: exiting.")
            return Result().wrong("Problem while creating the PNG file: '%s'" % e)


        result = {"path_to_png": relPNGFilePath, "path_to_legend_png": getLegendFilePath(relPNGFilePath),
                  "statistics": self.__statistics["%d" % iFREQ]}
            
        self.__logger.debug("getOneSliceAsPNG: exiting.")
        self.__timer.stop("getOneSliceAsPng", self.__timerPrefix)
        return Result().ok(result)

    def getSummedSliceRangeAsPNG( self, iFREQ0, iFREQ1, ittName=__default_transformation_name, lutName=__default_palette_name, vmName=__default_video_mode_name) -> Result:
        self.__logger.debug("getSummedSliceRangeAsPNG: entering.")
        self.__timer.start()

        relPNGFileDir = ('.').join(self.__relFITSFilePath.split('.')[:-1])
        absPNGFileDir = pm.PNGFilePrefix+"/"+relPNGFileDir

        ittName = self.__getForcedTransformationName(ittName)

        relPNGFilePath = ("%s/%d-%d.%s.%s.%s.png" % (relPNGFileDir, iFREQ0, iFREQ1, ittName, lutName, vmName))
        absPNGFilePath = pm.PNGFilePrefix + "/" + relPNGFilePath
        summedSliceRangeData = self.__getAverage_0(iFREQ0, iFREQ1)

        # converts 0 to NaN as NaN have been replaced by 0 when summing data
        #summedSliceRangeData = dask.array.where(summedSliceRangeData== 0, np.NaN, summedSliceRangeData)

        self.__logger.debug("File '%s' does not exists; it must be created..." % absPNGFilePath)
        try:
            self.__PNGDir = absPNGFileDir
            if not os.path.exists(self.__PNGDir):
                os.makedirs(self.__PNGDir)
        except Exception as e:
                self.__logger.debug("Failed to open '%s'. Message was '%s'" % (self.__PNGDir, e))
                self.__logger.debug("Traceback: %s" %traceback.format_exc())
                self.__logger.debug("getSummedSliceRangeAsPNG: exiting.")
                return Result().wrong("Problem while creating the PNG file: '%s'" % e )


        self.__logger.debug("... in cache directory is %s" % (self.__PNGDir))
        try:
            self.__logger.debug(f"About to square a {type(summedSliceRangeData)}")
            squaredData = self.__squareSliceData(summedSliceRangeData).compute()
            self.__logger.debug(f"Squared data are now stored in a {type(squaredData)}")

            self.__convertSummedSliceRange2PNG(iFREQ0, iFREQ1, squaredData, ittName, lutName, vmName)
            h = self.__header
            if "CDELT1" in h and "CDELT2" in h:
                x = {"BITPIX":   h["BITPIX"],
                    "NAXIS1":   squaredData.shape[0],
                    "NAXIS2":   squaredData.shape[0],
                    "CRPIX1":   h["CRPIX1"],
                    "CRPIX2":   h["CRPIX2"],
                    "CRVAL1":   h["CRVAL1"],
                    "CRVAL2":   h["CRVAL2"],
                    "CRTYPE1":  h["CTYPE1"],
                    "CRTYPE2":  h["CTYPE2"],
                    "RADESYS":  h["RADESYS"]
                    #"CD1_1":    h["CDELT1"],
                    #"CD2_2":    h["CDELT2"]
                }
            self.__addWCStoPNG(absPNGFilePath, h)
        except Exception as e:
            self.__logger.debug("Problem while creating the PNG file: '%s'" % e )
            self.__logger.debug("Traceback: %s" %traceback.format_exc())
            self.__logger.debug("getSummedSliceRangeAsPNG: exiting.")
            return Result().wrong("Problem while creating the PNG file: '%s'" % e)

        result = {"path_to_png": relPNGFilePath, "path_to_legend_png": getLegendFilePath(relPNGFilePath),
        "statistics": self.__statistics["%d-%d"%(iFREQ0, iFREQ1)]}
        self.__logger.debug(f"result = {result}")
        self.__logger.debug("getSummedSliceRangeAsPNG: exiting.")
        self.__timer.stop("getSummedSliceRangeAsPng", self.__timerPrefix)
        return Result().ok(result)


    #===========================================================================
    #   Testers
    #
    def is3D(self):
        return (self.__header["NAXIS"]==3 and self.__header["NAXIS3"] > 1) or \
            (self.__header["NAXIS"]==4 and self.__header["NAXIS3"] > 1 and self.__header["NAXIS4"] == 1 )

    #===========================================================================
    #   Other public methods
    #
    def createFits(self,  iRA, iDEC) -> Result:
        self.__timer.start()
        self.__logger.debug("createFITS: entering")

        if not self.is3D():
            result = Result().wrong("This dataset does not have dimensions compatible with the requested operation")
        else:
            result = self.__createFITS0( iRA, iDEC)
        self.__logger.debug("createFITS: exiting")
        self.__timer.stop("createFits", self.__timerPrefix)
        return Result().ok(result)

    def RADECRangeInDegrees(self) -> Result:
        self.__logger.debug("RADECRangeInDegrees: entering")

        w = self.__wcs2
        naxis1_1 = self.__header["NAXIS1"]-1
        naxis2_1 = self.__header["NAXIS2"]-1
        naxis_1 = max(naxis1_1, naxis2_1)

        pixcrd = np.array([[0, 0], [naxis1_1, naxis2_1], [naxis_1, naxis_1]], np.float_)
        world = w.wcs_pix2world(pixcrd, 0)
        tworld = tuple(map( tuple, world))

        self.__logger.debug("RADECRangeInDegrees: exiting")
        return Result().ok((tworld[0], tworld[1], tworld[2]))

    def degToHMSDMS(self, RAinDD, DECinDD) -> Result:
        self.__logger.debug("degToHMSDMS: entering")
        w = wcs.WCS(self.__header)
        pixcrd = np.array([[RAinDD, DECinDD, 0, 0]], np.float_)
        world = w.all_pix2world(pixcrd, 0)
        result = SkyCoord(world[0][0], world[0][1], unit="deg").to_string('hmsdms')
        self.__logger.debug("degToHMSDMS: exiting")
        return Result().ok(result)

    def rangeToHMS(self, iRA0: int, iRA1: int, iRAstep) -> Result:
        self.__logger.debug("rangeToHMS: entering")
        result = []
        for i in range(iRA0, iRA1, iRAstep):
            result.append(self.degToHMSDMS(i, 0).result)
        self.__logger.debug("rangeToHMS: exiting")
        return Result().ok(result)

    def rangeToDMS(self, iDEC0, iDEC1, iDECstep) -> Result:
        self.__logger.debug("rangeToDMS: entering")
        result = []
        for i in range(iDEC0, iDEC1, iDECstep):
            result.append(self.degToHMSDMS(0, i).result)
        self.__logger.debug("rangeToDMS: exiting")
        return Result().ok(result)

    def getDataBlockInfos(self, now, infoNames) -> Result:
        self.__logger.debug("getDataBlockInfos: entering")
        result = []
        for info in infoNames:
            if info in self.__infod:
                if  info == 'idle' :
                    result.append(int((now-self.__lastAccess).total_seconds()))
                else:
                    result.append(self.__infod[info]())
            else:
                result.append("_undefined_")
        self.__logger.debug("getDataBlockInfos: exiting")
        return Result().ok(result)

    def getContours(self, iFREQ=None, iDEC0=None, iDEC1=None, iRA0=None, iRA1=None, **kwargs) -> Result:
        self.__logger.debug("getContours: entering")
        # Determine the limits
        numDims = len(self.__data.shape)
        iDEC0 = iDEC0 if iDEC0 else 0
        iDEC1 = iDEC1 if iDEC1 else (self.__data.shape[1] if numDims > 2 else self.__data.shape[0])
        iRA0 = iRA0 if iRA0 else 0
        iRA1 = iRA1 if iRA1 else (self.__data.shape[2] if numDims > 2 else self.__data.shape[1])

        # Get the data to be contoured
        slice = self.__getSlice(iFREQ).compute()
        shape = slice.shape
        self.__logger.debug("slice shape %r", shape)

        # Determine the contours levels.
        levels = None
        for key, value in kwargs.items():
            if key == 'levels':
                levels = value
            elif key == 'quantiles':
                levels = [np.quantile(slice[~np.isnan(slice)], quantile) for quantile in value]
            elif key == 'numberOfBins':
                levels = np.histogram_bin_edges(slice[~np.isnan(slice)], value)
            elif key == 'histBinsMethod':
                levels = np.histogram_bin_edges(slice[~np.isnan(slice)], value)
                levels = levels [-10:]
        self.__logger.debug("levels = %r" % (levels))

        # Produce the contours.
        features = []

        for level in levels:
            slice_bw = np.zeros(shape, dtype=np.uint8)
            slice_bw[slice >= level] = 127
            contours, _ = cv2.findContours(slice_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            for contour in contours :
                pts = [x[0] for x in contour.tolist()]
                #pts = [[x[0], shape[0] - x[1] -1] for x in pts]
                feature = {"type":"LineString", \
                "coordinates": pts,\
                "properties": {"level" : {"value": level, "unit" : self.__header["BUNIT"]}}}
                features.append(feature)

        contours_geojson = { "type": "FeatureCollection", "features" : features }
        self.__logger.debug("getContours: exiting")
        return Result().ok(contours_geojson)

    def measureContour(self, iFREQ, contour, level) -> Result:
        self.__logger.debug("measureContour : entering")
        slice = self.__getSlice(iFREQ).compute()
        shape = slice.shape
        bdr = cv2.boundingRect(contour)
        x,y,w,h = bdr
        accum = []
        for j in np.arange(y, y+h):
            for i in np.arange(x, x+w):
                if cv2.pointPolygonTest(contour, (int(i), int(j)), False) != -1 and slice[j][i] >= level:
                    accum.append(slice[j,i])
        accum = np.asarray(accum)
        result = {}
        bunit = self.__header["BUNIT"]
        nosum = False
        if bunit == "Jy/beam" :
            sumunit = "Jy"
            bunit = "Jy/beam"
            if "BMIN" not in self.__header or "BMAJ" not in self.__header:
                nosum = True
        else:
            sumunit = bunit

        if nosum is False :
            result["sum"] = {"value": np.sum(accum).item() / self.__convert * self.__cdelt , "unit" : sumunit}
        else : 
            # bunit == "Jy/beam" and  BMIN or BMAX is not defined
            result["sum"] = {"value": "N/A", "unit" : "" }
            
        result["min"] = {"value":  np.min(accum).item() , "unit": bunit}
        result["max"] = {"value":  np.max(accum).item() , "unit": bunit}
        result["mean"] = {"value":  np.mean(accum).item() , "unit": bunit}
        result["stdev"] = {"value":  np.std(accum).item() , "unit": bunit}
        result["numpix"] = {"value": accum.shape[0], "unit" : "pixels"}
        result["percentage of total number of pixels"] = {"value": accum.shape[0] / (shape[0]*shape[1]) * 100, "unit": "%"}
        result["boundingRect"] = {"value" :bdr, "unit" :"pixels"}

        self.__logger.debug("measureContour : exiting")
        return Result().ok(result)

    def measureBox(self, iFREQ, iRA0=None, iRA1=None, iDEC0=None, iDEC1=None) -> Result:
        self.__logger.debug("measureBox: entering")
        numDims = len(self.__data.shape)
        iDEC0 = iDEC0 if iDEC0 else 0
        iDEC1 = iDEC1 if iDEC1 else (self.__data.shape[1] if numDims > 2 else self.__data.shape[0])
        iRA0 = iRA0 if iRA0 else 0
        iRA1 = iRA1 if iRA1 else (self.__data.shape[2] if numDims > 2 else self.__data.shape[1])

        box = self.__getSlice(iFREQ).compute()[iDEC0:iDEC1, iRA0:iRA1]
        shape = self.__data.shape
        result = {}
        bunit = self.__header["BUNIT"]
        nosum = False
        if bunit == "Jy/beam" :
            sumunit = "Jy"
            bunit = "Jy/beam"
            if "BMIN" not in self.__header or "BMAJ" not in self.__header:
                nosum = True
        elif bunit == "Jy/beam.km/s":
            sumunit = "Jy.km/s"
        elif bunit.startswith("K") and "km/s" not in bunit.lower():
            sumunit = f"{bunit}.km/s"
        else:
            sumunit = f"{bunit}"

        if nosum is False :
            result["sum"] = {"value": np.nansum(box).item() / self.__convert * self.__cdelt , "unit" : sumunit}
        else :
            # bunit == "Jy/beam" and  BMIN or BMAX is not defined
            result["sum"] = {"value": "N/A", "unit" : "" }

        result["min"] = {"value":  np.nanmin(box).item() , "unit": bunit}
        result["max"] = {"value":  np.nanmax(box).item() , "unit": bunit}
        result["mean"] = {"value":  np.nanmean(box).item() , "unit": bunit}
        result["stdev"] = {"value":  np.nanstd(box).item() , "unit": bunit}
        result["numpix"] = {"value":  np.count_nonzero(~np.isnan(box)), "unit" : "pixels (!=Nan)"}
        result["percentage of total number of pixels"] = {"value": (box.shape[0]*box.shape[1]) / (self.__header["NAXIS1"]*self.__header["NAXIS2"]) * 100, "unit": "%"}
        result["boundingRect"] = {"value":[iRA0, iDEC0, iRA1-iRA0, iDEC1-iDEC0], "unit":"pixels"}

        self.__logger.debug("measureBox : exiting")
        return Result().ok(result)
    
#
# End of DataBlock class definition.
#
