from matplotlib.colors import ListedColormap
from matplotlib import pyplot as plt
import iramColors as iram

class LutFactory():
    def getLut(self, type):
        cmap = None
        if type == LUTIram.lutname:
            iramLut = LUTIram()
            cmap = ListedColormap(iramLut.colors)
        elif type == LUTIram.lutname+"_r":
            iramLut = LUTIram()
            cmap = ListedColormap(iramLut.reversecolors)
        else:
            cmap = plt.get_cmap(type)
        
        return cmap

class LUT:
    def __init__(self, name):
        self.__colors = []
        self.__reversecolors = []
        self.__name = name
        #self.__logger = logger

    @property
    def name(self):
        return self.__name

    @property
    def colors(self):
        return self.__colors

    @property
    def reversecolors(self):
        return self.__reversecolors

    def setColors(self, colors):
        self.__colors = colors
        self.__reversecolors = self.getReversedColors(colors)

    def getReversedColors(self, colors):
        def reverse(table):
            result = []
            for element in table : 
                result.append(1-element)
            return result
        result = map(reverse, colors)
        #self.__logger.debug("### result")
        #self.__logger.debug(result)
        return list(result)

    def getColorMap(self):
        return ListedColormap(self.__colors)

    def getReverseColorMap(self):
        return ListedColormap(self.__reversecolors)


class LUTIram(LUT):
    lutname = "iram"
    def __init__(self):
        super().__init__(LUTIram.lutname)
        self.setColors(iram.colors)