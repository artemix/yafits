import os.path

rootDir = "/home/partemix"
DataRoot = rootDir + "/dataroot"
logDir = rootDir + '/log/'
FITSFilePrefix = DataRoot + '/FITS/'
PNGFilePrefix = DataRoot + '/PNG/'
OBJFilePrefix = DataRoot + '/OBJ/'
SMOOTHFilePrefix = DataRoot + '/SMOOTH/'
IMGFilePrefix = DataRoot + '/IMG/'
SPECTRUMFilePrefix = DataRoot + '/SPECTRUM/'
ApiDocPrefix = DataRoot + '/apidoc/'


def getAbsFITSFilePath(relFITSFilePath):
    absFITSFilePath = None
    if "//SMOOTH" in relFITSFilePath:
        absFITSFilePath = SMOOTHFilePrefix + relFITSFilePath.replace("//SMOOTH", "")
    elif "//IMG" in relFITSFilePath:
        absFITSFilePath = IMGFilePrefix + relFITSFilePath.replace("//IMG", "")
    elif "//PNG" in relFITSFilePath:
        absFITSFilePath = PNGFilePrefix + relFITSFilePath.replace("//PNG", "")
    elif "//OBJ" in relFITSFilePath:
        absFITSFilePath = OBJFilePrefix + relFITSFilePath.replace("//OBJ", "")
    elif "//SPECTRUM" in relFITSFilePath:
        absFITSFilePath = SPECTRUMFilePrefix + relFITSFilePath.replace("//SPECTRUM", "")
    # fits is default
    else : 
        absFITSFilePath = FITSFilePrefix + relFITSFilePath
    
    return absFITSFilePath

def getSMOOTHFilePath(relFITSFilePath):
    absSMOOTHFilePath = None
    if os.path.isfile(FITSFilePrefix + relFITSFilePath):
        absSMOOTHFilePath = FITSFilePrefix + relFITSFilePath
    elif os.path.isfile(SMOOTHFilePrefix + relFITSFilePath.replace("SMOOTH", "")):
        absSMOOTHFilePath = SMOOTHFilePrefix + relFITSFilePath.replace("SMOOTH", "")
    else:
        raise Exception("File %s not found"%relFITSFilePath)

    return absSMOOTHFilePath