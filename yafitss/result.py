####################################################################
#                                                                  #
#                A class to store results                          #
#                                                                  #
# An instance of the class has three properties:                   #
#                                                                  #
#  status : boolean . False <=> a problem occurred                 #
#  message : str . The cause of the problem if False or            #
#            informative message if True                           #
#  result : Any . The payload if status==True and None otherwise   #
#                                                                  #
# Author : Michel Caillat                                          #
# Date : Apr 2020                                                  #                    
#                                                                  #
#                                                                  #
####################################################################
from typing import Any, Dict
import json

class Result:
    def __init__(self) -> None:
        self.__status = False
        self.__message = "Undefined"
        self.__result = None

    def __repr__(self) -> str:
        return self.toJSON()

    def __str__(self) -> str:
        return self.toJSON()

    @property
    def status(self) -> bool:
        return self.__status

    @property
    def message(self) -> str:
        return self.__message

    @property
    def result(self) -> Any:
        return self.__result

    @status.setter
    def status(self, status: bool):
        self.__status = status

    @message.setter
    def message(self, message: str):
        self.__message = message

    @result.setter
    def result(self, result: Any):
        self.__result = result

    def toDict(self) -> Dict:
        return {'status': self.__status, 'message': self.__message, 'result': self.__result}

    def toJSON(self) -> str:
        return json.dumps(self.toDict())

    # To store a valid result. Provide the payload.
    def ok(self, result: Any) -> "Result":
        self.__status = True
        self.__message = ""
        self.__result = result
        return self

    # To store a invalid result. Provide the message describing the problem
    def wrong(self, message: str) -> "Result":
        self.__status = False
        self.__message = message
        self.__result = None
        return self
