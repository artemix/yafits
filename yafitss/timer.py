import time


class TimerError(Exception):
    """A custom exception used to report errors in use of Timer class"""


class Timer:

    def __init__(self, logger):
        self.__start_time = None
        self.__logger = logger
        self.__separator = " - "

    def start(self):
        """Start a new timer"""
        if self.__start_time is not None:
            self.stop("Stop a running timer")
            #raise TimerError(f"Timer is running. Use .stop() to stop it")

        self.__start_time = time.perf_counter()


    def stop(self, message: str, prefix=""):
        """Stop the timer, and report the elapsed time
        
        Parameters
        ----------

            message : str
                a message that will be added to the execution time

            prefix : str
                a text that will be used as a prefix before message
        
        """
        if self.__start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")


        elapsed_time = time.perf_counter() - self.__start_time
        self.__start_time = None
        self.__logger.debug(f"{prefix} {self.__separator} {message} {self.__separator} {elapsed_time:0.4f} seconds")


    def addComment(self, message: str, prefix=""):
        """ Add a simple comment to the logged content
        
        Parameters
        ----------

            message : str
                text content that will be added to log content

            prefix : str
                a prefix that will be written before message
        
        """
        self.__logger.debug(f"{prefix} {self.__separator} {message}")
