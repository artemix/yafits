define({ "api": [
  {
    "type": "NOOP",
    "url": "noAPI",
    "title": "",
    "group": "_Introduction",
    "description": "<p>The yafitss http server.</p> <h2>Summary</h2> <p>The server accepts a number of GET and POST calls allowing to work remotely, as an HTTP client, on a collection of FITS files containing images.</p> <h2>Technical aspects of our server</h2> <p>The server is a bottle HTTP server. It accepts requests ( GET or POST ), extracts their parameters, converts them from the string type to the appropriate type, delegates the processing to a dataManager layer from which it receives the result to be sent back to the HTTP client in <strong>json</strong> form.</p> <h2>Conventions</h2> <h2>Input parameters</h2> <p>Each method GET or POST of an HTTP server expects zero, one or more parameters which are passed via a dictionary structure, i.e a sequence of <code>key:value</code> pairs. Of course this is true for <code>yafitss</code>, but in order to give the server the flavour of a domain specific language a convention for the names of the keys is adopted.<br> The list below enumerates those names, the types and the semantics of their associated values. Indeed types are not real types as in the usual sense since the values are always passed as strings; they are indications on the form those values should have:</p> <ul> <li><code>relFITSFilePath</code> {String} the path of the FITS file of interest relative to the root directory of the FITS files</li> <li><code>iRA</code> {uInt} an indice along the RA axis</li> <li><code>iDEC</code> {uInt} an indice along the DEC axis</li> <li><code>RAinDD</code> {Float} a right ascension expressed in decimal degrees</li> <li><code>DECinDD</code> {Float} a declination expressed in decimal degrees</li> <li><code>iRA0</code> {uInt} the lower bound of a range of indices along the RA axis</li> <li><code>iRA1</code> {uInt} the upper bound of a range of indices along the RA axis</li> <li><code>iRAstep</code> {uInt} a step in a range of indices along the RA axis</li> <li><code>iDEC0</code> {uInt} the lower bound of a range of indices along the DEC axis</li> <li><code>iDEC1</code> {uInt} the upper bound of a range of indices along the DEC axis</li> <li><code>iDECstep</code> {uInt} a step in a range of indices along the DEC axis</li> <li><code>iFREQ</code> {uInt} an indice along the FREQ axis</li> <li><code>iFREQ0</code> {uInt} the lower bound of a range of indices along the FREQ axis</li> <li><code>iFREQ1</code> {uInt} the upper bound of a range of indices along the FREQ axis</li> </ul> <h2>Returned values</h2> <p>API calls return systematically  a compound result with the following structure :</p> <pre class=\"prettyprint\"> {  \"status\": True of False,   \"message\": <some text mostly used when something went wrong>,   \"result\" : <the expected result or None>  } </code></pre> <p>Consequently given a returned value stored in a variable <code>result</code>, a good practice is to proceed as follows:</p> <ul> <li><code>if result[&quot;status&quot;] :</code> <ul> <li>retrieve the payload in <code>result[&quot;result&quot;]</code></li> </ul> </li> <li><code>else :</code> <ul> <li>retrieve an error message in <code>result[&quot;message&quot;]</code></li> </ul> </li> </ul>",
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "_Introduction",
    "name": "NoopNoapi"
  },
  {
    "type": "GET",
    "url": "artemix/degToHMSDMS",
    "title": "degToHMSDMS - Converts a (RA,DEC) pair from decimal degrees to HMS DMS",
    "name": "degToHMSDMS",
    "group": "conversions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "RAinDD",
            "description": "<p>RA in decimal degree</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "DECinDD",
            "description": "<p>DEC in decimale degree</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {'status': True, 'message': '', 'result': '12h30m50.1624s +12d23m08.2758s'}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "conversions"
  },
  {
    "type": "GET",
    "url": "artemix/rangeToDMS",
    "title": "Generates a sequence of DMS from a range of DEC indexes",
    "name": "rangeToDMS",
    "group": "conversions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDECO",
            "description": "<p>the lower index of the range along the DEC axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDEC1",
            "description": "<p>the upper index of the range along the DEC axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDECstep",
            "description": "<p>the step along the DEC axis.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': ['12h30m50.8158s +12d23m07.6437s', '12h30m50.8158s +12d23m17.8437s', '12h30m50.8158s +12d23m28.0437s', '12h30m50.8158s +12d23m38.2437s']}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "conversions"
  },
  {
    "type": "GET",
    "url": "artemix/rangeToHMS",
    "title": "rangeToHMS - Generates a sequence of HMS from a range of RA indexes",
    "name": "rangeToHMS",
    "group": "conversions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRAO",
            "description": "<p>the lower index of the range along the RA axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRA1",
            "description": "<p>the upper index of the range along the RA axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRAstep",
            "description": "<p>the step along the RA axis.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {'status': True, 'message': '', 'result': ['12h30m50.8158s +12d23m07.6437s', '12h30m50.1196s +12d23m07.6439s', '12h30m49.4234s +12d23m07.6439s', '12h30m48.7272s +12d23m07.6439s']}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "conversions"
  },
  {
    "type": "GET",
    "url": "artemix/RADECRangeInDegrees",
    "title": "RADECRangeInDegrees - Returns the RA,DEC range.",
    "name": "RADECRangeInDegrees",
    "group": "data_infos",
    "description": "<p>Returns the RA,DEC range of the data. The result is returned as a pair of pairs of float values <code>[[ra0, dec0], [ra1, dec1], [ra2, dec2]]</code> expressing angles in decimal degrees where :</p> <ul> <li><code>ra0</code> corresponds to the index 0 along the RA axis.</li> <li><code>ra1</code> corresponds to the index NAXIS1-1 along the RA axis.</li> <li><code>dec0</code> corresponds to the index 0 along the DEC axis.</li> <li><code>dec1</code> corresponds to the index NAXIS2-1 along the DEC axis.</li> <li><code>ra2</code> corresponds to the index max(NAXIS1, NAXIS2)-1 along the RA axis.</li> <li><code>dec2</code> corresponds to the index max(NAXIS1, NAXIS2)-1 along the DEC axis.</li> </ul>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': [[187.71173244085927, 12.385456581760074], [187.700143312178, 12.396775748749931], [187.700143312178, 12.396775748749931]]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "data_infos"
  },
  {
    "type": "GET",
    "url": "artemix/getDimensions",
    "title": "Returns the shape of the data.",
    "name": "getDimensions",
    "group": "data_infos",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': [24, 800, 800]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "data_infos"
  },
  {
    "type": "GET",
    "url": "artemix/getHeader",
    "title": "getHeader - Returns the header.",
    "name": "getHeader",
    "group": "data_infos",
    "description": "<p>Returns the header of the FITS file of interest as a dictionary.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': '{\"SIMPLE\": true, \"BITPIX\": -32, \"NAXIS\": 4, \"NAXIS1\": 800, \"NAXIS2\": 800, \"NAXIS3\": 24, \"NAXIS4\": 1, \"EXTEND\": true, \"BSCALE\": 1.0, \"BZERO\": 0.0, \"BMAJ\": 7.385119795799e-05, \"BMIN\": 5.762678467565e-05, \"BPA\": 15.67044067383, \"BTYPE\": \"Intensity\", \"OBJECT\": \"M87\", \"BUNIT\": \"Jy/beam\", \"EQUINOX\": 2000.0, \"RADESYS\": \"FK5\", \"LONPOLE\": 180.0, \"LATPOLE\": 12.39112331, \"PC01_01\": 1.0, \"PC02_01\": 0.0, \"PC03_01\": 0.0, \"PC04_01\": 0.0, \"PC01_02\": 0.0, \"PC02_02\": 1.0, \"PC03_02\": 0.0, \"PC04_02\": 0.0, \"PC01_03\": 0.0, \"PC02_03\": 0.0, \"PC03_03\": 1.0, \"PC04_03\": 0.0, \"PC01_04\": 0.0, \"PC02_04\": 0.0, \"PC03_04\": 0.0, \"PC04_04\": 1.0, \"CTYPE1\": \"RA---SIN\", \"CRVAL1\": 187.70593075, \"CDELT1\": -1.416666666667e-05, \"CRPIX1\": 401.0, \"CUNIT1\": \"deg\", \"CTYPE2\": \"DEC--SIN\", \"CRVAL2\": 12.39112331, \"CDELT2\": 1.416666666667e-05, \"CRPIX2\": 401.0, \"CUNIT2\": \"deg\", \"CTYPE3\": \"FREQ\", \"CRVAL3\": 230436102960.0, \"CDELT3\": -76899199.37878, \"CRPIX3\": 1.0, \"CUNIT3\": \"Hz\", \"CTYPE4\": \"STOKES\", \"CRVAL4\": 1.0, \"CDELT4\": 1.0, \"CRPIX4\": 1.0, \"CUNIT4\": \"\", \"PV2_1\": 0.0, \"PV2_2\": 0.0, \"RESTFRQ\": 230538000000.0, \"SPECSYS\": \"BARYCENT\", \"ALTRVAL\": 132507.283331, \"ALTRPIX\": 1.0, \"VELREF\": 258, \"TELESCOP\": \"ALMA\", \"OBSERVER\": \"jtan\", \"DATE-OBS\": \"2015-08-16T19:00:41.616000\", \"TIMESYS\": \"UTC\", \"OBSRA\": 187.70593075, \"OBSDEC\": 12.39112331, \"OBSGEO-X\": 2225142.180269, \"OBSGEO-Y\": -5440307.370349, \"OBSGEO-Z\": -2481029.851874, \"DATE\": \"2015-12-08T18:24:15.408000\", \"ORIGIN\": \"CASA 4.5.0-REL (r35147)\", \"INSTRUME\": \"Unknown\"}'}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "data_infos"
  },
  {
    "type": "GET",
    "url": "artemix/getSlice",
    "title": "Returns a (RA, DEC) plane for a given frequency.",
    "name": "getSlice",
    "group": "data_selection",
    "description": "<p>The result is returned in a list of list ( 2D data ) with RA varying more rapidly than DEC. The result may contain nan values.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQ",
            "description": "<p>index along the FREQ axis of the (RA, DEC) plane to return</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': [[...]...[...]]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "data_selection"
  },
  {
    "type": "GET",
    "url": "artemix/setData",
    "title": "setData - Loads a FITS file in server's memory and return its header.",
    "name": "setData",
    "group": "data_selection",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest. If it's already loaded, just returns the header.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': '{\"SIMPLE\": true, \"BITPIX\": -32, \"NAXIS\": 4, \"NAXIS1\": 800, \"NAXIS2\": 800, \"NAXIS3\": 24, \"NAXIS4\": 1, \"EXTEND\": true, \"BSCALE\": 1.0, \"BZERO\": 0.0, \"BMAJ\": 7.385119795799e-05, \"BMIN\": 5.762678467565e-05, \"BPA\": 15.67044067383, \"BTYPE\": \"Intensity\", \"OBJECT\": \"M87\", \"BUNIT\": \"Jy/beam\", \"EQUINOX\": 2000.0, \"RADESYS\": \"FK5\", \"LONPOLE\": 180.0, \"LATPOLE\": 12.39112331, \"PC01_01\": 1.0, \"PC02_01\": 0.0, \"PC03_01\": 0.0, \"PC04_01\": 0.0, \"PC01_02\": 0.0, \"PC02_02\": 1.0, \"PC03_02\": 0.0, \"PC04_02\": 0.0, \"PC01_03\": 0.0, \"PC02_03\": 0.0, \"PC03_03\": 1.0, \"PC04_03\": 0.0, \"PC01_04\": 0.0, \"PC02_04\": 0.0, \"PC03_04\": 0.0, \"PC04_04\": 1.0, \"CTYPE1\": \"RA---SIN\", \"CRVAL1\": 187.70593075, \"CDELT1\": -1.416666666667e-05, \"CRPIX1\": 401.0, \"CUNIT1\": \"deg\", \"CTYPE2\": \"DEC--SIN\", \"CRVAL2\": 12.39112331, \"CDELT2\": 1.416666666667e-05, \"CRPIX2\": 401.0, \"CUNIT2\": \"deg\", \"CTYPE3\": \"FREQ\", \"CRVAL3\": 230436102960.0, \"CDELT3\": -76899199.37878, \"CRPIX3\": 1.0, \"CUNIT3\": \"Hz\", \"CTYPE4\": \"STOKES\", \"CRVAL4\": 1.0, \"CDELT4\": 1.0, \"CRPIX4\": 1.0, \"CUNIT4\": \"\", \"PV2_1\": 0.0, \"PV2_2\": 0.0, \"RESTFRQ\": 230538000000.0, \"SPECSYS\": \"BARYCENT\", \"ALTRVAL\": 132507.283331, \"ALTRPIX\": 1.0, \"VELREF\": 258, \"TELESCOP\": \"ALMA\", \"OBSERVER\": \"jtan\", \"DATE-OBS\": \"2015-08-16T19:00:41.616000\", \"TIMESYS\": \"UTC\", \"OBSRA\": 187.70593075, \"OBSDEC\": 12.39112331, \"OBSGEO-X\": 2225142.180269, \"OBSGEO-Y\": -5440307.370349, \"OBSGEO-Z\": -2481029.851874, \"DATE\": \"2015-12-08T18:24:15.408000\", \"ORIGIN\": \"CASA 4.5.0-REL (r35147)\", \"INSTRUME\": \"Unknown\"}'}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "data_selection"
  },
  {
    "type": "POST",
    "url": "artemix/getOneSliceAsPNG",
    "title": "Generates a PNG representation of a RA,DEC plane.",
    "name": "getOneSliceAsPNG",
    "group": "imagery",
    "description": "<ul> <li> <p>Generates a PNG image of a <code>RA,DEC</code> plane defined by an index, <code>iFREQ</code>, along the FREQ axis.</p> <ul> <li> <p>The image is built from the pixels values used as entries in the composition of an &quot;intensity transfert table&quot; (<code>ittName</code>), a &quot;look up table&quot; (<code>lutName</code>) and a &quot;video mode&quot; (<code>vmName</code>).</p> </li> <li> <p>The image is written to the server's filesystem in a space which can be browsed.</p> </li> <li> <p>The PNG file is named after <code>relFITSFilePath, iFREQ, ittName, lutName, vmName</code>.</p> </li> <li> <p>The PNG images use 256 colours palettes.</p> </li> <li> <p>Valid names values for <code>ittName, lutName, vmName</code> can be retrieved with the API <code>renderingCapabilities</code>.</p> </li> </ul> </li> <li> <p>Returns a dictionary with two entries : <code>{'data_steps': .... , 'path_to_png' : ...}</code></p> <ul> <li><code>'data_steps'</code> : an association (dictionary) between the 256 RGB triples used in the PNG image and the 256 levels of the sampling of the physical values present in the <code>RA,DEC</code> plane. Example of an element of that dictionary : <code>&quot;255_121_0&quot;: -0.037923078107483243</code></li> <li><code>'path_to_png'</code> : the path to the PNG file relative to some root directory defined in the server's configuration. The important point is that it can be used as an URL, e.g. for visualization purpose a on a browser.</li> </ul> </li> </ul>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n'{\"status\": true, \"message\": \"\", \"result\": {\"data_steps\": {\"255_0_41\": -0.051212918013334274, ... ,\"255_0_191\": 0.06175072118639946}, \"path_to_png\": \"2013/2013.1.00073.S/science_goal.uid___A001_X12f_X20b/group.uid___A001_X12f_X20c/member.uid___A001_X12f_X20d/product/M87_CO_v_0_2_1_image.pbcor/12.minmax.gist_rainbow.direct.png\"}}'",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>The path to the FITS file of interest</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQ",
            "description": "<p>An index along the <code>FREQ</code> axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "ittName",
            "description": "<p>The intensity transformation table name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lutName",
            "description": "<p>The look up table name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "vmName",
            "description": "<p>The video mode name.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "imagery"
  },
  {
    "type": "POST",
    "url": "artemix/getSummedSliceRangeAsPNG",
    "title": "Generates a PNG representation of a sum of RA,DEC planes.",
    "name": "getSummedSliceRangeAsPNG",
    "group": "imagery",
    "description": "<ul> <li> <p>Generates a PNG image of the sum of <code>RA,DEC</code> planes with indexes contained in a range <code>[iFREQ0, iFREQ1]</code> along the <code>FREQ</code> axis.</p> <ul> <li> <p>The image is built from the pixels values used as entries in the composition of an &quot;intensity transfert table&quot; (<code>ittName</code>), a &quot;look up table&quot; (<code>lutName</code>) and a &quot;video mode&quot; (<code>vmName</code>).</p> </li> <li> <p>The image is written to the server's filesystem in a space which can be browsed.</p> </li> <li> <p>The PNG file is named after <code>relFITSFilePath, iFREQ0, iFREQ1, ittName, lutName, vmName</code>.</p> </li> <li> <p>The PNG images use 256 colours palettes.</p> </li> <li> <p>Valid names values for <code>ittName, lutName, vmName</code> can be retrieved with the API <code>renderingCapabilities</code>.</p> </li> </ul> </li> <li> <p>Returns a dictionary with two entries : <code>{'data_steps': .... , 'path_to_png' : ...}</code></p> <ul> <li><code>'data_steps'</code> : an association (dictionary) between the 256 RGB triples used in the PNG image and the 256 levels of the sampling of the physical values present in the <code>RA,DEC</code> plane. Example of an element of that dictionary : <code>&quot;255_121_0&quot;: -0.037923078107483243</code></li> <li><code>'path_to_png'</code> : the path to the PNG file relative to some root directory defined in the server's configuration. The important point is that it can be used as an URL, e.g. for visualization purpose a on a browser.</li> </ul> </li> </ul>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>The path to the FITS file of interest</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQ",
            "description": "<p>An index along the <code>FREQ</code> axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "ittName",
            "description": "<p>The intensity transformation table name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lutName",
            "description": "<p>The look up table name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "vmName",
            "description": "<p>The video mode name.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n'{\"status\": true, \"message\": \"\", \"result\": {\"data_steps\": {\"255_0_41\": -56.15247344970703, ..., \"255_0_191\": 77.84288787841797}, \"path_to_png\": \"2013/2013.1.00073.S/science_goal.uid___A001_X12f_X20b/group.uid___A001_X12f_X20c/member.uid___A001_X12f_X20d/product/M87_CO_v_0_2_1_image.pbcor/6-18.minmax.gist_rainbow.direct.png\"}}'",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "imagery"
  },
  {
    "type": "GET",
    "url": "artemix/renderingCapabilities",
    "title": "Useful informations for the production of PNG files.",
    "name": "renderingCapabilities",
    "group": "imagery",
    "description": "<p>Returns informations useful for calls to APIs dedicated to the generation of PNG files.</p> <p>The returned information is structured as follows :</p> <p>{</p> <p>'itts': <em>list of itt names</em>, 'default_itt_index': <em>index to default itt name</em>,</p> <p>'luts': <em>list of lut names</em>, 'default_lut_index': <em>index to default lut name</em>,</p> <p>'vmodes': <em>list of video mode names</em>, 'default_vmode_index': <em>index to default video mode name</em></p> <p>}</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': {'itts': ['minmax', 'percent98'], 'default_itt_index': 0, 'luts': ['Greys', 'RdYlBu', 'hsv', 'gist_ncar', 'gist_rainbow', 'gist_gray', 'Spectral', 'jet', 'plasma', 'inferno', 'magma', 'afmhot', 'gist_heat'], 'default_lut_index': 2, 'vmodes': ['direct', 'inverse'], 'default_vmode_index': 0}}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "imagery"
  },
  {
    "type": "POST",
    "url": "/artemix/createFITS",
    "title": "Creates a FITS table containing a spectrum.",
    "name": "createFITS",
    "group": "misc",
    "description": "<p>Creates a FITS ASCII table whose content is a spectrum defined by a position in the <code>RA,DEC</code> plane. Returns the FITS table as a sequence of bytes.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>The path to the FITS file of interest</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRA",
            "description": "<p>index along the RA axis</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDEC",
            "description": "<p>index along the DEC axis</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': \"SIMPLE  =                    T / conforms to FITS standard                      BITPIX  =                    8 / array data type                                NAXIS   =                    0 / number of array dimensions                     EXTEND  =                    T                                                  DATE    = '2015-12-08T18:24:15.408000'                                          DATE-OBS= '2015-08-16T19:00:41.616000'                                          TELESCOP= 'ALMA    '                                                            OBSERVER= 'jtan    '                                                            RESTFRQ =       230538000000.0                                                  SPECSYS = 'BARYCENT'                                                            COMMENT Spectrum of a single pixel of the cube                                  END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             XTENSION= 'TABLE   '           / ASCII table extension                          BITPIX  =                    8 / array data type                                NAXIS   =                    2 / number of array dimensions                     NAXIS1  =                   30 / length of dimension 1                          NAXIS2  =                   24 / length of dimension 2                          PCOUNT  =                    0 / number of group parameters                     GCOUNT  =                    1 / number of groups                               TFIELDS =                    2 / number of table fields                         TTYPE1  = 'Frequency'                                                           TFORM1  = 'E15.7   '                                                            TUNIT1  = 'Hz      '                                                            TBCOL1  =                    1                                                  TTYPE2  = 'Flux    '                                                            TFORM2  = 'E15.7   '                                                            TUNIT2  = 'Jy/beam '                                                            TBCOL2  =                   16                                                  END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               2.3043611E+11  3.2496294E-03  2.3035920E+11  3.1511623E-03  2.3028230E+11  2.3847336E-03  2.3020541E+11  2.2302533E-03  2.3012850E+11  2.0003885E-03  2.3005161E+11  1.2175933E-03  2.2997470E+11  1.1119752E-03  2.2989781E+11  3.6920310E-04  2.2982091E+11 -3.7513475E-04  2.2974402E+11 -3.3260061E-04  2.2966711E+11 -1.1299318E-03  2.2959020E+11 -1.6285286E-03  2.2951331E+11 -2.0787886E-03  2.2943641E+11 -1.5112817E-03  2.2935952E+11 -2.7610569E-03  2.2928261E+11 -2.2096562E-03  2.2920572E+11 -2.5501542E-03  2.2912881E+11 -2.2054571E-03  2.2905192E+11 -2.6812006E-03  2.2897502E+11 -2.6687880E-03  2.2889811E+11 -3.7882254E-03  2.2882122E+11 -3.9494671E-03  2.2874431E+11 -3.7729631E-03  2.2866742E+11 -4.9771518E-03                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                \"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "misc"
  },
  {
    "type": "GET",
    "url": "artemix/ping",
    "title": "ping - Check server readiness",
    "name": "Check_server_readiness",
    "group": "server",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{status: True, message : 'Up and waiting', 'result': }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "server"
  },
  {
    "type": "POST",
    "url": "artemix/getDataBlockInfos",
    "title": "getDataBlockInfos -",
    "name": "getDataBlockInfos",
    "group": "server",
    "description": "<p>Returns informations about the server's characteristics and the DataBlock objects present in memory. A DataBlock is an object created when a FITS file is read in memory; it encapsulates the FITS file content plus a number of other fields.</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "server"
  },
  {
    "type": "GET",
    "url": "getAverageSpectrum",
    "title": "Returns a spectrum whose values are obtained by sums on (RA, DEC) planes.",
    "name": "getAverageSpectrum",
    "group": "values_and_projections",
    "description": "<p>Given two pairs -- <code>iRA0,iRA1</code> and <code>iDEC0,iDEC1</code> -- defining a rectangular area in the RAxDEC plane and a frequency range defined by a pair of integers <code>iFREQ0</code>,<code>iFREQ1</code>, a call to this API returns the result of <code>numpy.nansum(datacube[iFREQ0:iFRE1, iDEC0:iDEC1, iRA0:iRA1], (1,2))</code> multiplied by the appropriate value to take into account the telescope's beam when its relevant (radio astronomical data)</p> <p>The result is returned as a dictionary with two keys whose values are defined below :</p> <ul> <li><code>&quot;averageSpectrum&quot;</code> : the average spectrum as a list of float values</li> <li><code>&quot;averageSpectrumFits&quot;</code> : the average spectrum as a FITS table. Note this part is returned <strong>if and only if</strong> the parameter <code>retFITS</code> has been passed with a True value. This optional product is mainly intended for distribution via SAMP.</li> </ul>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRAO",
            "description": "<p>the lower index of the range along the RA axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRA1",
            "description": "<p>the upper index of the range along the RA axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDECO",
            "description": "<p>the lower index of the range along the DEC axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDEC1",
            "description": "<p>the upper index of the range along the DEC axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQO",
            "description": "<p>the lower index of the range along the FREQ axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQ1",
            "description": "<p>the upper index of the range along the FREQ axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "retFITS",
            "description": "<p>True means returns the spectrum also as a FITS table.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': {'averageSpectrum': [-0.20683714747428894, 0.06167888641357422, -0.01623634621500969, 0.024083947762846947, -0.02355683408677578, -0.05549465864896774, -0.08872086554765701, -0.08671190589666367, -0.06696601957082748, 0.05688297748565674, -0.13291282951831818, -0.21775048971176147, -0.24340537190437317, -0.10746107250452042, -0.20933619141578674, -0.19364875555038452, -0.2382306307554245, -0.28746917843818665, -0.2542598247528076, -0.2730403542518616, -0.28176575899124146, -0.17414641380310059, -0.30788716673851013, -0.39479535818099976], 'averageSpectrumFits': None}}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "values_and_projections"
  },
  {
    "type": "GET",
    "url": "artemix/getPixelValueAtiFreqiRAiDEC",
    "title": "Returns the value of a pixel located by a triple (iRA, iDEC, iFREQ)",
    "name": "getPixelValueAtiFreqiRAiDEC",
    "group": "values_and_projections",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRA",
            "description": "<p>index along the RA axis</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDEC",
            "description": "<p>index along the DEC axis</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQ",
            "description": "<p>index along the FREQ axis</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': -0.00207878858782351}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "values_and_projections"
  },
  {
    "type": "POST",
    "url": "artemix/getPixelsValueAtiFreqiRAiDEC",
    "title": "Returns the values of a collection of pixels whose positions are defined in an array of triples (iRA, iDEC, iFREQ)",
    "name": "getPixelsValueAtiFreqiRAiDEC",
    "group": "values_and_projections",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "iPositions",
            "description": "<p>the array of positions. Each position must be an array of three integers values for iFreq, iRA, iDEC in that order.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': [-0.00207878858782351]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "values_and_projections"
  },
  {
    "type": "GET",
    "url": "artemix/getSumOverSliceRectArea",
    "title": "Weighted sum over a rectangular area in a RA,DEC plane",
    "name": "getSumOnSliceRectArea",
    "group": "values_and_projections",
    "description": "<p>Given two pairs of indexes -- <code>iRA0,iRA1</code> and <code>iDEC0,iDEC1</code> -- defining a rectangular area in the RA,DEC plane and a frequency defined by an integer iFREQ returns the result of <code>numpy.nansum(datacube[iFREQ, iDEC0:iDEC1, iRA0:iRA1], (1,2))</code> multiplied by the appropriate value to take into account the telescope's beam when it's relevant ( radio astronomical data ).</p> <p>The result is a float value.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "relFITSFilePath",
            "description": "<p>the path to the FITS file of interest.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRAO",
            "description": "<p>the lower index of the range along the RA axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iRA1",
            "description": "<p>the upper index of the range along the RA axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDECO",
            "description": "<p>the lower index of the range along the DEC axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iDEC1",
            "description": "<p>the upper index of the range along the DEC axis.</p>"
          },
          {
            "group": "Parameter",
            "type": "uInt",
            "optional": false,
            "field": "iFREQ",
            "description": "<p>index along the FREQ axis.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{'status': True, 'message': '', 'result': -0.24340537190437317}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./serverWsgi.py",
    "groupTitle": "values_and_projections"
  }
] });
