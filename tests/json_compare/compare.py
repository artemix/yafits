import sys
import argparse
import json
import os
import time
import traceback

def get_file_content(name):
    """Load content of json file

       Parameters
       ----------

       name : string
              Name of the json file
    """
    with open(name, "r") as f:
        return json.load(f)

def iterate_dict(dict1, dict2, chain, errors):
    """ Iterates on a dictionary and compares values to another one

        Parameters
        ----------

        dict1 : dict
                The reference dictionary we are iterating

        dict2 : dict
                Dictionary we want to compare to dict1

        chain : string
                The path we are exploring in dict1
    
        errors : list
                 A list of data that do not match between dict1 and dict2
    
    """
    for entry in dict1.items():
        newchain = chain + "['" + entry[0] + "']"
        if isinstance(entry[1], dict):     
            iterate_dict(entry[1], dict2, newchain, errors)
        else : 
            value1 = entry[1]
            value2 = eval("dict2"+newchain)
            if entry[1] != eval("dict2"+newchain):
                errors.append("values do not match for {} : ref is {}, test run is {} ".format(newchain, value1, value2))


def write_file_report(report_filename, checked_filename, errors):
    """ Appends report of a data file into report file

        Parameters
        ----------

        report_filename : string
                          path to file were report will be written

        checked_filename : string
                           name of checked data file

        errors : list
                 list of found errors
        
    """
    with open(report_filename, 'a') as f:
        f.write("Processed file : {} \n".format(checked_filename))
        if len(errors) == 0:
            f.write("\t")
            f.write("No error found")
            f.write("\n")
        else: 
            for e in errors :
                f.write("\t")
                f.write(e)
                f.write("\n")
        f.write("\n\n")

def write_exception_report(report_filename, message):
    """ Appends an exception to an output file

        Parameters:
        ----------

        report_filename : string
                          name of output file
                        
        message : string
                  the exception description
    """
    with open(report_filename, 'a') as f:
        f.write("WARNING : {}".format(message))
        f.write("\n\n")

def compare(data_dir, ref_dir):
    """ Compares files existing in ref_dir to those in data_dir and write a report

        Parameters
        ----------

        data_dir : string
                   path of directory containing the data files

        ref_dir : string
                  path of directory containing the reference files
    """
    date = time.localtime()
    output_filename = time.strftime("report-%Y-%m-%d-%H:%M:%S.txt", date)

    for ref_file in os.listdir(ref_dir):
        try : 
            file1 = get_file_content(ref_dir + "/" + ref_file)
            file2 = get_file_content(data_dir + "/" + ref_file)
            errors = []
            iterate_dict(file1, file2, "", errors)
            write_file_report(output_filename, ref_file, errors)
        except FileNotFoundError as e:
            traceback.format_exc()
            write_exception_report(output_filename, "File {} in reference directory is missing in OBJ directory".format(ref_file))


def main():
    data_dir = ""
    ref_dir = ""

    parser = argparse.ArgumentParser(
                    prog = 'YafitsDataTester',
                    description = 'Checks that data displayed by Yafits are correct')

    parser.add_argument("-r", "--reference", help="Path to reference data folder")
    parser.add_argument("-d", "--data", help="Path to comparison data folder")
    
    args = parser.parse_args(sys.argv[1:])    

    correct_args = 0

    if args.reference is not None :  
        ref_dir = args.reference
        correct_args = correct_args + 1

    if args.data is not None :  
        data_dir = args.data
        correct_args = correct_args + 1

    if correct_args !=  2:
        print("Incorrect parameters")
        print('compare.py -r <reference data path> -d <data path> -h')
        sys.exit()
    else : 
        compare(data_dir, ref_dir)


if __name__ == "__main__":
    main()



